﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace CRM.Hcsra.WebConfigDictionary
{
    static public class WebConfigKeyValue
    {
        public static string mvcPort = ConfigurationManager.AppSettings["MVCPort"];
        public static string mvcPath = ConfigurationManager.AppSettings["MVCPath"];
        public static string ifnFolder = ConfigurationManager.AppSettings["IFNFolder"];
        public static string ifnFolderFroEmailAttachment = ConfigurationManager.AppSettings["IFNFolderFroEmailAttachment"];        
        public static string claimDocsIFNFolder = ConfigurationManager.AppSettings["ClaimDocsIFNFolder"];
        public static string claimDocsIFNIdentifier = ConfigurationManager.AppSettings["ClaimDocsIFNIdentifier"];
        public static string serviceAddress = ConfigurationManager.AppSettings["ServiceAddress"];
        public static string serverURL = ConfigurationManager.AppSettings["ServerURL"];
        public static string crm4ServiceURL = serverURL + ConfigurationManager.AppSettings["crm4ServiceURL"];
        public static string metaDataServiceURL = serverURL + ConfigurationManager.AppSettings["MetaDataServiceURL"];
        public static string webFacadeServiceUrl = serverURL + ":" + mvcPort + ConfigurationManager.AppSettings["WebFacadeServiceUrl"];
        public static string webFacadePolicyMethod = ConfigurationManager.AppSettings["WebFacadePolicyMethod"];
        public static string webFacadeClaimParticipantMethod = ConfigurationManager.AppSettings["WebFacadeClaimParticipantMethod"];
        public static string webFacadeClaimPolicyMethod = ConfigurationManager.AppSettings["WebFacadeClaimPolicyMethod"];
        public static string webFacadeClaimKisuiMethod = ConfigurationManager.AppSettings["WebFacadeClaimKisuiMethod"];
        public static string crmOwner = ConfigurationManager.AppSettings["CrmOwner"];
        public static string crmOwnerPassword = ConfigurationManager.AppSettings["CrmOwnerPassword"];
        public static string crmDomain = ConfigurationManager.AppSettings["CrmDomain"];
        public static string organizationName = ConfigurationManager.AppSettings["organizationName"];
        public static string templatesXMLFile = ConfigurationManager.AppSettings["TemplatesXMLFile"];
        public static string templatesFiles = ConfigurationManager.AppSettings["TemplatesFiles"];
        public static string imagesFolder = ConfigurationManager.AppSettings["ImagesFolder"];
        public static string templatesDocFiles = ConfigurationManager.AppSettings["TemplatesDocFiles"];        
        public static string csvToEntityMappingXML = ConfigurationManager.AppSettings["CSVToEntityMappingXML"];
        public static string csvToEntityExeFilePath = ConfigurationManager.AppSettings["CSVToEntityExeFilePath"];
        public static string generalInfoWCFPath = ConfigurationManager.AppSettings["generalInfoWCFPath"];
        public static string updateTachanaWCFPath = ConfigurationManager.AppSettings["updateTachanaWCFPath"];
        public static string policyInfoWCFPath = ConfigurationManager.AppSettings["policyInfoWCFPath"];
        public static string kisuimWCFPath = ConfigurationManager.AppSettings["kisuimWCFPath"];
        public static string KisuimFields = ConfigurationManager.AppSettings["KisuimFields"];
        public static string policyFields = ConfigurationManager.AppSettings["PolicyFields"];
        public static string polisaIconLocation = ConfigurationManager.AppSettings["PolisaIconLocation"];
        public static string evFileLoadIconLocation = ConfigurationManager.AppSettings["EVFileLoadIconLocation"];
        public static string noPremissionToUpdateTahana = ConfigurationManager.AppSettings["NoPremissionToUpdateTahana"];
        public static string policiesErrorNoPremission = ConfigurationManager.AppSettings["PliciesErrorNoPremission"];
        public static string generalErrorMessage = ConfigurationManager.AppSettings["GeneralErrorMessage"];
        public static string noPremissionErrorOracle = ConfigurationManager.AppSettings["NoPremissionErrorOracle"];
        public static string entityRecordURL = ConfigurationManager.AppSettings["EntityRecordURL"];
        public static string fullEntityRecordURL = ConfigurationManager.AppSettings["FullEntityRecordURL"];
        public static string asposeLicense = ConfigurationManager.AppSettings["AsposeLicense"];
        public static string emailToIFNCode = ConfigurationManager.AppSettings["EmailToIFNCode"];
        public static string tahanaHistoryWCFPath = ConfigurationManager.AppSettings["TahanaHistoryWCFPath"];
        public static string TifulitCancelPolicy = ConfigurationManager.AppSettings["TifulitCancelPolicy"];
        public static string TifulitCancelGvia = ConfigurationManager.AppSettings["TifulitCancelGvia"];
        public static string IsTifulitProductionEnvirment = ConfigurationManager.AppSettings["IsTifulitProductionEnvirment"];
        public static string pidyonTzvirotWCFPath = ConfigurationManager.AppSettings["pidyonTzvirotWCFPath"];
        public static string SetHoratTashlumTviaWCFPath = ConfigurationManager.AppSettings["SetHoratTashlumTviaWCFPath"];
        public static string SetHorahatTashlumPremiaWCFPath = ConfigurationManager.AppSettings["SetHorahatTashlumPremiaWCFPath"];
        public static string mappingXMLFile = ConfigurationManager.AppSettings["MappingXMLFile"];
        public static string printPolicyUrl = ConfigurationManager.AppSettings["printPolicyUrl"];
        public static string reportServiceUrl = ConfigurationManager.AppSettings["ReportServiceUrl"];
        public static string safeFamilySoldReport = ConfigurationManager.AppSettings["SafeFamilySoldReport"];
        public static string pniyaYeshiraTemplateTitle = ConfigurationManager.AppSettings["PniyaYeshiraTemplateTitle"];
        public static string pikuahTemplateTitle = ConfigurationManager.AppSettings["PikuahTemplateTitle"];
        public static string misparTachana = ConfigurationManager.AppSettings["misparTachana"];
        public static string sqlServerURL = ConfigurationManager.AppSettings["SQLServerURL"];
        public static string docTypeCode = ConfigurationManager.AppSettings["docTypeCode"];
        public static string directoryCode = ConfigurationManager.AppSettings["directoryCode"];
        public static string holdingsXmlFilePath = ConfigurationManager.AppSettings["HoldingsXmlFilePath"];
        public static string DocFilesFilePath = ConfigurationManager.AppSettings["DocFilesFilePath"];
        public static string DocumentsForIncident = ConfigurationManager.AppSettings["DocumentsForIncident"];
        
        public static string GetDocFilesFolder(string fileName)
        {
            return WebConfigKeyValue.serverURL + ":" + WebConfigKeyValue.mvcPort + "/" + WebConfigKeyValue.mvcPath + WebConfigKeyValue.DocFilesFilePath + System.IO.Path.GetFileName(fileName);
        }
    }
}