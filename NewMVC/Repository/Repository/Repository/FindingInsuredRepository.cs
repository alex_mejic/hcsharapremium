﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Xrm.Sdk.Client;
using CRM.Hcsra.DataModel;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using CRM.Hcsra.Repository;
using DataModel.Models;

namespace Repository.Repository
{
    public class FindingInsuredRepository : RepositoryBase
    {
        public FindingInsuredRepository(OrganizationServiceProxy service = null)
          : base(service)
        {

        }
        public List<InshuredContactVM> GetInshured(FindingInsuredSearchModel model)
        {

            var query = (from con in HcsraContext.ContactSet
                         where con.FullName.Contains(model.insuredName)
                         where con.cs1_mis_telefon.Contains(model.inshuredTel)
                         where con.cs1_l_mis_zehut.Contains(model.inshuredId)
                         where con.StateCode == ContactState.Active
                         select con);


            if (!string.IsNullOrEmpty(model.inshuredKollektive))
            {
                query.Join(HcsraContext.AccountSet,
                    contact => contact.ParentCustomerId.Id,
                    collective => collective.AccountId.Value,
                    (contact, collective) => new { contact, collective })
                    .Where(x => x.collective.Name.Contains(model.inshuredKollektive));

            }

            if (!string.IsNullOrEmpty(model.inshuredPolicyNum))
            {
                query.Join(HcsraContext.cs1_polisaSet,
                    user => user.ParentCustomerId.Id,
                    polisa => polisa.cs1_l_mis_zehut.Id,
                    (user, polisa) => new { user, polisa })
                    .Where(o => o.polisa.cs1_name.Contains(model.inshuredPolicyNum));

            }

            var items = (from con in query
                         select new InshuredContactVM()
                         {
                             entityId = con.Id.ToString(),
                             FullName = con.cs1_shem_prati + " " + con.cs1_shem_mishpacha,
                             BDate = con.cs1_l_tar_leida.HasValue ? con.cs1_l_tar_leida.Value : new DateTime(0),
                             FlatNumber = con.cs1_flat,
                             Fname = con.cs1_shem_prati,
                             HouseNumber = con.cs1_bait,
                             Lname = con.cs1_shem_mishpacha,
                             MainCity = con.cs1_shem_yishuv_s,
                             MainStreet = con.cs1_rechov_s,
                             MobilPhone = con.cs1_mobile_phone,
                             Phone = con.cs1_mis_telefon,
                             TeudaType = "",
                             TeudaTypeInt = con.cs1_kod_zehut.Value != null ? con.cs1_kod_zehut.Value : (int?)0,
                             TeudatZehut = con.cs1_l_mis_zehut,
                             ZipCode = con.cs1_mikud

                         })
                        .Take(500)
                        .ToList();

            #region old
            ////old
            //if (!string.IsNullOrEmpty(model.inshuredKollektive))
            //{
            //    items = (from con in HcsraContext.ContactSet
            //             join coll in HcsraContext.AccountSet on con.ParentCustomerId.Id equals coll.AccountId.Value
            //             where con.FullName.Contains(model.insuredName)
            //             where con.cs1_mis_telefon.Contains(model.inshuredTel)
            //             where con.cs1_l_mis_zehut.Contains(model.inshuredId)
            //             where con.StateCode == ContactState.Active
            //             where coll.Name.Contains(model.inshuredKollektive)
            //             select new InshuredContactVM()
            //             {
            //                 entityId = con.Id.ToString(),
            //                 FullName = con.cs1_shem_prati + " " + con.cs1_shem_mishpacha,
            //                 BDate = con.cs1_l_tar_leida.HasValue ? con.cs1_l_tar_leida.Value : new DateTime(0),
            //                 FlatNumber = con.cs1_flat,
            //                 Fname = con.cs1_shem_prati,
            //                 HouseNumber = con.cs1_bait,
            //                 Lname = con.cs1_shem_mishpacha,
            //                 MainCity = con.cs1_shem_yishuv_s,
            //                 MainStreet = con.cs1_rechov_s,
            //                 MobilPhone = con.cs1_mobile_phone,
            //                 Phone = con.cs1_mis_telefon,
            //                 TeudaType = "",
            //                 TeudaTypeInt = con.cs1_kod_zehut.Value != null ? con.cs1_kod_zehut.Value : (int?)0,
            //                 TeudatZehut = con.cs1_l_mis_zehut,
            //                 ZipCode = con.cs1_mikud
            //             }).Take(4000)
            //       .ToList();
            //}

            //else if (!string.IsNullOrEmpty(model.inshuredPolicyNum))
            //{
            //    items =
            //         (from con in HcsraContext.ContactSet
            //          join pol in HcsraContext.cs1_polisaSet on con.ContactId.Value equals pol.cs1_l_mis_zehut.Id
            //          where con.FullName.Contains(model.insuredName)
            //          where con.cs1_mis_telefon.Contains(model.inshuredTel)
            //          where con.cs1_l_mis_zehut.Contains(model.inshuredId)
            //          where con.StateCode == ContactState.Active
            //          where pol.cs1_name.Contains(model.inshuredPolicyNum)
            //          select new InshuredContactVM()
            //          {
            //              entityId = con.Id.ToString(),
            //              FullName = con.cs1_shem_prati + " " + con.cs1_shem_mishpacha,
            //              BDate = con.cs1_l_tar_leida.HasValue ? con.cs1_l_tar_leida.Value : new DateTime(0),
            //              FlatNumber = con.cs1_flat,
            //              Fname = con.cs1_shem_prati,
            //              HouseNumber = con.cs1_bait,
            //              Lname = con.cs1_shem_mishpacha,
            //              MainCity = con.cs1_shem_yishuv_s,
            //              MainStreet = con.cs1_rechov_s,
            //              MobilPhone = con.cs1_mobile_phone,
            //              Phone = con.cs1_mis_telefon,
            //              TeudaType = "",
            //              TeudaTypeInt = con.cs1_kod_zehut.Value != null ? con.cs1_kod_zehut.Value : (int?)0,
            //              TeudatZehut = con.cs1_l_mis_zehut,
            //              ZipCode = con.cs1_mikud
            //          }).Take(4000)
            //    .ToList();
            //}
            //else
            //{
            //    items =
            //         (from con in HcsraContext.ContactSet
            //          where con.FullName.Contains(model.insuredName)
            //          where con.cs1_mis_telefon.Contains(model.inshuredTel)
            //          where con.cs1_l_mis_zehut.Contains(model.inshuredId)
            //          where con.StateCode == ContactState.Active
            //          select new InshuredContactVM()
            //          {
            //              entityId = con.Id.ToString(),
            //              FullName = con.cs1_shem_prati + " " + con.cs1_shem_mishpacha,
            //              BDate = con.cs1_l_tar_leida.HasValue ? con.cs1_l_tar_leida.Value : new DateTime(0),
            //              FlatNumber = con.cs1_flat,
            //              Fname = con.cs1_shem_prati,
            //              HouseNumber = con.cs1_bait,
            //              Lname = con.cs1_shem_mishpacha,
            //              MainCity = con.cs1_shem_yishuv_s,
            //              MainStreet = con.cs1_rechov_s,
            //              MobilPhone = con.cs1_mobile_phone,
            //              Phone = con.cs1_mis_telefon,
            //              TeudaType = "",
            //              TeudaTypeInt = con.cs1_kod_zehut.Value != null ? con.cs1_kod_zehut.Value : (int?)0,
            //              TeudatZehut = con.cs1_l_mis_zehut,
            //              ZipCode = con.cs1_mikud
            //          }).Take(4000)
            //    .ToList();

            //} 
            #endregion

            return items;
        }
    }
}
