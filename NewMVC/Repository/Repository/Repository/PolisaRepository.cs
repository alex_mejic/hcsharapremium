﻿using CRM.Hcsra.DataModel;
using CRM.Hcsra.Repository;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Repository
{
    public class PolisaRepository : RepositoryBase
    {
        public PolisaRepository(OrganizationServiceProxy service = null)
            : base(service)
        {

        }

        public List<PolisaModel> GetPolisotToView(Guid entityGuid, String entityLogicalName, String ID)
        {
            IEnumerable<PolisaModel> policiesModel = new List<PolisaModel>();

            if (entityLogicalName == "contact")
            {
                policiesModel = HcsraContext.cs1_polisaSet.Where(p => p.cs1_l_mis_zehut.Id == entityGuid || p.cs1_l_mis_zehut_mishni.Id == entityGuid)
                                                           .Select(policy => new PolisaModel
                                                           {
                                                               Key = policy.cs1_polisaId,
                                                               Code = policy.cs1_l_mis_polisa, //מספר פוליסה                                                                  
                                                               StartDate = policy.cs1_l_tar_hatchala,//תאריך התחלת ביטוח
                                                               TkufatBituh = policy.cs1_tkufat_bituach,//תקופת ביטוח                                                                   
                                                               TarBitul = policy.cs1_l_tar_bitul,//תאריך ביטול
                                                               MisCheshbonBank = policy.cs1_mis_cheshbon_bank,//חשבון                                                                                                      
                                                               MevutahRashi = policy.cs1_l_mis_zehut,
                                                               MisZehutBalPolisa = policy.cs1_l_mis_zehut_bal_polisa,
                                                               TarihTumTkufatBituh = policy.cs1_tar_sium,
                                                               DerechGvia = policy.cs1_derech_gvia != null ? policy.FormattedValues["cs1_derech_gvia"] : "",

                                                               Kollektive = policy.cs1_mis_kolektiv != null ? (from kol in HcsraContext.AccountSet
                                                                                                               where kol.AccountId.Value == policy.cs1_mis_kolektiv.Id
                                                                                                               select kol.Name).FirstOrDefault() : "",
                                                               AgentName = policy.cs1_mispar_sochen != null ? (from ag in HcsraContext.cs1_sochenSet
                                                                                                               where ag.cs1_sochenId.Value == policy.cs1_mispar_sochen.Id
                                                                                                               select ag.cs1_name).FirstOrDefault() : ""
                                                           }).ToList();






                foreach (PolisaModel polisa in policiesModel)
                {
                    polisa.SugMevutah = "משני";

                    if (polisa.MevutahRashi != null)
                    {
                        polisa.SugMevutah = polisa.MevutahRashi.Id == entityGuid ? "ראשי" : "משני";
                    }

                    polisa.IsMevutahRashi = polisa.MisZehutBalPolisa == ID ? "כן" : "לא";
                }
            }
            else
            {
                policiesModel = HcsraContext.cs1_polisaSet.Where(p => p.cs1_mis_kolektiv.Id == entityGuid)
                                                           .Select(policy => new PolisaModel
                                                           {
                                                               Key = policy.cs1_polisaId,
                                                               Code = policy.cs1_l_mis_polisa, //מספר פוליסה                                                                  
                                                               StartDate = policy.cs1_l_tar_hatchala,//תאריך התחלת ביטוח
                                                               TkufatBituh = policy.cs1_tkufat_bituach,//תקופת ביטוח                                                                   
                                                               TarBitul = policy.cs1_l_tar_bitul,//תאריך ביטול
                                                               MisCheshbonBank = policy.cs1_mis_cheshbon_bank,//חשבון
                                                               DerechGvia = policy.cs1_derech_gvia != null ? policy.FormattedValues["cs1_derech_gvia"] : ""
                                                           }).ToList();
            }

            return policiesModel.ToList();
        }


        public List<PolisaModel> GetPolisotToIncidentView(Guid incidentId, Guid contactId, string ID)
        {
            IEnumerable<PolisaModel> policiesModel = new List<PolisaModel>();

            policiesModel = (from policy in HcsraContext.cs1_polisaSet
                             join polisaIncidentCoonection in HcsraContext.cs1_incident_cs1_polisaSet
                             on policy.cs1_polisaId equals polisaIncidentCoonection.cs1_polisaid
                             where polisaIncidentCoonection.incidentid == incidentId
                             select new PolisaModel
                             {
                                 Key = policy.cs1_polisaId,
                                 Code = policy.cs1_l_mis_polisa, //מספר פוליסה                                                                  
                                 StartDate = policy.cs1_l_tar_hatchala,//תאריך התחלת ביטוח
                                 TkufatBituh = policy.cs1_tkufat_bituach,//תקופת ביטוח                                                                   
                                 TarBitul = policy.cs1_l_tar_bitul,//תאריך ביטול
                                 MisCheshbonBank = policy.cs1_mis_cheshbon_bank,//חשבון                                                                                                      
                                 MevutahRashi = policy.cs1_l_mis_zehut,
                                 MisZehutBalPolisa = policy.cs1_l_mis_zehut_bal_polisa,
                                 TarihTumTkufatBituh = policy.cs1_tar_sium,
                                 DerechGvia = policy.cs1_derech_gvia != null ? policy.FormattedValues["cs1_derech_gvia"] : ""
                             }).ToList();

            foreach (PolisaModel polisa in policiesModel)
            {
                polisa.SugMevutah = "משני";

                if (polisa.MevutahRashi != null)
                {
                    polisa.SugMevutah = polisa.MevutahRashi.Id == contactId ? "ראשי" : "משני";
                }

                polisa.IsMevutahRashi = polisa.MisZehutBalPolisa == ID ? "כן" : "לא";
            }

            return policiesModel.ToList();
        }


        public PolisaModel GetPolisaById(Guid key)
        {
            //PicklistAttributeMetadata ofenTashlumMetaData = RetrievePickListAttributeMetaData(cs1_polisa.EntityLogicalName, "cs1_ofen_tashlum");

            var polisa = HcsraContext.cs1_polisaSet.Where(p => p.cs1_polisaId.Value == key)
                                                       .ToList()
                                                       .Select(policy => new PolisaModel
                                                       {
                                                           Key = policy.cs1_polisaId,
                                                           Code = policy.cs1_l_mis_polisa, //מספר פוליסה                                                                  
                                                           StartDate = policy.cs1_l_tar_hatchala,//תאריך התחלת ביטוח
                                                           TkufatBituh = policy.cs1_tkufat_bituach,//תקופת ביטוח                                                                   
                                                           TarBitul = policy.cs1_l_tar_bitul,//תאריך ביטול
                                                           MisCheshbonBank = policy.cs1_mis_cheshbon_bank,//חשבון
                                                           // PaymentType = RetrieveOptionSetName(ofenTashlumMetaData, policy.cs1_ofen_tashlum),// אופן תשלום                                                           
                                                           PaymentType = policy.cs1_ofen_tashlum != null ? policy.FormattedValues["cs1_ofen_tashlum"] : "",// אופן תשלום
                                                           DerechGvia = policy.cs1_derech_gvia != null ? policy.FormattedValues["cs1_derech_gvia"] : ""
                                                       }).FirstOrDefault();


            return polisa;
        }

        public Guid GetOrCreatePolisaByElementaryInfo(string policyNumber, string branchNumber, string contactGuid)
        {
            cs1_polisa pol = HcsraContext.cs1_polisaSet
                .Where(p => p.cs1_name == policyNumber && p.cs1_anaf_number == branchNumber)
                .FirstOrDefault();


            if (pol == null)
            {
                pol = new cs1_polisa()
                {
                    cs1_polisaId = Guid.NewGuid(),
                    cs1_name = policyNumber,
                    cs1_l_mis_polisa = policyNumber,
                    cs1_anaf_number = branchNumber,
                    cs1_l_mis_zehut = new EntityReference(Contact.EntityLogicalName, new Guid(contactGuid))
                };

                Create(pol);
            }


            return pol.cs1_polisaId.Value;
        }

        //private String RetrieveOptionSetName(PicklistAttributeMetadata optionsetMetaData, OptionSetValue optionValue)
        //{
        //    if (optionValue != null)
        //    {
        //        foreach (OptionMetadata optionMeta in optionsetMetaData.OptionSet.Options)
        //        {
        //            if (optionMeta.Value == optionValue.Value)
        //            {
        //                return optionMeta.Label.UserLocalizedLabel.Label;
        //            }
        //        }
        //    }
        //    return String.Empty;
        //}

        // returns related polisot byclaimId + number of first (top) polisot to retrieve - in case needed
        public List<cs1_policies_claims> GetPolisotRelatedToClaim(Guid claimId, int topAmount = -1)
        {
            var polisot = (from polisa in HcsraContext.cs1_policies_claimsSet
                           where polisa.cs1_claim.Id == claimId
                           select new cs1_policies_claims
                           {
                               Id = polisa.Id,
                               cs1_l_mis_polisa = polisa.cs1_l_mis_polisa
                           });

            if (topAmount != -1)
            {
                polisot = polisot.Take(topAmount);
            }

            return polisot.ToList();
        }

        public cs1_polisa GetPolisaByPolisaNumOrId(string polisaNumber, Guid polisaGuid)
        {
            cs1_polisa polisa = new cs1_polisa();
            if (polisaGuid == Guid.Empty)
            {
                polisa = (from pol in HcsraContext.cs1_polisaSet
                          where pol.cs1_l_mis_polisa == polisaNumber
                          select new cs1_polisa
                          {
                              Id = pol.Id,
                              cs1_l_mis_polisa = pol.cs1_l_mis_polisa,
                              cs1_l_mis_zehut = pol.cs1_l_mis_zehut,
                              cs1_l_mis_zehut_mishni = pol.cs1_l_mis_zehut_mishni
                          }).FirstOrDefault();
            }
            else
            {
                polisa = (from pol in HcsraContext.cs1_polisaSet
                          where pol.cs1_polisaId.Value == polisaGuid
                          select new cs1_polisa
                          {
                              Id = pol.Id,
                              cs1_l_mis_polisa = pol.cs1_l_mis_polisa,
                              cs1_l_mis_zehut = pol.cs1_l_mis_zehut,
                              cs1_l_mis_zehut_mishni = pol.cs1_l_mis_zehut_mishni
                          }).FirstOrDefault();
            }
            return polisa;
        }

        //public List<cs1_polisa> GetPolisotRelatedToClaims(Guid claimId)
        //{
        //    return (from pol in HcsraContext.cs1_polisaSet
        //            join polClaim in HcsraContext.cs1_policies_claimsSet
        //            on pol.cs1_polisaId.Value equals polClaim.cs1_l_mis_polisa.Id
        //            where polClaim.cs1_claim.Id == claimId
        //            select new cs1_polisa
        //            {
        //                Id = pol.cs1_polisaId.Value,
        //                cs1_l_mis_polisa = pol.cs1_l_mis_polisa,
        //                cs1_l_mis_zehut = pol.cs1_l_mis_zehut,
        //                cs1_l_mis_zehut_mishni = pol.cs1_l_mis_zehut_mishni
        //            })
        //            .Distinct(new EntityComparer<cs1_polisa>())
        //            .ToList()
        //            .ConvertAll(x => (cs1_polisa)x);
        //}

        //public List<PolicyPaymentScreenModel> GetPolisotForPaymentScreen(Guid claimId, List<cs1_index_rate> indexRates = null)
        //{
        //    return (from polisaClaim in HcsraContext.cs1_policies_claimsSet
        //            join polisa in HcsraContext.cs1_polisaSet
        //            on polisaClaim.cs1_l_mis_polisa.Id equals polisa.cs1_polisaId.Value
        //            where polisaClaim.cs1_claim.Id == claimId
        //            select new PolicyPaymentScreenModel
        //            {
        //                cs1_claim_date_index_rate = polisaClaim.cs1_claim_date_index_rate.HasValue ? polisaClaim.cs1_claim_date_index_rate.Value : decimal.Zero,
        //                cs1_mis_polisa = polisa.cs1_l_mis_polisa,
        //                link = WebConfigKeyValue.serverURL + "/" + WebConfigKeyValue.organizationName + "/main.aspx?etn=cs1_polisa&pagetype=entityrecord&extraqs=id=" + polisaClaim.cs1_l_mis_polisa.Id,
        //                cs1_gross_polisa_pay = polisaClaim.cs1_gross_polisa_pay != null ? polisaClaim.cs1_gross_polisa_pay.Value : 0.0M,
        //                cs1_indexed_gross = polisaClaim.cs1_indexed_gross != null ? polisaClaim.cs1_indexed_gross.Value : 0.0M,
        //                cs1_madad = GetMadadFromIndexRate(indexRates, new OptionSetValue(1)),
        //                cs1_pigur = polisaClaim.cs1_pigur != null ? polisaClaim.cs1_pigur.Value : 0.0M,
        //                Id = polisaClaim.cs1_l_mis_polisa.Id,
        //                cs1_polisa_claimId = polisaClaim.Id,
        //                cs1_bonus = polisaClaim.cs1_bonus != null ? polisaClaim.cs1_bonus.Value : decimal.Zero
        //            }).ToList();
        //}

        private decimal GetMadadFromIndexRate(List<cs1_index_rate> indexRates, OptionSetValue cs1_madad)
        {
            decimal? result = decimal.Zero;
            if (cs1_madad != null)
            {
                result = (from indexRate in indexRates
                          where indexRate.cs1_madad != null && indexRate.cs1_madad.Value == cs1_madad.Value
                          select indexRate.cs1_value).FirstOrDefault();
            }

            return result != null ? result.Value : 0.0M;
        }

        public PolisaDetailsModel RetrievePolisotDetailsModel(Guid polisaId)
        {
            return (from polisa in HcsraContext.cs1_polisaSet
                    where polisa.cs1_polisaId.Value == polisaId
                    select new PolisaDetailsModel
                    {
                        cs1_derech_gvia = polisa.cs1_derech_gvia != null ? polisa.cs1_derech_gvia.Value : -1,
                        cs1_ikul = polisa.cs1_ikul != null ? polisa.cs1_ikul.Value : -1,
                        cs1_kod_meashen_rashi = polisa.cs1_kod_meashen_rashi != null ? polisa.cs1_kod_meashen_rashi.Value : -1,
                        cs1_kod_meashen_mishni = polisa.cs1_kod_meashen_mishni != null ? polisa.cs1_kod_meashen_mishni.Value : -1,
                        cs1_l_mis_zehut = polisa.cs1_l_mis_zehut != null ? polisa.cs1_l_mis_zehut.Name : "",
                        cs1_l_mis_zehut_mishni = polisa.cs1_l_mis_zehut_mishni != null ? polisa.cs1_l_mis_zehut_mishni.Name : "",
                        cs1_l_tar_bitul = polisa.cs1_l_tar_bitul.HasValue ? polisa.cs1_l_tar_bitul.Value.ToLocalTime().ToShortDateString() : "",
                        cs1_mis_atraa = polisa.cs1_mis_atraa,
                        cs1_mis_kolektiv = polisa.cs1_mis_kolektiv != null ? polisa.cs1_mis_kolektiv.Name : "",
                        cs1_mis_mefakeach_mekori = polisa.cs1_mis_mefakeach_mekori != null ? polisa.cs1_mis_mefakeach_mekori.Name : "",
                        cs1_mispar_sochen = polisa.cs1_mispar_sochen != null ? polisa.cs1_mispar_sochen.Name : "",
                        cs1_product = polisa.cs1_product != null ? polisa.cs1_product.Name : "",
                        cs1_schum_loan = polisa.cs1_schum_loan != null ? string.Format("{0:n}", polisa.cs1_schum_loan.Value) : "",
                        cs1_schum_premia = polisa.cs1_schum_premia != null ? string.Format("{0:n}", polisa.cs1_schum_premia.Value) : "",
                        cs1_shem_bal_polisa = polisa.cs1_shem_bal_polisa
                    }).FirstOrDefault();
        }

        public cs1_polisa GetPolicyForFileSpecificationById(Guid policyId)
        {
            return (from policy in HcsraContext.cs1_polisaSet
                    where policy.cs1_polisaId.Value == policyId
                    select new cs1_polisa()
                    {
                        Id = policy.cs1_polisaId.Value,
                        cs1_kod_honi = policy.cs1_kod_honi,
                        cs1_name = policy.cs1_name
                    }).FirstOrDefault();
        }

        public EntityReference GetContact(Guid policyId)
        {
            return (from policy in HcsraContext.cs1_polisaSet
                    where policy.cs1_polisaId.Value == policyId
                    select policy.cs1_l_mis_zehut).FirstOrDefault();
        }

        public List<PolisaModel> AddSelectedPolisotToList(List<PolisaModel> policiesList, string polisotToAdd, Guid contactId, string ID)
        {
            List<Guid> setectedPolisaGuidList = SetPolisaGuidList(polisotToAdd);

            if (setectedPolisaGuidList.Count() == 0)
            {
                return policiesList;
            }

            IEnumerable<PolisaModel> policiesModel = new List<PolisaModel>();

            //var t = setectedPolisaGuidList.Where(x => x HcsraContext.cs1_polisaSet)

            policiesModel = (from policy in HcsraContext.cs1_polisaSet
                             where policy.cs1_polisaId.Value.ToString().Contains("")
                             //where setectedPolisaGuidList.Contains(policy.cs1_polisaId.Value)
                             //where setectedPolisaGuidList.Any(polisaToAdd => policy.cs1_polisaId.Equals(polisaToAdd))
                             select new PolisaModel
                             {
                                 Key = policy.cs1_polisaId,
                                 Code = policy.cs1_l_mis_polisa, //מספר פוליסה                                                                  
                                 StartDate = policy.cs1_l_tar_hatchala,//תאריך התחלת ביטוח
                                 TkufatBituh = policy.cs1_tkufat_bituach,//תקופת ביטוח                                                                   
                                 TarBitul = policy.cs1_l_tar_bitul,//תאריך ביטול
                                 MisCheshbonBank = policy.cs1_mis_cheshbon_bank,//חשבון                                                                                                      
                                 MevutahRashi = policy.cs1_l_mis_zehut,
                                 MisZehutBalPolisa = policy.cs1_l_mis_zehut_bal_polisa,
                                 TarihTumTkufatBituh = policy.cs1_tar_sium,
                                 DerechGvia = policy.cs1_derech_gvia != null ? policy.FormattedValues["cs1_derech_gvia"] : ""
                             }).ToList();

            foreach (PolisaModel polisa in policiesModel)
            {
                polisa.SugMevutah = "משני";

                if (polisa.MevutahRashi != null)
                {
                    polisa.SugMevutah = polisa.MevutahRashi.Id == contactId ? "ראשי" : "משני";
                }

                polisa.IsMevutahRashi = polisa.MisZehutBalPolisa == ID ? "כן" : "לא";

                policiesList.Add(polisa);
            }

            return policiesList;
        }

        public List<Guid> SetPolisaGuidList(string polisotToAdd)
        {
            List<Guid> polisaGuidList = new List<Guid>();

            if (!string.IsNullOrEmpty(polisotToAdd))
            {
                string[] polisaIdArr = polisotToAdd.Split('+');

                foreach (var polisaId in polisaIdArr)
                {
                    if (!string.IsNullOrEmpty(polisaId))
                    {
                        polisaGuidList.Add(new Guid(polisaId));
                    }
                }
            }

            return polisaGuidList;
        }
    }
}
