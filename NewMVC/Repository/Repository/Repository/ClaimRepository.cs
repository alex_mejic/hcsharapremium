﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using Microsoft.Xrm.Sdk.Client;
//using Microsoft.Xrm.Sdk;
//using Microsoft.Xrm.Sdk.Query;

//namespace CRM.Hcsra.Repository
//{
//    public class ClaimRepository : RepositoryBase
//    {
//        /// <summary>
//        /// Constructor
//        /// </summary>
//        /// <param name="service"></param>
//        public ClaimRepository(OrganizationServiceProxy service = null)
//            : base(service)
//        {

//        }

//        /// <summary>
//        /// Get claim view type by claim id
//        /// </summary>
//        /// <param name="claimId"></param>
//        /// <returns></returns>
//        public string GetClaimViewType(Guid claimId)
//        {
//            string result = string.Empty;

//            cs1_claimtype claimType = (from clType in HcsraContext.cs1_claimtypeSet
//                                       join cl in HcsraContext.cs1_claimSet
//                                       on clType.cs1_claimtypeId.Value equals cl.cs1_claimtype.Id
//                                       where cl.cs1_claimId.Value == claimId
//                                       select new cs1_claimtype
//                                       {
//                                           cs1_decision_view_type = clType.cs1_decision_view_type,
//                                           cs1_decision_view_sub_type = clType.cs1_decision_view_sub_type
//                                       }).FirstOrDefault();

//            if (claimType != null)
//            {
//                result = claimType.cs1_decision_view_type != null ? claimType.cs1_decision_view_type.Value.ToString() : "";
//                //רק במידה וסוג תצוגה הוא בריאות יש לבדוק מה סוג תצוגה משני
//                if (claimType.cs1_decision_view_type != null && claimType.cs1_decision_view_type.Value == 2)
//                {
//                    result += claimType.cs1_decision_view_sub_type != null ? claimType.cs1_decision_view_sub_type.Value.ToString() : "";
//                }
//            }

//            return result;
//        }

//        /// <summary>
//        /// Get claim reminder fields by claim id
//        /// </summary>
//        /// <param name="claimId"></param>
//        /// <returns></returns>
//        public cs1_claim GetClaimReminderFieldsById(Guid claimId)
//        {
//            cs1_claim claim = (from cl in HcsraContext.cs1_claimSet
//                               where cl.cs1_claimId == claimId
//                               select new cs1_claim
//                               {
//                                   Id = cl.Id,
//                                   cs1_mis_zehut = cl.cs1_mis_zehut,
//                                   cs1_name = cl.cs1_name,
//                                   cs1_first_reminder = cl.cs1_first_reminder,
//                                   cs1_second_reminder = cl.cs1_second_reminder,
//                                   cs1_first_reminder_date = cl.cs1_first_reminder_date
//                               }).FirstOrDefault();

//            return claim;
//        }

//        /// <summary>
//        /// Get claim by id
//        /// </summary>
//        /// <param name="key"></param>
//        /// <returns></returns>
//        public cs1_claim GetClaimById(Guid key)
//        {
//            cs1_claim claim = (from cl in HcsraContext.cs1_claimSet
//                               where cl.cs1_claimId == key
//                               select new cs1_claim
//                               {
//                                   Id = cl.Id,
//                                   cs1_first_save = cl.cs1_first_save,
//                                   cs1_contact = cl.cs1_contact,
//                                   cs1_offset_dependened_calc = cl.cs1_offset_dependened_calc,
//                                   cs1_offset_dependened_manual = cl.cs1_offset_dependened_manual,
//                                   //cs1_medtech_treats = cl.cs1_medtech_treats,
//                                   //cs1_medtech_exp_treats = cl.cs1_medtech_exp_treats,
//                                   //cs1_medtech_alternates = cl.cs1_medtech_alternates,
//                                   cs1_medtech_sum = cl.cs1_medtech_sum,
//                                   cs1_claimtype = cl.cs1_claimtype
//                               }).FirstOrDefault();

//            return claim;
//        }

//        public cs1_claim GetClaimEntityById(Guid key)
//        {
//            return (from cl in HcsraContext.cs1_claimSet
//                    where cl.cs1_claimId == key
//                    select cl).FirstOrDefault();
//        }

//        /// <summary>
//        /// Get claim fields and claim typecode by ID
//        /// </summary>
//        /// <param name="key"></param>
//        /// <returns></returns>
//        public ClaimModel GetClaimFieldsAndClaimTypeCodeById(Guid key)
//        {
//            return (from cl in HcsraContext.cs1_claimSet
//                    join ct in HcsraContext.cs1_claimtypeSet
//                    on cl.cs1_claimtype.Id equals ct.Id
//                    where cl.cs1_claimId == key
//                    select new ClaimModel
//                    {
//                        Id = cl.Id,
//                        Contact = cl.cs1_contact,
//                        First_Save = cl.cs1_first_save,
//                        Mis_Zehut = cl.cs1_mis_zehut,
//                        IncidentDate = cl.cs1_date,
//                        ClaimTypeCode = ct.cs1_claimtypecode != null ? ct.cs1_claimtypecode.Trim(new char[] { '\'' }) : "",
//                        ClaimDateChanged = cl.cs1_is_date_changed.HasValue ? cl.cs1_is_date_changed.Value : false,
//                        MutavType = ct.cs1_mutav_type != null ? ct.cs1_mutav_type.Value == 1 ? "ח" : "מ" : ""
//                    }).FirstOrDefault();
//        }

//        /// <summary>
//        /// Get all open claims for specific contact by claim id and contact id
//        /// </summary>
//        /// <param name="claimId"></param>
//        /// <param name="contactId"></param>
//        /// <returns></returns>
//        public List<cs1_claim> GetAllOpenClaimsForSpecificContact(Guid claimId, Guid contactId)
//        {
//            List<cs1_claim> claims = (from cl in HcsraContext.cs1_claimSet
//                                      join con in HcsraContext.ContactSet
//                                      on cl.cs1_contact.Id equals con.Id
//                                      where cl.cs1_contact.Id == contactId && cl.cs1_claimId.Value != claimId && cl.cs1_is_closed != true
//                                      select new cs1_claim
//                                      {
//                                          Id = cl.Id
//                                      }).ToList();

//            return claims;
//        }

//        /// <summary>
//        /// Get claims by contact id
//        /// </summary>
//        /// <param name="contactId"></param>
//        /// <param name="consultationId"></param>
//        /// <param name="addClaim"></param>
//        /// <returns></returns>
//        public List<ClaimAddRemoveModel> GetClaimsByContactId(Guid contactId, Guid consultationId, int addClaim)
//        {
//            List<ClaimAddRemoveModel> result = (from cl in HcsraContext.cs1_claimSet
//                                                join kisui in HcsraContext.cs1_kisui_activeSet
//                                                on cl.cs1_claimId.Value equals kisui.cs1_claim.Id
//                                                where cl.cs1_is_closed != true && cl.cs1_contact.Id == contactId
//                                                select new ClaimAddRemoveModel()
//                                                {
//                                                    cs1_claim = new EntityReference(cl.LogicalName, cl.cs1_claimId.Value),
//                                                    cs1_consultation = new EntityReference(cs1_consultation.EntityLogicalName, consultationId),
//                                                    cs1_claimtype = cl.cs1_claimtype != null ? cl.cs1_claimtype.Name : "",
//                                                    cs1_date = cl.cs1_date.HasValue ? cl.cs1_date.Value.ToLocalTime().ToShortDateString() : "",
//                                                    cs1_name = cl.cs1_name,
//                                                    addClaim = addClaim
//                                                })
//                                               .Distinct(new ModelComparer<ClaimAddRemoveModel>())
//                                               .ToList()
//                                               .ConvertAll(x => (ClaimAddRemoveModel)x);

//            return result;
//        }

//        /// <summary>
//        /// Get claims with consultation claim relation by consultation id
//        /// </summary>
//        /// <param name="consultationId"></param>
//        /// <returns></returns>
//        public List<Guid> GetClaimsWithConsultationClaimRelation(Guid consultationId)
//        {
//            List<Guid> result = (from cons in HcsraContext.cs1_consultation_claim_relationSet
//                                 where cons.cs1_consultation.Id == consultationId
//                                 select cons.cs1_claim.Id).Distinct().ToList();

//            return result;
//        }

//        /// <summary>
//        /// Get claims related to consultation by consultation id
//        /// </summary>
//        /// <param name="consultationId"></param>
//        /// <param name="addClaim"></param>
//        /// <returns></returns>
//        public List<ClaimAddRemoveModel> GetClaimsRelatedToConsultation(Guid consultationId, int addClaim)
//        {
//            List<ClaimAddRemoveModel> result = new List<ClaimAddRemoveModel>();

//            result = (from cl in HcsraContext.cs1_claimSet
//                      join ccr in HcsraContext.cs1_consultation_claim_relationSet
//                      on cl.cs1_claimId.Value equals ccr.cs1_claim.Id
//                      where cl.cs1_is_closed != true
//                      where ccr.cs1_consultation.Id == consultationId
//                      select new ClaimAddRemoveModel()
//                      {
//                          cs1_claim = new EntityReference(cl.LogicalName, cl.cs1_claimId.Value),
//                          cs1_consultation = new EntityReference(cs1_consultation.EntityLogicalName, consultationId),
//                          cs1_claimtype = cl.cs1_claimtype != null ? cl.cs1_claimtype.Name : "",
//                          cs1_date = cl.cs1_date.HasValue ? cl.cs1_date.Value.ToLocalTime().ToShortDateString() : "",
//                          cs1_name = cl.cs1_name,
//                          addClaim = addClaim
//                      })
//                      .Distinct(new ModelComparer<ClaimAddRemoveModel>())
//                      .ToList()
//                      .ConvertAll(x => (ClaimAddRemoveModel)x);

//            return result;
//        }

//        /// <summary>
//        /// Get decision view type by claim type id
//        /// </summary>
//        /// <param name="claimTypeId"></param>
//        /// <returns></returns>
//        public int GetDecisionViewType(Guid claimTypeId)
//        {
//            OptionSetValue decisionViewType = (from clType in HcsraContext.cs1_claimtypeSet
//                                               where clType.cs1_claimtypeId.Value == claimTypeId
//                                               select clType.cs1_decision_view_type).FirstOrDefault();

//            return decisionViewType != null ? decisionViewType.Value : 0;
//        }

//        /// <summary>
//        /// Get claim by claim num or claim id
//        /// </summary>
//        /// <param name="claimNum"></param>
//        /// <param name="claimGuid"></param>
//        /// <returns></returns>
//        public ClaimModel GetClaimByClaimNumOrId(string claimNum, Guid claimGuid)
//        {
//            ClaimModel claim = new ClaimModel();
//            if (claimGuid == Guid.Empty && !string.IsNullOrEmpty(claimNum))
//            {
//                claim = (from cl in HcsraContext.cs1_claimSet
//                         join clType in HcsraContext.cs1_claimtypeSet
//                         on cl.cs1_claimtype.Id equals clType.cs1_claimtypeId.Value
//                         where cl.cs1_name == claimNum
//                         select new ClaimModel
//                         {
//                             Id = cl.Id,
//                             ClaimNumber = cl.cs1_name,
//                             ClaimTypeCode = clType.cs1_claimtypecode != null ? clType.cs1_claimtypecode.Trim(new char[] { '\'' }) : "",
//                             IncidentDate = cl.cs1_date,
//                             Contact = cl.cs1_contact,
//                             ClaimClosed = cl.cs1_is_closed,
//                             ClaimStatus = cl.cs1_claimstatus,
//                             ClaimTypeName = clType.cs1_name,
//                             CreatedOn = cl.CreatedOn
//                         }).FirstOrDefault();
//            }
//            else if (claimGuid != Guid.Empty)
//            {
//                claim = (from cl in HcsraContext.cs1_claimSet
//                         join clType in HcsraContext.cs1_claimtypeSet
//                         on cl.cs1_claimtype.Id equals clType.cs1_claimtypeId.Value
//                         where cl.cs1_claimId.Value == claimGuid
//                         select new ClaimModel
//                         {
//                             Id = cl.Id,
//                             ClaimNumber = cl.cs1_name,
//                             ClaimTypeCode = clType.cs1_claimtypecode != null ? clType.cs1_claimtypecode.Trim(new char[] { '\'' }) : "",
//                             IncidentDate = cl.cs1_date,
//                             Contact = cl.cs1_contact,
//                             ClaimClosed = cl.cs1_is_closed,
//                             ClaimStatus = cl.cs1_claimstatus,
//                             ClaimTypeName = clType.cs1_name,
//                             CreatedOn = cl.CreatedOn
//                         }).FirstOrDefault();
//            }
//            return claim;
//        }

//        /// <summary>
//        /// Get claim by claim num or claim id with out join to claimtype
//        /// </summary>
//        /// <param name="claimNum"></param>
//        /// <param name="claimGuid"></param>
//        /// <returns></returns>
//        public ClaimModel GetClaimByClaimNumOrIdWithNoClaimType(string claimNum, Guid claimGuid)
//        {
//            ClaimModel claim = new ClaimModel();
//            if (claimGuid == Guid.Empty && !string.IsNullOrEmpty(claimNum))
//            {
//                claim = (from cl in HcsraContext.cs1_claimSet
//                         where cl.cs1_name == claimNum
//                         select new ClaimModel
//                         {
//                             Id = cl.Id,
//                             ClaimNumber = cl.cs1_name,
//                             IncidentDate = cl.cs1_date,
//                             Contact = cl.cs1_contact,
//                             ClaimClosed = cl.cs1_is_closed,
//                             ClaimStatus = cl.cs1_claimstatus,
//                             CreatedOn = cl.CreatedOn
//                         }).FirstOrDefault();
//            }
//            else if (claimGuid != Guid.Empty)
//            {
//                claim = (from cl in HcsraContext.cs1_claimSet
//                         where cl.cs1_claimId.Value == claimGuid
//                         select new ClaimModel
//                         {
//                             Id = cl.Id,
//                             ClaimNumber = cl.cs1_name,
//                             IncidentDate = cl.cs1_date,
//                             Contact = cl.cs1_contact,
//                             ClaimClosed = cl.cs1_is_closed,
//                             ClaimStatus = cl.cs1_claimstatus,
//                             CreatedOn = cl.CreatedOn
//                         }).FirstOrDefault();
//            }
//            return claim;
//        }

//        /// <summary>
//        /// Get claims by contact id
//        /// </summary>
//        /// <param name="contactId"></param>
//        /// <returns></returns>
//        public List<ClaimModel> GetClaimsByContact(Guid contactId, EntityReference claimType)
//        {
//            List<ClaimModel> result = new List<ClaimModel>();

//            if (claimType != null)
//            {
//                result = (from cl in HcsraContext.cs1_claimSet
//                          join clType in HcsraContext.cs1_claimtypeSet
//                          on cl.cs1_claimtype.Id equals clType.cs1_claimtypeId.Value
//                          where cl.cs1_contact.Id == contactId
//                          where clType.cs1_claimtypeId.Value == claimType.Id
//                          select new ClaimModel
//                          {
//                              Id = cl.Id,
//                              ClaimNumber = cl.cs1_name,
//                              ClaimTypeCode = clType.cs1_claimtypecode != null ? clType.cs1_claimtypecode.Trim(new char[] { '\'' }) : "",
//                              IncidentDate = cl.cs1_date,
//                              Contact = cl.cs1_contact,
//                              ClaimClosed = cl.cs1_is_closed,
//                              ClaimStatus = cl.cs1_claimstatus,
//                              ClaimTypeName = clType.cs1_name,
//                              CreatedOn = cl.CreatedOn
//                          }).ToList();
//            }
//            else
//            {
//                result = (from cl in HcsraContext.cs1_claimSet
//                          join clType in HcsraContext.cs1_claimtypeSet
//                          on cl.cs1_claimtype.Id equals clType.cs1_claimtypeId.Value
//                          into clTemp
//                          from clT in clTemp.DefaultIfEmpty()
//                          where cl.cs1_contact.Id == contactId
//                          select new ClaimModel
//                          {
//                              Id = cl.Id,
//                              ClaimNumber = cl.cs1_name,
//                              IncidentDate = cl.cs1_date,
//                              Contact = cl.cs1_contact,
//                              ClaimClosed = cl.cs1_is_closed,
//                              ClaimStatus = cl.cs1_claimstatus,
//                              ClaimTypeCode = clT.cs1_claimtypecode != null ? clT.cs1_claimtypecode.Trim(new char[] { '\'' }) : "",
//                              ClaimTypeName = clT != null ? clT.cs1_name : "",
//                              CreatedOn = cl.CreatedOn
//                          }).ToList();
//            }

//            return result;
//        }

//        /// <summary>
//        /// Get claims by polisa id
//        /// </summary>
//        /// <param name="polisaId"></param>
//        /// <returns></returns>
//        public List<ClaimModel> GetClaimsByPolisa(Guid polisaId, String misZhut)
//        {
//            List<ClaimModel> result = new List<ClaimModel>();

//            if (string.IsNullOrEmpty(misZhut))
//            {
//                result = (from cl in HcsraContext.cs1_claimSet
//                          join polClaim in HcsraContext.cs1_policies_claimsSet
//                          on cl.cs1_claimId.Value equals polClaim.cs1_claim.Id
//                          join clType in HcsraContext.cs1_claimtypeSet
//                          on cl.cs1_claimtype.Id equals clType.cs1_claimtypeId.Value
//                          where polClaim.cs1_l_mis_polisa.Id == polisaId
//                          select new ClaimModel
//                          {
//                              Id = cl.Id,
//                              ClaimNumber = cl.cs1_name,
//                              ClaimTypeCode = clType.cs1_claimtypecode != null ? clType.cs1_claimtypecode.Trim(new char[] { '\'' }) : "",
//                              IncidentDate = cl.cs1_date,
//                              Contact = cl.cs1_contact,
//                              ClaimClosed = cl.cs1_is_closed,
//                              ClaimStatus = cl.cs1_claimstatus,
//                              ClaimTypeName = clType.cs1_name,
//                              CreatedOn = cl.CreatedOn
//                          }).ToList();
//            }
//            else
//            {
//                result = (from cl in HcsraContext.cs1_claimSet
//                          join contact in HcsraContext.ContactSet
//                          on cl.cs1_contact.Id equals contact.ContactId.Value
//                          join polClaim in HcsraContext.cs1_policies_claimsSet
//                          on cl.cs1_claimId.Value equals polClaim.cs1_claim.Id
//                          join clType in HcsraContext.cs1_claimtypeSet
//                          on cl.cs1_claimtype.Id equals clType.cs1_claimtypeId.Value
//                          where contact.cs1_l_mis_zehut == misZhut
//                          where polClaim.cs1_l_mis_polisa.Id == polisaId
//                          select new ClaimModel
//                          {
//                              Id = cl.Id,
//                              ClaimNumber = cl.cs1_name,
//                              ClaimTypeCode = clType.cs1_claimtypecode != null ? clType.cs1_claimtypecode.Trim(new char[] { '\'' }) : "",
//                              IncidentDate = cl.cs1_date,
//                              Contact = cl.cs1_contact,
//                              ClaimClosed = cl.cs1_is_closed,
//                              ClaimStatus = cl.cs1_claimstatus,
//                              ClaimTypeName = clType.cs1_name,
//                              CreatedOn = cl.CreatedOn
//                          }).ToList();
//            }

//            return result;
//        }

//        /// <summary>
//        /// Get claim record by claim type and polisa
//        /// </summary>
//        /// <param name="claimTypeId">Claim type id</param>
//        /// <param name="polisaId">Polisa id</param>
//        /// <returns>cs1_claim record</returns>
//        public List<ClaimModel> GetClaimRecordByClaimTypeAndPolisa(Guid claimTypeId, Guid polisaId, String misZhut)
//        {
//            List<ClaimModel> result = new List<ClaimModel>();
//            if (string.IsNullOrEmpty(misZhut))
//            {
//                result = (from polClaim in HcsraContext.cs1_policies_claimsSet
//                          join pol in HcsraContext.cs1_polisaSet
//                          on polClaim.cs1_l_mis_polisa.Id equals pol.cs1_polisaId.Value
//                          join claim in HcsraContext.cs1_claimSet
//                          on polClaim.cs1_claim.Id equals claim.cs1_claimId.Value
//                          join claimType in HcsraContext.cs1_claimtypeSet
//                          on claim.cs1_claimtype.Id equals claimType.cs1_claimtypeId.Value
//                          where pol.cs1_polisaId.Value == polisaId
//                          where claimType.cs1_claimtypeId.Value == claimTypeId
//                          select new ClaimModel
//                          {
//                              Id = claim.cs1_claimId.Value,
//                              ClaimNumber = claim.cs1_name,
//                              ClaimTypeCode = claimType.cs1_claimtypecode != null ? claimType.cs1_claimtypecode.Trim(new char[] { '\'' }) : "",
//                              IncidentDate = claim.cs1_date,
//                              Contact = claim.cs1_contact,
//                              CreatedOn = claim.CreatedOn
//                          }).ToList();
//            }
//            else
//            {
//                result = (from polClaim in HcsraContext.cs1_policies_claimsSet
//                          join pol in HcsraContext.cs1_polisaSet
//                          on polClaim.cs1_l_mis_polisa.Id equals pol.cs1_polisaId.Value
//                          join claim in HcsraContext.cs1_claimSet
//                          on polClaim.cs1_claim.Id equals claim.cs1_claimId.Value
//                          join contact in HcsraContext.ContactSet
//                          on claim.cs1_contact.Id equals contact.ContactId.Value
//                          join claimType in HcsraContext.cs1_claimtypeSet
//                          on claim.cs1_claimtype.Id equals claimType.cs1_claimtypeId.Value
//                          where pol.cs1_polisaId.Value == polisaId
//                          where claimType.cs1_claimtypeId.Value == claimTypeId
//                          where contact.cs1_l_mis_zehut == misZhut
//                          select new ClaimModel
//                          {
//                              Id = claim.cs1_claimId.Value,
//                              ClaimNumber = claim.cs1_name,
//                              ClaimTypeCode = claimType.cs1_claimtypecode != null ? claimType.cs1_claimtypecode.Trim(new char[] { '\'' }) : "",
//                              IncidentDate = claim.cs1_date,
//                              Contact = claim.cs1_contact,
//                              CreatedOn = claim.CreatedOn
//                          }).ToList();
//            }

//            return result;
//        }

//        /// <summary>
//        /// Get index rate value by date
//        /// </summary>
//        /// <param name="date"></param>
//        /// <returns></returns>
//        public decimal? GetIndexRateByDate(DateTime date, int indexRateType)
//        {
//            return (from index in HcsraContext.cs1_index_rateSet
//                    where index.cs1_madad.Value == indexRateType
//                    && index.cs1_date <= date
//                    && index.cs1_date > date.AddMonths(-1)
//                    select index.cs1_value.Value).FirstOrDefault();
//        }

//        /// <summary>
//        /// Get the following nomth index rate value by date
//        /// </summary>
//        /// <param name="date"></param>
//        /// <returns></returns>
//        public decimal GetFollowingMonthIndexRateByDate(DateTime date, int indexRateType)
//        {
//            return (from index in HcsraContext.cs1_index_rateSet
//                    where index.cs1_madad.Value == indexRateType
//                    && index.cs1_date >= date
//                    && index.cs1_date < date.AddMonths(1)
//                    select index.cs1_value.Value).FirstOrDefault();
//        }

//        /// <summary>
//        /// Get current index rate
//        /// </summary>
//        /// <param name="date"></param>
//        /// <returns></returns>
//        public decimal GetCurrentIndexRate(int indexRateType)
//        {
//            return (from index in HcsraContext.cs1_index_rateSet
//                    where index.statecode == 0
//                    && index.cs1_madad.Value == indexRateType
//                    select index.cs1_value.Value).FirstOrDefault();
//        }

//        /// <summary>
//        /// מחזיר רשימת פריטי תשלום ומשתתפים לכל התביעה
//        /// </summary>
//        /// <param name="date"></param>
//        /// <returns></returns>
//        public List<PaymentItemModel> GetPaymentsItemsByClaimId(Guid claimId)
//        {
//            return (from paymentItem in HcsraContext.cs1_payment_itemSet
//                    join participent in HcsraContext.cs1_claim_participantSet
//                    on paymentItem.cs1_claim_participant.Id equals participent.cs1_claim_participantId.Value
//                    where paymentItem.cs1_claim.Id == claimId && paymentItem.cs1_status.Value == 2 && paymentItem.cs1_polisa_number != null && paymentItem.cs1_is_send_tifulit == false
//                    where participent.cs1_sug_mutav != null
//                    select new PaymentItemModel
//                    {
//                        Payment = paymentItem,
//                        Paticipent = participent
//                    }).ToList();
//        }

//        public EntityReference GetPolisaFromClaimId(Guid claimId)
//        {
//            return (from polClaim in HcsraContext.cs1_policies_claimsSet
//                    where polClaim.cs1_claim.Id == claimId
//                    select polClaim.cs1_l_mis_polisa)
//                    .FirstOrDefault();
//        }

//        public EntityReference GetClaimTypeByClaimId(Guid claimId)
//        {
//            return (from claim in HcsraContext.cs1_claimSet
//                    where claim.cs1_claimId.Value == claimId
//                    select claim.cs1_claimtype).FirstOrDefault();

//        }

//        public ClaimModel GetClaimTypeAndClaimDateByClaimId(Guid claimId)
//        {
//            return (from claim in HcsraContext.cs1_claimSet
//                    where claim.cs1_claimId.Value == claimId
//                    select new ClaimModel()
//                    {
//                        ClaimType = claim.cs1_claimtype,
//                        IncidentDate = claim.cs1_date.HasValue ? claim.cs1_date.Value : DateTime.MinValue
//                    }).FirstOrDefault();
//        }

//        public Guid GetKisuiByClaimPolicyId(Guid ClaimPolicyId)
//        {

//            return (from kisuiClaimPol in HcsraContext.cs1_kisui_activeSet
//                    join claimpol in HcsraContext.cs1_policies_claimsSet
//                    on kisuiClaimPol.cs1_policies_claims.Id equals claimpol.cs1_policies_claimsId.Value
//                    where claimpol.cs1_policies_claimsId.Value == ClaimPolicyId
//                    select kisuiClaimPol.cs1_kisui_activeId.Value).FirstOrDefault();

//        }

//        public string GetPaymentClauseNameByKisuiId(Guid kisuiId, Guid claimType)
//        {
//            return (from PaymentClause in HcsraContext.cs1_payment_clauseSet
//                    join KisuiClaim in HcsraContext.cs1_kisui_activeSet
//                    on PaymentClause.cs1_kisuino.Id equals KisuiClaim.cs1_sug_bituach.Id
//                    where PaymentClause.cs1_claim_type.Id == claimType
//                    where KisuiClaim.cs1_kisui_activeId.Value == kisuiId
//                    select PaymentClause.cs1_name
//                   ).FirstOrDefault();
//        }

//        public int? GetBankKod(Guid cs1_bankId)
//        {
//            return (from bank in HcsraContext.cs1_bankSet
//                    where bank.cs1_bankId.Value == cs1_bankId
//                    select bank.cs1_kod_bank).FirstOrDefault();
//        }

//        public EntityReference GetClaimTypeByClaimPolicyId(Guid ClaimPolicyId)
//        {
//            return (from claim in HcsraContext.cs1_claimSet
//                    join ClaimPolicy in HcsraContext.cs1_policies_claimsSet
//                    on claim.cs1_claimId.Value equals ClaimPolicy.cs1_claim.Id
//                    where ClaimPolicy.cs1_policies_claimsId.Value == ClaimPolicyId
//                    select claim.cs1_claimtype).FirstOrDefault();
//        }

//        public cs1_claim_participant GetClaimParticipantById(Guid claimParticipantById)
//        {
//            return (from participant in HcsraContext.cs1_claim_participantSet
//                    where participant.cs1_claim_participantId.Value == claimParticipantById
//                    select participant).FirstOrDefault();
//        }

//        public EntityReference GetContactRefByClaimId(Guid claimId)
//        {
//            return (from cl in HcsraContext.cs1_claimSet
//                    where cl.cs1_claimId.Value == claimId
//                    select new EntityReference()
//                    {
//                        Id = cl.cs1_contact.Id,
//                        LogicalName = Contact.EntityLogicalName
//                    })
//                    .FirstOrDefault();
//        }

//        public bool IsClaimClosed(Guid claimId)
//        {
//            return (from cl in HcsraContext.cs1_claimSet
//                    where cl.cs1_claimId.Value == claimId
//                    select new
//                    {
//                        cs1_is_closed = cl.cs1_is_closed.HasValue ? cl.cs1_is_closed.Value : false
//                    })
//                    .FirstOrDefault()
//                    .cs1_is_closed;
//        }

//        public List<ClaimPolicyReleaseDataModel> GetClaimPolisiesByClaimId(Guid claimId)
//        {
//            return (from polClaim in HcsraContext.cs1_policies_claimsSet
//                    join kisuiActive in HcsraContext.cs1_kisui_activeSet
//                    on polClaim.cs1_policies_claimsId equals kisuiActive.cs1_policies_claims.Id
//                    join kisuiNo in HcsraContext.cs1_kisuinoSet
//                    on kisuiActive.cs1_sug_bituach.Id equals kisuiNo.cs1_kisuinoId.Value
//                    where polClaim.cs1_claim.Id == claimId
//                    select new ClaimPolicyReleaseDataModel
//                    {
//                        PolicyId = polClaim.Id,
//                        PolicyName = polClaim.cs1_name,
//                        ClaimId = polClaim.cs1_claim != null ? polClaim.cs1_claim.Id : Guid.Empty,
//                        PolicyClaimId = polClaim.Id
//                    }).ToList();
//        }

//        public int GetClaimTreatmentCounterByClaimId(Guid claimId, int treatmentType, DateTime treatmentDate)
//        {
//            return (from treatmentCounter in HcsraContext.cs1_treatment_counterSet
//                    where treatmentCounter.cs1_claim.Id == claimId
//                    && treatmentCounter.cs1_treatment_type.Value == (int)treatmentType
//                    && treatmentCounter.cs1_treatment_date.Value >= new DateTime(treatmentDate.Year, 1, 1)
//                    && treatmentCounter.cs1_treatment_date.Value < new DateTime(treatmentDate.Year + 1, 1, 1)
//                    select treatmentCounter).ToList().Count();
//        }

//        public int GetClaimTreatmentCounter(Guid claimId)
//        {
//            return (from treatmentCounter in HcsraContext.cs1_treatment_counterSet
//                    where treatmentCounter.cs1_claim.Id == claimId
//                    select treatmentCounter).ToList().Count();
//        }

//        public List<ClaimModel> GetClaimsByCustomerId(Guid contactGuid)
//        {
//            return (from claim in HcsraContext.cs1_claimSet
//                    join claimStatus in HcsraContext.cs1_claimstatusSet
//                    on claim.cs1_claimstatus.Id equals claimStatus.cs1_claimstatusId.Value
//                    join claimType in HcsraContext.cs1_claimtypeSet
//                    on claim.cs1_claimtype.Id equals claimType.cs1_claimtypeId.Value
//                    where claim.cs1_contact.Id == contactGuid
//                    select new ClaimModel()
//                    {
//                        ClaimName = claim.cs1_name,
//                        OwnerName = claim.OwnerId != null ? claim.OwnerId.Name : string.Empty,
//                        IncidentDateValue = claim.cs1_date.HasValue ? claim.cs1_date.Value.ToLocalTime().Day.ToString() + "/" + claim.cs1_date.Value.ToLocalTime().Month.ToString() + "/" + claim.cs1_date.Value.ToLocalTime().Year.ToString() : string.Empty,
//                        ClaimStatusName = claimStatus.cs1_name,
//                        ClaimTypeName = claimType.cs1_name
//                    }).ToList();
//        }

//        public bool IsClaimTypeDeath(cs1_claim claim)
//        {
//            if (claim.cs1_claimtype != null)
//            {
//                string claimtypecode = (from cltp in HcsraContext.cs1_claimtypeSet
//                                        where cltp.cs1_claimtypeId.Value == claim.cs1_claimtype.Id
//                                        select cltp.cs1_claimtypecode).FirstOrDefault();

//                return !string.IsNullOrEmpty(claimtypecode)
//                    && (claimtypecode == "36" // מוות
//                       || claimtypecode == "40" // מוות מתאונה
//                       || claimtypecode == "43"); // מוות מתאונת דרכים

//            }

//            return false;
//        }

//        public int? GetInsuranceProductBypolicyProductId(Guid policyProductId)
//        {
//            return (from insuranceProduct in HcsraContext.cs1_insurance_productSet
//                    where insuranceProduct.cs1_insurance_productId.Value == policyProductId
//                    select insuranceProduct.cs1_product_id).FirstOrDefault();
//        }

//        public List<cs1_policies_claims> GetPoliciesClaimFromClaim(cs1_claim claim)
//        {
//            return (from policy in HcsraContext.cs1_policies_claimsSet
//                    where policy.cs1_claim.Id == claim.cs1_claimId.Value
//                    select policy).ToList();
//        }

//        public void SetClaimEmployeeClass(string claimId, string empClass)
//        {


//            cs1_claim cl = (from claim in HcsraContext.cs1_claimSet
//                            where claim.cs1_claimId == new Guid(claimId)
//                            select claim).FirstOrDefault();

//            cl.cs1_employee_class = new OptionSetValue(Convert.ToInt16(empClass));
//            Update(cl);

//        }

//        public ClaimPrintingModel GetClaimPrintingDetails(Guid claimId)
//        {
//            return (from claim in HcsraContext.cs1_claimSet
//                    where claim.cs1_claimId.Value == claimId
//                    select new ClaimPrintingModel
//                    {
//                        //@@ מספר תביעה
//                        Name = claim.cs1_name,

//                        //@@ מבוטח
//                        Contact = claim.cs1_contact.Name,

//                        //@@ סוג תביעה
//                        ClaimType = claim.cs1_claimtype.Name,

//                        ////@@ בעלים
//                        Owner = claim.OwnerId.Name,

//                        ////@@ מס ת.ז.
//                        IdNumber = claim.cs1_mis_zehut,

//                        ////@@ סטטוס
//                        ClaimStatus = claim.cs1_claimstatus.Name,

//                        ////@@ תאריך דיווח
//                        GetClaimDate = (claim.cs1_date_getclaim != null && claim.cs1_date_getclaim.HasValue) ? claim.cs1_date_getclaim.Value.ToString("dd/MM/yyyy") : String.Empty,

//                        ////@@ תאריך לידה
//                        BirthDate = (claim.cs1_l_tar_leida != null && claim.cs1_l_tar_leida.HasValue) ? claim.cs1_l_tar_leida.Value.ToString("dd/MM/yyyy") : String.Empty,

//                        ////@@ תאריך אירוע
//                        EventDate = (claim.cs1_date != null && claim.cs1_date.HasValue) ? claim.cs1_date.Value.ToString("dd/MM/yyyy") : String.Empty,
//                    }).SingleOrDefault();
//        }

//        public List<string> GetPolicyNumbersByClaimId(Guid claimId)
//        {
//            return (from polClaim in HcsraContext.cs1_policies_claimsSet
//                    join kisuiActive in HcsraContext.cs1_kisui_activeSet
//                    on polClaim.cs1_policies_claimsId equals kisuiActive.cs1_policies_claims.Id
//                    join kisuiNo in HcsraContext.cs1_kisuinoSet
//                    on kisuiActive.cs1_sug_bituach.Id equals kisuiNo.cs1_kisuinoId.Value
//                    where polClaim.cs1_claim.Id == claimId
//                    select polClaim.cs1_l_mis_polisa.Name).ToList();
//        }

//        public List<ClaimAnnotationPrintingModel> GetPolicyAnnotationsByClaimId(Guid claimId)
//        {
//            var Annotations = this.GetAnnotationsByObjectId(claimId);

//            return (from annotation in Annotations
//                    select new ClaimAnnotationPrintingModel
//                    {
//                        CreatedBy = annotation.CreatedBy.Name,
//                        CreatedOn = (annotation.CreatedOn != null && annotation.CreatedOn.HasValue) ? annotation.CreatedOn.Value.ToString("dd/MM/yyyy") : String.Empty,
//                        Description = annotation.NoteText,
//                    }).ToList();
//        }

//        private List<Annotation> GetAnnotationsByObjectId(Guid objectId)
//        {
//            return (from annotation in HcsraContext.AnnotationSet
//                    where annotation.ObjectId.Id == objectId
//                    orderby annotation.CreatedOn descending
//                    select annotation).ToList();
//        }


//        public DateTime SetClaimGetDate(string claimId, DateTime date)
//        {
//            DateTime oldValue;
//            cs1_claim cl = (from claim in HcsraContext.cs1_claimSet
//                            where claim.cs1_claimId == new Guid(claimId)
//                            select claim).FirstOrDefault();

//            oldValue = cl.cs1_date_getclaim.Value;
//            cl.cs1_date_getclaim = date;
//            Update(cl);
//            return oldValue;
//        }

//        public List<ClaimBasicVM> GetClaimsByContact(Guid contactGuid)
//        {
//            return (from claim in HcsraContext.cs1_claimSet
//                    where claim.cs1_contact.Id == contactGuid
//                    where claim.statecode == cs1_claimState.Active
//                    select new ClaimBasicVM()
//                    {
//                        entityId = claim.Id.ToString(),
//                        owner = claim.cs1_contact.Name != null ? claim.cs1_contact.Name : string.Empty,
//                        name = claim.cs1_name,
//                        status = claim.cs1_claimstatus != null ? claim.cs1_claimstatus.Name : string.Empty,
//                        createdby = claim.CreatedBy != null ? claim.CreatedBy.Name : string.Empty,
//                        date = claim.CreatedOn.HasValue ? claim.CreatedOn.Value : new DateTime(0),
//                        type = claim.cs1_claimtype != null ? claim.cs1_claimtype.Name : string.Empty

//                    }).ToList();
//        }
//    }
//}