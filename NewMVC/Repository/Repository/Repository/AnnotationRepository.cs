﻿using CRM.Hcsra.Repository;
using Microsoft.Xrm.Sdk.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRM.Hcsra.DataModel;
using Microsoft.Xrm.Sdk;
using DataModel.Models;
using Microsoft.Crm.Sdk.Messages;

namespace Repository.Repository
{
   public class AnnotationRepository : RepositoryBase
    {
        public AnnotationRepository(OrganizationServiceProxy service = null)
          : base(service)
        {

        }

        public Guid CreateAnnotation(Annotation anno)
        {
           return Create(anno);
        }

        public Annotation getAnnotationByRelID(Guid id)
        {
           return (from ann in HcsraContext.AnnotationSet
                  where ann.ObjectId.Id == id
                  select ann).FirstOrDefault();
        }
    }
}
