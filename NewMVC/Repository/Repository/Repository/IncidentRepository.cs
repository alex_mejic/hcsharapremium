﻿using CRM.Hcsra.Repository;
using Microsoft.Xrm.Sdk.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRM.Hcsra.DataModel;
using Microsoft.Xrm.Sdk;
using DataModel.Models;
using Microsoft.Crm.Sdk.Messages;
using static DataModel.Models.Enums.DetectEnums;
using System.Configuration;

namespace Repository.Repository
{
   public class IncidentRepository : RepositoryBase
    {
        public IncidentRepository(OrganizationServiceProxy service = null)
          : base(service)
        {

        }



    

        public MVCResponse GetIncidentsByContact(Guid contactGuid)
        {
            MVCResponse resp = new MVCResponse();
            var items = (from inc in HcsraContext.IncidentSet
                         where inc.CaseOriginCode != null
                         where inc.CustomerId.Id == contactGuid &&
                         inc.StateCode == IncidentState.Active
                         && inc.CaseOriginCode.Value == 861650005
                         select new
                         {
                             incidentId = inc.IncidentId.Value != null ? inc.IncidentId.Value : Guid.Empty,
                             cs1_status = inc.cs1_status != null ? inc.cs1_status.Name : string.Empty,
                             createdby = inc.CreatedBy != null ? inc.CreatedBy.Name : string.Empty,
                             createdon = inc.CreatedOn.HasValue ? inc.CreatedOn.Value : new DateTime(0),
                             cs1_tat_status = inc.cs1_tat_status != null ? inc.cs1_tat_status.Name : string.Empty,
                             ownerid = inc.OwnerId != null ? inc.OwnerId.Name : string.Empty,
                             title = inc.Title,
                             PolisotNumbers = (from incidentToPolisaSet in HcsraContext.cs1_incident_cs1_polisaSet
                                               where (incidentToPolisaSet.incidentid == inc.IncidentId)
                                               select incidentToPolisaSet.cs1_polisaid).Distinct().ToList(),
                             editmodel = new
                             {
                                 ambulatory = inc.cs1_is_ambulatory.Value == (int)Ambulatory.ambulatory ? true : false,
                                 //ambulatoryReason = inc.cs1_ambulatory_type != null ? inc.FormattedValues["cs1_ambulatory_type"] : string.Empty,
                                 //otherReason = inc.cs

                                 note = (from note in HcsraContext.AnnotationSet
                                         where note.ObjectId.Id == inc.Id
                                          && (note.Subject.Contains("פירוט טיפול אמבולאטורי") || note.Subject.Contains("נושא הפנייה"))
                                         select new
                                         {
                                             ID = note.Id,
                                             Text = note.NoteText
                                         }).FirstOrDefault(),
                                 OwnerName = inc.OwnerId.Name,

                                 FileNames = (from note in HcsraContext.AnnotationSet
                                              where note.ObjectId.Id == inc.Id
                                              && note.FileName != null
                                              select new
                                              {
                                                  FileName = note.FileName,
                                                  CreatedOn = note.CreatedOn.Value,
                                                  Subject = note.Subject
                                              }).ToList()
                             }


                         }).ToList();
                        resp.Data.Add("Incidents", items);


            return resp;

        }

        public string updateIncTeam(Guid guid)
        {


            Incident inc = GetIncidentById(guid);
            if (inc == null)
                return "אין אפשרות לאתר אירוע";


            string username =  ConfigurationManager.AppSettings["user_to_assign_to"];
            string teamname = ConfigurationManager.AppSettings["team_to_assign_to"];

            if (!string.IsNullOrEmpty(username))
            {
                //return "שם משתמם לבעלות חסר בקובץ הגדרות";

                var id = (from u in HcsraContext.SystemUserSet
                          where u.DomainName.Contains(username)
                          select u.Id).FirstOrDefault();

                if (id != Guid.Empty)
                {
                    string logname = SystemUser.EntityLogicalName;
                    AssignTo(inc, id, logname);
                }

            }
            else if (!string.IsNullOrEmpty(teamname))
            {
                var id = (from u in HcsraContext.TeamSet
                          where u.Name.Contains(teamname)
                          select u.Id).FirstOrDefault();

                if (id != Guid.Empty)
                {
                    string logname = Team.EntityLogicalName;
                    AssignTo(inc, id, logname);

                }
            }else
            {
                return "שם משתמם או קבוצה לבעלות חסרים בקובץ הגדרות";
            }


            return string.Empty;
        }

        public MVCResponse GetlatestIncidentsPremium()
        {
            MVCResponse resp = new MVCResponse();
            var items = (from inc in HcsraContext.IncidentSet
                         where inc.CaseOriginCode != null &&
                         inc.StateCode == IncidentState.Active
                         && inc.CaseOriginCode.Value == 861650005
                         select new
                         {
                             incidentId = inc.IncidentId.Value != null ? inc.IncidentId.Value : Guid.Empty,
                             cs1_status = inc.cs1_status != null ? inc.cs1_status.Name : string.Empty,
                             createdby = inc.CreatedBy != null ? inc.CreatedBy.Name : string.Empty,
                             createdon = inc.CreatedOn.HasValue ? inc.CreatedOn.Value : new DateTime(0),
                             cs1_tat_status = inc.cs1_tat_status != null ? inc.cs1_tat_status.Name : string.Empty,
                             ownerid = inc.OwnerId != null ? inc.OwnerId.Name : string.Empty,
                             title = inc.Title,
                             PolisotNumbers = (from incidentToPolisaSet in HcsraContext.cs1_incident_cs1_polisaSet
                                               where (incidentToPolisaSet.incidentid == inc.IncidentId)
                                               select incidentToPolisaSet.cs1_polisaid).Distinct().ToList(),
                             editmodel = new
                             {
                                 ambulatory = inc.cs1_is_ambulatory.Value == (int)Ambulatory.ambulatory ? true : false,
                                 //ambulatoryReason = inc.cs1_ambulatory_type != null ? inc.FormattedValues["cs1_ambulatory_type"] : string.Empty,
                                 //otherReason = inc.cs

                                 note = (from note in HcsraContext.AnnotationSet
                                         where note.ObjectId.Id == inc.Id
                                          && (note.Subject.Contains("פירוט טיפול אמבולאטורי") || note.Subject.Contains("נושא הפנייה"))
                                         select new {
                                             ID = note.Id,
                                             Text = note.NoteText
                                         }).FirstOrDefault(),
                                 OwnerName = inc.OwnerId.Name,

                                 FileNames = (from note in HcsraContext.AnnotationSet
                                              where note.ObjectId.Id == inc.Id
                                              && note.FileName != null
                                              select new
                                              {
                                                  FileName = note.FileName,
                                                  CreatedOn = note.CreatedOn.Value,
                                                  Subject = note.Subject
                                              }).ToList()
                             }


                         }).ToList();
            resp.Data.Add("Incidents", items);


            return resp;
        }

        private void AssignTo(Incident inc, Guid id, string logname)
        {
            AssignRequest assignRequest = new AssignRequest()
            {
                Assignee = new EntityReference
                {
                    LogicalName = logname,
                    Id = id
                },

                Target = new EntityReference(Incident.EntityLogicalName, inc.Id)
            };
            Execute(assignRequest);
        }

        public Guid CreateIncident(Guid contactId, IncidentNewVM model)
        {
            Incident incNew = new Incident();
            incNew.CustomerId = new EntityReference(Contact.EntityLogicalName, contactId);


            if (model.ambulatory == true)
            {
                incNew.cs1_is_ambulatory = new OptionSetValue((int)Ambulatory.ambulatory);
            }
            else
            {
                incNew.cs1_is_ambulatory = new OptionSetValue((int)Ambulatory.not_ambulatory);
            }

            incNew.CaseOriginCode = new OptionSetValue((int)IncOrigin.מוקד_פמי_פרימיום);

            var item = HcsraContext.SubjectSet
                    .Where(x => x.Title.Contains("פמי פרימיום"))
                    .FirstOrDefault();

            if (item != null)
            {
                incNew.SubjectId = new EntityReference(Subject.EntityLogicalName, item.SubjectId.Value);
            }

            Guid created = Create(incNew);

            #region assign to team
            var team = HcsraContext.TeamSet
                     .Where(x => x.Name.Contains("פמי פרימיום"))
                     .FirstOrDefault();

            if (team != null)
            {
                AssignRequest assignRequest = new AssignRequest()
                {
                    Assignee = new EntityReference
                    {
                        LogicalName = Team.EntityLogicalName,
                        Id = team.Id
                    },

                    Target = new EntityReference(Incident.EntityLogicalName, created)
                };
                Execute(assignRequest);
            } 
            #endregion

            return created;
        }

        public Incident GetIncidentById(Guid guid)
        {
            return (from inc in HcsraContext.IncidentSet
                    where inc.Id == guid
                    select inc).FirstOrDefault();
        }
    }
}
