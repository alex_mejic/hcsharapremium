﻿using System;
using System.Linq;
using Microsoft.Xrm.Sdk.Client;
using E4D.Apps.Dev.Utils.CRM;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using System.Globalization;
using CRM.Hcsra.WebConfigDictionary;
using System.Collections.Generic;

namespace CRM.Hcsra.Repository
{
    public class RepositoryBase : IDisposable
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="service"></param>
        public RepositoryBase(OrganizationServiceProxy service = null)
        {
            if (service == null)
            {
                Crm5ServiceCreator crm5ServiceCreator = new Crm5ServiceCreator();
                this.service = crm5ServiceCreator.GetCrmService();
            }
            else
            {
                this.service = service;
            }
        }

        /// <summary>
        /// Crm Organization service field
        /// </summary>
        private OrganizationServiceProxy service = null;

        /// <summary>
        /// Crm Organization service property
        /// </summary>
        public OrganizationServiceProxy Service
        {
            get
            {
                return service;
            }
        }

        /// <summary>
        /// Hcsra Crm Context field
        /// </summary>
        private HcsraCrmContext hcsraContext = null;

        /// <summary>
        /// Hcsra Crm Context field property
        /// </summary>
        public HcsraCrmContext HcsraContext
        {
            get
            {
                if (hcsraContext == null)
                {
                    hcsraContext = new HcsraCrmContext(Service);
                    hcsraContext.MergeOption = MergeOption.NoTracking;
                }

                return hcsraContext;
            }
            set
            {
                hcsraContext = value;
            }
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="er"></param>
        public void Delete(EntityReference er)
        {
            Service.Delete(er.LogicalName, er.Id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entityName"></param>
        /// <param name="entityID"></param>
        public void Delete(String entityName, Guid entityID)
        {
            Service.Delete(entityName, entityID);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        public void Update(Entity entity)
        {
            Service.Update(entity);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Guid Create(Entity entity)
        {
            return Service.Create(entity);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="organizationRequest"></param>
        /// <returns></returns>
        public OrganizationResponse Execute(OrganizationRequest organizationRequest)
        {
            return Service.Execute(organizationRequest);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public EntityCollection RetrieveMultiple(QueryBase query)
        {
            return Service.RetrieveMultiple(query);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entityName"></param>
        /// <param name="entityGuid"></param>
        /// <param name="attributes"></param>
        /// <returns></returns>
        public Entity Retrieve(String entityName, Guid entityGuid, String[] attributes)
        {
            ColumnSet columnSet = attributes.Count() > 0 ? new ColumnSet(attributes) : new ColumnSet(true);
            return Service.Retrieve(entityName, entityGuid, columnSet);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            HcsraContext.Dispose();
            this.service = null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public EntityReference GetCurrentUser()
        {
            return (from user in HcsraContext.SystemUserSet
                    where user.DomainName == GetDomainUserNameFromWindowsIdentity()
                    select new EntityReference
                    {
                        LogicalName = SystemUser.EntityLogicalName,
                        Name = user.FullName,
                        Id = user.Id
                    }).FirstOrDefault();
        }

        public string GetUserNameFromWindowsIdentity()
        {
            string[] windowsIdentity = null;
            if (System.Security.Principal.WindowsIdentity.GetCurrent() != null &&
               !String.IsNullOrEmpty(System.Security.Principal.WindowsIdentity.GetCurrent().Name))
            {
                windowsIdentity = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Split(new string[] { "\\" }, StringSplitOptions.RemoveEmptyEntries);
            }

            return windowsIdentity != null && windowsIdentity.Count() > 0 ? windowsIdentity[1] : WebConfigKeyValue.crmOwner;
        }

        public static string GetDomainUserNameFromWindowsIdentity()
        {
            if (System.Security.Principal.WindowsIdentity.GetCurrent() != null &&
               !String.IsNullOrEmpty(System.Security.Principal.WindowsIdentity.GetCurrent().Name))
            {
                return System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            }

            return WebConfigKeyValue.crmDomain + "\\" + WebConfigKeyValue.crmOwner;
        }     

        /// <summary>
        /// 
        /// </summary>
        /// <param name="assignee"></param>
        /// <param name="target"></param>
        public void AssignRequest(EntityReference assignee, EntityReference target)
        {
            AssignRequest newOwnerRequest = new AssignRequest();
            newOwnerRequest.Assignee = assignee;
            newOwnerRequest.Target = target;

            Execute(newOwnerRequest);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entityName"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public PicklistAttributeMetadata RetrievePickListAttributeMetaData(string entityName, string fieldName)
        {
            RetrieveAttributeRequest attrReq = new RetrieveAttributeRequest
            {
                EntityLogicalName = entityName,
                LogicalName = fieldName,
                RetrieveAsIfPublished = true
            };

            PicklistAttributeMetadata optionSetMeta = RetriveOptionSetMetadata(attrReq);
            return optionSetMeta;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="attrReq"></param>
        /// <returns></returns>
        public PicklistAttributeMetadata RetriveOptionSetMetadata(RetrieveAttributeRequest attrReq)
        {
            RetrieveAttributeResponse attrResp = Service.Execute(attrReq) as RetrieveAttributeResponse;
            AttributeMetadata attrMeta = attrResp.AttributeMetadata;
            PicklistAttributeMetadata optionSetMeta = (PicklistAttributeMetadata)attrMeta;
            return optionSetMeta;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="optionsetMetaData"></param>
        /// <param name="optionValue"></param>
        /// <returns></returns>
        public String RetrieveOptionSetName(PicklistAttributeMetadata optionsetMetaData, OptionSetValue optionValue)
        {
            if (optionValue != null)
            {
                foreach (OptionMetadata optionMeta in optionsetMetaData.OptionSet.Options)
                {
                    if (optionMeta.Value == optionValue.Value)
                    {
                        return optionMeta.Label.UserLocalizedLabel.Label;
                    }
                }
            }
            return String.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="relatedEntities"></param>
        /// <param name="entity"></param>
        /// <param name="relationshipName"></param>
        /// <param name="sameEntityType"></param>
        public void AssociateEntitiesRequest(EntityReferenceCollection relatedEntities, EntityReference entity, string relationshipName, bool sameEntityType = false)
        {
            AssociateRequest request = new AssociateRequest
            {
                RelatedEntities = relatedEntities,
                Relationship = new Relationship(relationshipName),
                Target = entity
            };

            if (sameEntityType) { request.Relationship.PrimaryEntityRole = EntityRole.Referenced; }

            Execute(request);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entityLogicalName"></param>
        /// <param name="logicalName"></param>
        /// <param name="retrieveAsIfPublished"></param>
        /// <param name="selectedValue"></param>
        /// <returns></returns>
        public string HandleGetAttribute(string entityLogicalName, string logicalName, bool retrieveAsIfPublished, int selectedValue)
        {
            RetrieveAttributeRequest attributeRequest = new RetrieveAttributeRequest
            {
                EntityLogicalName = entityLogicalName,
                LogicalName = logicalName,
                RetrieveAsIfPublished = retrieveAsIfPublished
            };
            RetrieveAttributeResponse attributeResponse = (RetrieveAttributeResponse)Execute(attributeRequest);
            PicklistAttributeMetadata retrievedPicklistAttributeMetadata = (PicklistAttributeMetadata)attributeResponse.AttributeMetadata;
            OptionMetadata[] optionList = retrievedPicklistAttributeMetadata.OptionSet.Options.ToArray();
            string selectedOptionLabel = string.Empty;

            foreach (OptionMetadata oMD in optionList)
            {
                if (oMD.Value == selectedValue)
                {
                    selectedOptionLabel = oMD.Label.UserLocalizedLabel.Label;
                    break;
                }
            }

            return selectedOptionLabel;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fetchXml"></param>
        /// <returns></returns>
        public EntityCollection RetrieveMultiple(string fetchXml)
        {
            return RetrieveMultiple(new FetchExpression(fetchXml));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="recordId"></param>
        /// <returns></returns>
        public Entity GetRelatedQitem(Guid recordId)
        {
            Entity queue = (from q in HcsraContext.QueueItemSet
                            where q.ObjectId.Id == recordId
                            select q).FirstOrDefault();
            return queue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="emailId"></param>
        public void SendEmailRequest(Guid emailId)
        {
            SendEmailRequest sendEmailreq = new SendEmailRequest
            {
                EmailId = emailId,
                TrackingToken = "",
                IssueSend = true
            };

            SendEmailResponse sendEmailresp = (SendEmailResponse)Execute(sendEmailreq);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="target"></param>
        /// <param name="relatedEntities"></param>
        /// <param name="relationship"></param>
        public void AssociateRequest(EntityReference target, EntityReferenceCollection relatedEntities, String relationship)
        {
            AssociateRequest entityToEntity = new AssociateRequest
            {
                Target = target,
                RelatedEntities = relatedEntities,
                Relationship = new Relationship(relationship)
            };

            Service.Execute(entityToEntity);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="target"></param>
        /// <param name="relatedEntities"></param>
        /// <param name="relationship"></param>
        public void DisassociateRequest(EntityReference target, EntityReferenceCollection relatedEntities, String relationship)
        {
            DisassociateRequest entityToEntity = new DisassociateRequest
            {
                Target = target,
                RelatedEntities = relatedEntities,
                Relationship = new Relationship(relationship)
            };

            Execute(entityToEntity);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <param name="newValue"></param>
        /// <returns></returns>
        public T GetValueOrDefaultByUser<T>(T value, T newValue)
        {
            if (value == null)
            {
                return newValue;
            }

            return value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="currencyName"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public Guid GetCurrencyIdByName(string currencyName)
        {
            Guid currencyGuid = (from curr in HcsraContext.TransactionCurrencySet
                                 where curr.CurrencyName == currencyName
                                 select curr.Id).FirstOrDefault();
            return currencyGuid;
        }

        public void SetEntityState(EntityReference entityReference, int state, int status)
        {
            SetStateRequest setStateRequest = new SetStateRequest()
                            {
                                EntityMoniker = entityReference,
                                State = new OptionSetValue(state),
                                Status = new OptionSetValue(status),
                            };

            this.Execute(setStateRequest);
        }

        /// <summary>
        /// Get current index rate
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public decimal GetCurrentIndexRate(int indexRateType)
        {
            return (from index in HcsraContext.cs1_index_rateSet
                    where index.statecode == 0
                    && index.cs1_madad.Value == indexRateType
                    select index.cs1_value.Value).FirstOrDefault();
        }

        /// <summary>
        /// Get index rate value by date
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public decimal? GetIndexRateByDate(DateTime date, int indexRateType)
        {
            return (from index in HcsraContext.cs1_index_rateSet
                    where index.cs1_madad.Value == indexRateType
                    && index.cs1_date <= date
                    && index.cs1_date > date.AddMonths(-1)
                    select index.cs1_value.Value).FirstOrDefault();
        }

        /// <summary>
        /// Get the following nomth index rate value by date
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public decimal GetFollowingMonthIndexRateByDate(DateTime date, int indexRateType)
        {
            return (from index in HcsraContext.cs1_index_rateSet
                    where index.cs1_madad.Value == indexRateType
                    && index.cs1_date >= date
                    && index.cs1_date < date.AddMonths(1)
                    select index.cs1_value.Value).FirstOrDefault();
        }

        public NumberFormatInfo GetNumberFormatInfo(int currencyPrecision = 2, int numberPrecision = 2)
        {
            NumberFormatInfo numberFormatInfo = new NumberFormatInfo();
            numberFormatInfo.CurrencyDecimalDigits = currencyPrecision;
            numberFormatInfo.NumberDecimalDigits = numberPrecision;
            return numberFormatInfo;
        }

        public List<CalendarRule> GetAllCalendarRules()
        {
            return (from workDay in HcsraContext.CalendarSet
                    where workDay.Description == "Calendar for Business Closure"
                    select workDay)
                            .FirstOrDefault()
                            .CalendarRules
                            .ToList();
        }

        /// <summary>
        /// Checks weather the given year month and day are a calendar working day
        /// </summary>
        /// <param name="allCalendarRules">Holds all the calendar rules</param>
        /// <param name="year">Holds the year of the working day query</param>
        /// <param name="month">Holds the month of the working day query</param>
        /// <param name="day">Holds the day of the working day query</param>
        /// <returns>True if it is a working day. False if it is not</returns>
        public bool IsWorkingDay(List<CalendarRule> allCalendarRules, int year, int month, int day)
        {
            if (IsWeekend(new DateTime(year, month, day)))
            {
                return false;
            }

            var work = (from workDay in allCalendarRules
                        where workDay.StartTime.Value.Year == year &&
                        workDay.StartTime.Value.Month == month &&
                        workDay.StartTime.Value.Day == day
                        select workDay).FirstOrDefault();

            return work == null;
        }

        /// <summary>
        /// Checks weather the given year month and day, after adding the daysForward are a calendar working day
        /// </summary>
        /// <param name="allCalendarRules">Holds all the calendar rules</param>
        /// <param name="year">Holds the year of the working day query</param>
        /// <param name="month">Holds the month of the working day query</param>
        /// <param name="day">Holds the day of the working day query</param>
        /// <param name="daysForward">Holds the number of working days forward which the query will work on</param>
        /// <returns>True if it is a working day. False if it is not</returns>
        public bool IsWorkingDayWithDaysForward(List<CalendarRule> allCalendarRules, int year, int month, int day, int daysForward)
        {
            DateTime date = new DateTime(year, month, day);
            for (int i = 1; i <= daysForward; ++i)
            {
                bool isWorkingDay = false;
                date = date.AddDays(1);
                isWorkingDay = IsWorkingDay(allCalendarRules, date.Year, date.Month, date.Day);
                while (!isWorkingDay)
                {
                    date = date.AddDays(1);
                    isWorkingDay = IsWorkingDay(allCalendarRules, date.Year, date.Month, date.Day);
                }
            }

            if (IsWeekend(date))
            {
                return false;
            }

            var work = (from workDay in allCalendarRules
                        where workDay.StartTime.Value.Year == date.Year &&
                        workDay.StartTime.Value.Month == date.Month &&
                        workDay.StartTime.Value.Day == date.Day
                        select workDay).FirstOrDefault();

            return work == null;
        }

        /// <summary>
        /// Returns an array with the not working days of the given month
        /// </summary>
        /// <param name="allCalendarRules">Holds all the calendar rules</param>
        /// <param name="year">Holds the year of the working day query</param>
        /// <param name="month">Holds the month of the working day query</param>
        /// <returns>Array of ints with the not working days of the given month</returns>
        public List<int> NotWorkingDaysOfTheMonth(List<CalendarRule> allCalendarRules, int year, int month)
        {
            // Create a new list for the result
            List<int> result = new List<int>();
            // Loop over the days of the month starting the first of the month
            for (int i = 1; i <= DateTime.DaysInMonth(year, month); ++i)
            {
                // Check if the current element in the loop is NOT a working day
                if (!IsWorkingDay(allCalendarRules, year, month, i))
                {
                    // Add the element to the result array
                    result.Add(i);
                }
            }

            return result;
        }

        /// <summary>
        /// Checks how many working days left until the end of the month
        /// </summary>
        /// <param name="allCalendarRules">Holds all the calendar rules</param>
        /// <returns>The number of working days left to the end of the month</returns>
        public int WorkingDaysUntilEndOfTheMonth(List<CalendarRule> allCalendarRules)
        {
            // Get today date
            DateTime today = DateTime.UtcNow;
            // Create a new list for the result
            List<int> result = new List<int>();
            // Loop over the days of the month starting today
            for (int i = today.Day; i <= DateTime.DaysInMonth(today.Year, today.Month); ++i)
            {
                // Check if the current day in the loop is a working day
                if (IsWorkingDay(allCalendarRules, today.Year, today.Month, i))
                {
                    // Add the working day to the rseult array
                    result.Add(i);
                }
            }

            // Return the number of working days
            return result.Count;
        }

        /// <summary>
        /// Checks if the given date is a weekend or not
        /// </summary>
        /// <param name="date">Hold the given date of the query</param>
        /// <returns>True if this is a weekend. False if it is not</returns>
        private bool IsWeekend(DateTime date)
        {
            //return date.DayOfWeek == DayOfWeek.Friday || date.DayOfWeek == DayOfWeek.Saturday;
            return false;
        }

        public cs1_parameter RetriveParameterEntity()
        {
            return (from parameter in HcsraContext.cs1_parameterSet
                    select parameter).FirstOrDefault();
        }

        public Guid? GetSubjectIdBySubjectName(string subjectName)
        {
            return (from subject in HcsraContext.SubjectSet
                    where subject.Title == subjectName
                    select subject.Id).FirstOrDefault();
        }

        public Guid? GetIncidentQuickResponseOwnerFromParameters()
        {
            return (from parameter in HcsraContext.cs1_parameterSet
                    select parameter.cs1_incident_quick_response.Id).FirstOrDefault();
        }

        public Guid? GetStatusByName(string statusName)
        {
            return (from status in HcsraContext.cs1_statusSet
                    where status.cs1_name == statusName
                    select status.Id).FirstOrDefault();
        }

        public Guid? GetTatStatusByName(string tatStatusName)
        {
            return (from tatStatus in HcsraContext.cs1_tat_statusSet
                    where tatStatus.cs1_name == tatStatusName
                    select tatStatus.Id).FirstOrDefault();
        }
    }
}
