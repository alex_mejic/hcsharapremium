﻿using System;
using Microsoft.Xrm.Sdk;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Messages;
using System.Linq;
using System.Text;
using System.IO;
using Microsoft.Xrm.Sdk.Metadata;
using System.Net;
using System.Globalization;
using CRM.Hcsra.WebConfigDictionary;

namespace E4D.Hcsra.WebResources.BL.CommonHelpers
{
    public static class Manager
    {
        public static Guid GetIdAsGuidOrDefault(string claimId)
        {
            Guid claimGuid;
            if (String.IsNullOrEmpty(claimId)) { claimGuid = new Guid("FA3C7409-3674-E211-AE62-005056A30870"); }
            else { claimGuid = new Guid(claimId); }
            return claimGuid;
        }

        public static void UpdateState(Guid entityForUpdate, string statuscode, IOrganizationService service, int state, string entityName, string logicalName)
        {
            SetStateRequest setState = new SetStateRequest();
            setState.EntityMoniker = new EntityReference();
            setState.EntityMoniker.Id = entityForUpdate;
            setState.EntityMoniker.Name = entityName;
            setState.EntityMoniker.LogicalName = logicalName;
            setState.State = new OptionSetValue();
            setState.State.Value = state;
            if (statuscode != null && statuscode != "")
            {
                setState.Status = new OptionSetValue();
                setState.Status.Value = int.Parse(statuscode);
            }
            else
            {
                setState.Status = new OptionSetValue();
                setState.Status.Value = -1;
            }
            SetStateResponse setStateResponse = (SetStateResponse)service.Execute(setState);
        }

        //get the selected text value from the optionSet
        public static string GetOptionsSetTextByValue(IOrganizationService service, string entityName, string attributeName, int selectedValue)
        {
            RetrieveAttributeRequest retrieveAttributeRequest = new RetrieveAttributeRequest
            {
                EntityLogicalName = entityName,
                LogicalName = attributeName,
                RetrieveAsIfPublished = true
            };

            // Execute the request.
            RetrieveAttributeResponse retrieveAttributeResponse = (RetrieveAttributeResponse)service.Execute(retrieveAttributeRequest);
            // Access the retrieved attribute.
            PicklistAttributeMetadata retrievedPicklistAttributeMetadata = (PicklistAttributeMetadata)retrieveAttributeResponse.AttributeMetadata;// Get the current options list for the retrieved attribute.
            OptionMetadata[] optionList = retrievedPicklistAttributeMetadata.OptionSet.Options.ToArray();
            string selectedOptionLabel = string.Empty;
            foreach (OptionMetadata oMD in optionList)
            {
                if (oMD.Value == selectedValue)
                {
                    selectedOptionLabel = oMD.Label.UserLocalizedLabel.Label;
                    break;
                }
            }

            return selectedOptionLabel;
        }//private string GetOptionsSetTextOnValue

        public static string WebFacadeCall(string postData, string requestUri)
        {
            string webFacadeServiceUrl = WebConfigKeyValue.webFacadeServiceUrl;
            HttpWebRequest httpWeb = (HttpWebRequest)WebRequest.Create(webFacadeServiceUrl + requestUri);
            httpWeb.ContentType = "application/json";
            httpWeb.Method = "POST";
            UTF8Encoding encoding = new UTF8Encoding();
            byte[] data = encoding.GetBytes(postData);
            httpWeb.ContentLength = data.Length;
            Stream newStream = httpWeb.GetRequestStream();
            newStream.Write(data, 0, data.Length);
            newStream.Close();
            HttpWebResponse webResponse = (HttpWebResponse)httpWeb.GetResponse();
            UTF8Encoding enc = new UTF8Encoding();
            StreamReader responseStream = new StreamReader(webResponse.GetResponseStream(), enc);
            string response = responseStream.ReadToEnd();
            responseStream.Close();
            webResponse.Close();

            return response.ToString();
        }

        public static T ConvertIfDbNullTo<T>(object fieldData)
        {
            if (DBNull.Value.Equals(fieldData))
            {
                return default(T);
            }
            else
            {
                if (fieldData.ToString() != "")
                {
                    return (T)Convert.ChangeType(fieldData, typeof(T));
                }
                else
                {
                    return default(T);
                }
            }
        }

        public static T ConvertIfDbNullTo<T>(object fieldData, T defaultValue)
        {
            T val = ConvertIfDbNullTo<T>(fieldData);
            if (val.Equals(default(T)))
                return defaultValue;
            return val;
        }

        public static string GetUserNameFromWindowsIdentity()
        {
            string[] windowsIdentity = null;
            if (System.Security.Principal.WindowsIdentity.GetCurrent() != null &&
               !String.IsNullOrEmpty(System.Security.Principal.WindowsIdentity.GetCurrent().Name))
            {
                windowsIdentity = System.Security.Principal.WindowsIdentity.GetCurrent().Name.Split(new string[] { "\\" }, StringSplitOptions.RemoveEmptyEntries);
            }

            return windowsIdentity != null && windowsIdentity.Count() > 0 ? windowsIdentity[1] : WebConfigKeyValue.crmOwner;
        }

        public static string GetDomainUserNameFromWindowsIdentity()
        {
            if (System.Security.Principal.WindowsIdentity.GetCurrent() != null &&
               !String.IsNullOrEmpty(System.Security.Principal.WindowsIdentity.GetCurrent().Name))
            {
                return System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            }

            return WebConfigKeyValue.crmDomain + "\\" + WebConfigKeyValue.crmOwner;
        }     
    }
}