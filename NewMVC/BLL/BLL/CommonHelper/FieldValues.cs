﻿using System;

namespace E4D.Hcsra.WebResources.BL.CommonHelpers
{
    public class FieldValues
    {
        public String fieldType { get; set; }

        public Guid? Id { get; set; }
        public String entityLogicalName { get; set; }

        public String fieldValue { get; set; }

        // lookup
        public FieldValues(string type, Guid id, string logicalName) {
            fieldType = type;
            Id = id;
            entityLogicalName = logicalName;
        }

        // text/null lookup
        public FieldValues(string type, string value)
        {
            fieldType = type;
            fieldValue = value;
        }
    }
    
}