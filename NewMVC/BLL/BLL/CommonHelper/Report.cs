﻿using System;
using System.Net;
using Microsoft.Xrm.Sdk;
using System.IO;
using System.Globalization;
using E4D.Hcsra.WebResources.BL.CommonHelpers;
using System.ServiceModel;
using CRM.Hcsra.WebConfigDictionary;
using CRM.Hcsra.BL.ReportServer;

namespace E4D.Hcsra.WebResources.BL.CommonHelper
{
    public static class Report
    {
        public static void CreateReportPDF(IOrganizationService service, ParameterValue[] parameters, string reportPath, string fileNameWithUrl, bool userDefaultCredential = false)
        {
            try
            {
                DataSourceCredentials[] datasourceCredentials = GetDataSourceCredentials(service);
                System.Net.NetworkCredential networkCredentials = null;
                if (userDefaultCredential)
                {
                    networkCredentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                }
                else
                {
                    networkCredentials = new NetworkCredential(WebConfigKeyValue.crmOwner, WebConfigKeyValue.crmOwnerPassword, WebConfigKeyValue.crmDomain);
                }

                //replace [Organization_MSCRM] 
                Byte[] bytes = RenderReport(networkCredentials, datasourceCredentials, @reportPath, parameters, "PDF");
                //do something with your bytes like write to pdf file or attach to email etc:          
                //simple example to write to pdf file:      
                using (FileStream fs = new FileStream(@fileNameWithUrl, FileMode.Create))
                {
                    using (BinaryWriter bw = new BinaryWriter(fs))
                    {
                        bw.Write(bytes);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }            
        }

        private static DataSourceCredentials[] GetDataSourceCredentials(IOrganizationService service)
        {
            //The DataSourceName is the DataSourceName set within the actual report.
            //It is not the data source name set in SSRS Manager for the org = DataSource_MSCRM
            //If you create a report via CRM Report Wizard, it will always create the DataSourceName as CRM.
            //When you create a custom report (to upload it to CRM) BIDS (VS9) defaults the DataSourceName to something like: datasource1. 
            //it will be best to rename it to CRM to keep the below code simple. There's no easy way to
            //retrieve the datasource name out of a custom report, other than loading the file via something like FileStream and reading the xml  

            //Create DataSourceCredentials for SRS       
            DataSourceCredentials dsc = new DataSourceCredentials();
            dsc.DataSourceName = "DS";
            //this is DataSource name in the actual Report.    
            dsc.Password = WebConfigKeyValue.crmOwnerPassword; //orgId       
            dsc.UserName = WebConfigKeyValue.crmOwner ; //userId                
            DataSourceCredentials[] dsCredentials = new DataSourceCredentials[] { dsc };
            return dsCredentials;
        }

        private static Byte[] RenderReport(NetworkCredential clientCredentials, DataSourceCredentials[] dataSourceCredentials, string reportPath, ParameterValue[] parameters, string renderFormat)
        {
            byte[] resultBytes;
            string encoding;
            string mimeType;
            Warning[] warnings = null;
            string[] streamids;
            string extension;
            BasicHttpBinding binding = GetBinding(); //some use Basic  
            string deviceInfo = @"<DeviceInfo><Toolbar>False</Toolbar></DeviceInfo>";
            EndpointAddress endpoint = new EndpointAddress(WebConfigKeyValue.sqlServerURL + WebConfigKeyValue.reportServiceUrl);
            ReportExecutionServiceSoapClient client = new ReportExecutionServiceSoapClient(binding, endpoint);
            client.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            client.ClientCredentials.Windows.ClientCredential = clientCredentials;
            ExecutionInfo executionInfo; ExecutionHeader executionHeader;
            ServerInfoHeader serverInfoHeader;
            TrustedUserHeader trustedUserHeader = null;
            //load the report
            executionHeader = client.LoadReport(trustedUserHeader, reportPath, null, out serverInfoHeader, out executionInfo);
            executionHeader.ExecutionID = executionInfo.ExecutionID;
            //add the data source credentials 
            client.SetExecutionCredentials(executionHeader, trustedUserHeader, dataSourceCredentials, out executionInfo);
            //add any report parameters 
            if (parameters != null)
            {
                client.SetExecutionParameters(executionHeader, trustedUserHeader, parameters, CultureInfo.CurrentCulture.Name, out executionInfo);
            }
            //render the report to the selectef format ie PDF 
            ServerInfoHeader Info = client.Render(executionHeader, trustedUserHeader, renderFormat, deviceInfo, out resultBytes, out extension, out mimeType, out encoding, out warnings, out streamids);
            return resultBytes;
        }

        private static BasicHttpBinding GetBinding()
        {
            BasicHttpBinding binding = new BasicHttpBinding(BasicHttpSecurityMode.TransportCredentialOnly);
            binding.Security.Message.ClientCredentialType = BasicHttpMessageCredentialType.UserName;
            binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            binding.Security.Transport.ProxyCredentialType = HttpProxyCredentialType.None;
            binding.Security.Transport.Realm = string.Empty;
            binding.MaxReceivedMessageSize = 2147483647;
            return binding;
        }
    }
}