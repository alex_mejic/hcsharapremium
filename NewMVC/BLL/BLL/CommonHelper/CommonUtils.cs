﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web.Hosting;
using System.Xml;
using CRM.Hcsra.Repository;
using CRM.Hcsra.WebConfigDictionary;
using E4D.Hcsra.WebResources.BL.CommonHelpers;
using Microsoft.Xrm.Sdk;
using Xealcom.Security;
using CRM.Hcsra.BL.Monitor;

namespace CRM.Hcsra.Utils.Helpers
{

    /// <summary>
    /// Common Utils - Logs, Exceptions, App Settings...
    /// </summary>
    public static class CommonUtils
    {
        // Memory Log Entry index + lock object
        private static object currentLogEntryIndexLock = new object();
        private static long currentLogEntryIndex = 1;

        // set if we work in debug mode
        public static bool DebugMode = false;
        /// <summary>
        /// get random number
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static int GetRandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }

        #region strings manipulation
        public static string Left(string param, int length)
        {
            //we start at 0 since we want to get the characters starting from the
            //left and with the specified lenght and assign it to a variable
            string result = param.Substring(0, length);
            //return the result of the operation
            return result;
        }

        public static string Right(string param, int length)
        {
            //start at the index based on the lenght of the sting minus
            //the specified lenght and assign it a variable
            string result = param.Substring(param.Length - length, length);
            //return the result of the operation
            return result;
        }

        public static string Mid(string param, int startIndex, int length)
        {
            //start at the specified index in the string ang get N number of
            //characters depending on the lenght and assign it to a variable
            string result = param.Substring(startIndex, length);
            //return the result of the operation
            return result;
        }

        public static string Mid(string param, int startIndex)
        {
            //start at the specified index and return all characters after it
            //and assign it to a variable
            string result = param.Substring(startIndex);
            //return the result of the operation
            return result;
        }
        #endregion

        public static string GetObjectAsJSON(object obj)
        {
            // deserialize opportunity object
            MemoryStream objectStream = new MemoryStream();
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());
            serializer.WriteObject(objectStream, obj);
            return Encoding.UTF8.GetString(objectStream.ToArray());
        }

        public static string MergePdfFiles(List<string> filesURL)
        {
            string result = string.Empty;
            if (filesURL.Count > 0)
            {
                result = HostingEnvironment.MapPath(WebConfigKeyValue.templatesDocFiles) + Guid.NewGuid() + ".pdf";

                try
                {
                    PdfDocument outPutDoc = new PdfDocument();

                    foreach (string fileURL in filesURL)
                    {
                        PdfDocument filePDF = PdfReader.Open(fileURL, PdfDocumentOpenMode.Import);
                        foreach (PdfPage page in filePDF.Pages)
                        {
                            outPutDoc.Pages.Add(page);
                        }
                    }

                    outPutDoc.Save(result);
                    result = Path.GetFileNameWithoutExtension(result);
                }
                catch (Exception ex)
                {
                    result = ex.Message;
                }
            }

            return result;
        }

        public static string AddLeadingCharToString(string value, string charToAdd, int valueLength)
        {
            StringBuilder sb = new StringBuilder(value);
            while (sb.ToString().Length < valueLength)
            {
                sb.Insert(0, charToAdd);
            }

            return sb.ToString();
        }

        public static string ImpersonateAndCreateFolder(string folderName)
        {
            if (!Directory.Exists(folderName))
            {
                using (new ImpersonateUser(WebConfigKeyValue.crmDomain, WebConfigKeyValue.crmOwner, WebConfigKeyValue.crmOwnerPassword))
                {
                    Directory.CreateDirectory(folderName);
                }
            }

            return folderName;
        }

        public static string SaveContentAsFile(string fileName, string content, string mimeType)
        {
            Guid directoryName = Guid.NewGuid();
            string directoryPath = HostingEnvironment.MapPath(WebConfigKeyValue.templatesDocFiles) + directoryName;
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }

            string filePath = HostingEnvironment.MapPath(WebConfigKeyValue.templatesDocFiles) + directoryName + @"\" + fileName;
            string fileURL = WebConfigKeyValue.serverURL + ":" + WebConfigKeyValue.mvcPort + "/" + WebConfigKeyValue.mvcPath + WebConfigKeyValue.templatesDocFiles.TrimStart('~') + directoryName + "/" + fileName;
            byte[] dataBuffer = Convert.FromBase64String(content);

            try
            {
                if (mimeType == "text/html")
                {
                    using (StreamWriter sw = new StreamWriter(File.Open(filePath, FileMode.Create), Encoding.UTF8))
                    {
                        sw.Write(Encoding.UTF8.GetString(dataBuffer));
                    }
                }
                else
                {
                    using (FileStream fileStream = new FileStream(filePath, FileMode.Create, FileAccess.Write))
                    {
                        if (dataBuffer.Length > 0)
                        {
                            fileStream.Write(dataBuffer, 0, dataBuffer.Length);
                        }
                    }
                }
            }
            catch (Exception)
            {
                fileURL = "-1";
            }
            return fileURL;
        }

        public static TemplateCommonBL ProductTemplate(RepositoryBase repBase, string productId, string folderForFiles, string fileName, string templateId, List<XmlDocument> dataToAdd = null)
        {
            folderForFiles = CommonUtils.ImpersonateAndCreateFolder(folderForFiles) + "\\";

            TemplateCommonBL templateCommonBL = new TemplateCommonBL();
            templateCommonBL.fileName = fileName;
            templateCommonBL.crmData = templateCommonBL.RetrieveCRMDataByTemplateId(templateId, repBase.Service, false, productId, "");
            if (dataToAdd != null)
            {
                dataToAdd.Add(templateCommonBL.crmData);
                templateCommonBL.crmData = templateCommonBL.CreateXmlDocumentKeysAndValuesFromDoc(dataToAdd);
            }

            templateCommonBL.repeatingCrmData = new System.Xml.XmlDocument();
            templateCommonBL.fileName = templateCommonBL.CreateDocuemntFromCRMData(templateCommonBL.fileName, templateCommonBL.crmData.InnerXml, templateCommonBL.repeatingCrmData.InnerXml, folderForFiles);

            return templateCommonBL;
        }

        public static XmlDocument AddDataToTemplate(string fieldId, string fieldValue)
        {
            XmlDocument doc = new XmlDocument();
            XmlElement resultXml = doc.CreateElement("resultset");
            doc.AppendChild(resultXml);
            XmlElement entityXml = doc.CreateElement("result");
            resultXml.AppendChild(entityXml);
            XmlElement attributeXml = doc.CreateElement(fieldId);
            attributeXml.InnerText = fieldValue;
            entityXml.AppendChild(attributeXml);

            return doc;
        }

        public static Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        public static Guid CreateAttachmentInAnnotation(string objectLogicalName, Guid objectGuid, string fileName, string fileExtension, string body)
        {
            Guid result = Guid.Empty;

            using (CodeMonitor codeMonitor = new CodeMonitor("Method: CreateAttachmentInAnnotation"))
            {
                using (RepositoryBase rep = new RepositoryBase())
                {
                    try
                    {
                        if (fileName != null && fileExtension != null)
                        {
                            Annotation annotation = new Annotation();

                            annotation.ObjectId = new EntityReference(objectLogicalName, objectGuid);
                            annotation.DocumentBody = body;
                            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(body);
                            annotation.DocumentBody = Convert.ToBase64String(plainTextBytes);

                            annotation.ObjectTypeCode = objectLogicalName;
                            annotation.Subject = "Sample Attachment";
                            annotation.IsDocument = true;
                            annotation.FileName = fileName + "." + fileExtension;

                            result = rep.Create(annotation);
                        }
                    }
                    catch (Exception ex)
                    {
                        CodeMonitor.WriteExceptionEntry("An error occurred in the CreateAttachmentInAnnotation Method", ex, "");
                    }
                }
            }

            return result;
        }
    }
}