﻿using System;
using System.Collections.Generic;
using E4D.Apps.Dev.Utils.Configuration;
using System.Diagnostics;

namespace CRM.Hcsra.BL.Monitor
{
    /// <summary>
    /// Code Monitor (Log, Exception & Process Time)
    /// </summary>
    public class CodeMonitor : IDisposable
    {

        #region "Local Variables"
        private static ConfigurationsManager appConfig = new ConfigurationsManager();
        private static object currentLogEntryLock = new object();
        private int currentMilliseconds = 0;
        private string message;
        private string extraMessageData;
        //  private ConfigurationsManager configManager;
        #endregion

        #region Constructors
        public CodeMonitor()
        {
            //  configManager = new ConfigurationsManager();
            currentMilliseconds = Environment.TickCount;
            message = "";
            extraMessageData = "";
        }

        public CodeMonitor(string messageToAutoLog)
        {
            currentMilliseconds = Environment.TickCount;
            message = messageToAutoLog;
        }
        #endregion

        #region "Public Members"
        /// <summary>
        /// Get Current Milliseconds (since initialize)
        /// </summary>
        /// <returns></returns>
        public int CurrentMilliseconds()
        {
            return Environment.TickCount - currentMilliseconds;
        }

        /// <summary>
        /// Add extra message data to log
        /// </summary>
        /// <param name="str"></param>
        public void AddExtraMessageData(string str)
        {
            if (extraMessageData != "") extraMessageData += " ";
            extraMessageData += str;
        }

        /// <summary>
        /// clear message (so it would not be written automatically on dispose)
        /// </summary>
        public void ClearMessageData()
        {
            message = "";
        }

        /// <summary>
        /// Write Log Entry
        /// </summary>
        /// <param name="message"></param>
        /// <param name="traceLevel"></param>
        /// <param name="extraData"></param>
        //public static void WriteLogEntry(string message, TraceLevel traceLevel, string extraData)
        //{
        //    // write log entry to memory
        //    lock (currentLogEntryLock)
        //    {
        //        // get context variables

        //        // write to logs file
        //        // <add key="ErrorsMonitorMode" value="true"/>

        //        string LogsMonitorMode = appConfig.GetApplicationSettingValue("LogsMonitorMode", "").Trim();
        //        if (LogsMonitorMode == "true")
        //        {
        //            string logsDirectory = appConfig.GetApplicationSettingValue("Logs_Directory", "").Trim();
        //            if (logsDirectory != "")
        //            {
        //                string logsFile = System.IO.Path.Combine(logsDirectory, string.Format("E4D.Hcsra.MVCServices_logs_{0}.txt", DateTime.Now.ToString("yyyy-MM-dd")));
        //                string logMessage = "\r\n// **************************************************************************** //";
        //                logMessage += "\r\nDate & Time: " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        //                //logMessage += "\r\nEnvironment: " + string.Format("session id = {0}; thread = {1}; user = {2}; client ip = {3};", contextVariables.SessionID, contextVariables.ThreadID, contextVariables.User, contextVariables.ClientIP);
        //                logMessage += "\r\nTrace Level: " + traceLevel.ToString();
        //                logMessage += "\r\nMessage: " + message;
        //                logMessage += "\r\nExtra Data: " + extraData;
        //                //System.IO.File.AppendAllText(logsFile, "\r\n" + logMessage);
        //            }
        //        }
        //    }
        //}
        public static void WriteLogEntry(string message, TraceLevel traceLevel, string extraData)
        {
            TraceEventType traceEventType = GetTraceEventType(traceLevel);

            LogWriter.Log.WriteLogEntry(message, traceEventType, extraData);
        }

        public static void WriteLogEntry(string message, TraceLevel traceLevel, string extraData, string category)
        {
            TraceEventType traceEventType = GetTraceEventType(traceLevel);
            LogWriter.Message logMessage = new LogWriter.Message();

            logMessage.UserMessage = "\r\nMessage: " + message + "\r\nExtra Data: " + extraData;
            logMessage.Severity = traceEventType;
            logMessage.TechnicalMessage = String.Empty;
            logMessage.Properties = null;

            LogWriter.Log.WriteLog(logMessage, category);
        }
        
        public static void WriteLogEntry(string message, TraceLevel traceLevel, string extraData, params KeyValuePair<string, object>[] properties)
        {
            TraceEventType traceEventType = GetTraceEventType(traceLevel);

            LogWriter.Message logMessage = new LogWriter.Message();
            logMessage.UserMessage = "\r\nMessage: " + message + "\r\nExtra Data: " + extraData;
            logMessage.Severity = traceEventType;
            logMessage.TechnicalMessage = String.Empty;
            logMessage.Properties = new Dictionary<string, object>();
            foreach (var property in properties)
            {
                logMessage.Properties.Add(property);
            }

            //CodeMonitor.WriteLogEntry(message, traceLevel, extraData);

            
            LogWriter.Log.WriteLog(logMessage);
        }

        /// <summary>
        /// Write Exception Entry 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="ex"></param>
        /// <param name="extraData"></param>
        //public static void WriteExceptionEntry(string source, Exception ex, string extraData)
        //{
        //    lock (currentLogEntryLock)
        //    {
        //        string ErrorsMonitorMode = appConfig.GetApplicationSettingValue("ErrorsMonitorMode", "").Trim();
        //        if (ErrorsMonitorMode == "true")
        //        {
        //            // write log entry
        //            WriteLogEntry("Error on " + source + " : " + ex.Message, TraceLevel.Error, extraData);

        //            // get context variables

        //            // write to errors file
        //            string logsDirectory = appConfig.GetApplicationSettingValue("Logs_Directory", "").Trim();
        //            if (logsDirectory != "")
        //            {
        //                string errorsFile = System.IO.Path.Combine(logsDirectory, string.Format("E4D.Hcsra.MVCServices_errors_{0}.txt", DateTime.Now.ToString("yyyy-MM-dd")));
        //                string errorMessage = "\r\n// **************************************************************************** //";
        //                errorMessage += "\r\nDate & Time: " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        //                // errorMessage += "\r\nEnvironment: " + string.Format("session id = {0}; thread = {1}; user = {2}; client ip = {3};", contextVariables.SessionID, contextVariables.ThreadID, contextVariables.User, contextVariables.ClientIP);
        //                errorMessage += "\r\nSource: " + source;
        //                errorMessage += "\r\nException: " + ex.Message;
        //                errorMessage += "\r\nExtra Data: " + extraData;
        //                errorMessage += "\r\nStack Trace: " + ex.StackTrace;
        //                if (ex.InnerException != null)
        //                {
        //                    errorMessage += "\r\nInner Exception: " + ex.InnerException.Message;
        //                    errorMessage += "\r\nInner Stack Trace: " + ex.InnerException.StackTrace;
        //                }
        //                System.IO.File.AppendAllText(errorsFile, "\r\n" + errorMessage);
        //            }

        //            //create new Log Record
        //            new GlobalBL().WriteLogRecord(source, ex, extraData);                    
        //        }
        //    }
        //}
        public static void WriteExceptionEntry(string source, Exception ex, string extraData)
        {
            LogWriter.Log.WriteExceptionLog(source, ex, extraData);
        }
        #endregion

        /// <summary>
        /// Auto write log entry on dispose
        /// </summary>
        public void Dispose()
        {
            if (message != "")
            {
                int currentMS = Environment.TickCount - currentMilliseconds;
                WriteLogEntry(string.Format("{0} ; process time: {1}ms", message + extraMessageData, currentMS.ToString()), TraceLevel.Verbose, "");
            }
        }

        private static TraceEventType GetTraceEventType(TraceLevel traceLevel)
        {
            int traceLevelValue = (int)traceLevel;

            switch (traceLevelValue)
            {
                //Off = 0
                case 0:
                    {
                        return TraceEventType.Information;
                    }
                //Error = 1
                case 1:
                    {
                        return TraceEventType.Error;
                    }
                //Warning = 2
                case 2:
                    {
                        return TraceEventType.Warning;
                    }
                //Info = 3
                case 3:
                    {
                        return TraceEventType.Information;
                    }
                //Verbose = 4
                case 4:
                    {
                        return TraceEventType.Verbose;
                    }
                default:
                    {
                        return TraceEventType.Information;
                    }
            }
        }
    }
}
