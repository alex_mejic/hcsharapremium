﻿using E4D.Hcsra.WebResources.BL.CommonHelpers;
using System.IO;
using System.Text;
using Aspose.Words;
using CRM.Hcsra.WebConfigDictionary;

namespace E4D.Hcsra.WebResources.BL.CommonHelper
{
    public class AsposeWord
    {
        public AsposeWord()
        {
            Aspose.Words.License license = new Aspose.Words.License();
            license.SetLicense(WebConfigKeyValue.asposeLicense);
        }

        public void ConvertFileToPDF(string sourceFilePath, string destinationFilePath)
        {
            Aspose.Words.Document doc = new Aspose.Words.Document(sourceFilePath);
            doc.Save(destinationFilePath);
        }

        public void AddFooterAndHeaderToDocFile(string fileName, string header, string footer)
        {
            Document doc = new Document(fileName);
            DocumentBuilder builder = new DocumentBuilder(doc);
            Section currentSection = builder.CurrentSection;
            PageSetup pageSetup = currentSection.PageSetup;
            pageSetup.HeaderDistance = 5;
            builder.MoveToHeaderFooter(HeaderFooterType.HeaderPrimary);
            builder.InsertImage(header, Aspose.Words.Drawing.RelativeHorizontalPosition.Page, 0
                                , Aspose.Words.Drawing.RelativeVerticalPosition.TopMargin, 0, 520, 80, Aspose.Words.Drawing.WrapType.Square);
            //builder.MoveToDocumentEnd();
            currentSection = builder.CurrentSection;
            pageSetup = currentSection.PageSetup;
            pageSetup.FooterDistance = 5;
            builder.MoveToHeaderFooter(HeaderFooterType.FooterPrimary);
            builder.InsertImage(footer, Aspose.Words.Drawing.RelativeHorizontalPosition.Page, 0
                               , Aspose.Words.Drawing.RelativeVerticalPosition.BottomMargin, 0, 600, 80, Aspose.Words.Drawing.WrapType.None);
            HeaderFooter primaryFooter = currentSection.HeadersFooters[HeaderFooterType.FooterPrimary];

            //builder
            doc.Save(fileName);
        }

        public void RemoveHeaderAndFooter(string fileName)
        {
            Document doc = new Document(fileName);

            foreach (Section item in doc)
            {
                HeaderFooter footerPrimary = item.HeadersFooters[HeaderFooterType.FooterPrimary];
                if (footerPrimary != null)
                    footerPrimary.Remove();

                //HeaderFooter footerFirst = item.HeadersFooters[HeaderFooterType.FooterFirst];
                //if (footerFirst!= null)
                //    footerFirst.Remove();

                //HeaderFooter footerEven = item.HeadersFooters[HeaderFooterType.FooterEven];
                //if (footerEven!= null)
                //    footerEven.Remove();

                HeaderFooter headerSection = item.HeadersFooters[HeaderFooterType.HeaderPrimary];
                if (headerSection != null)
                    headerSection.Remove();
            }

            doc.Save(fileName);
        }

        public string GetHtmlStreamFromDocFile(string filePath)
        {
            string result = string.Empty;
            Aspose.Words.Document document = new Aspose.Words.Document(filePath);
            MemoryStream stream = new MemoryStream();
            document.Save(stream, Aspose.Words.SaveFormat.Html);
            result = Encoding.UTF8.GetString(stream.ToArray());
            stream.Close();

            return result;
        }

        public void StreamToPdf(Stream stream, LoadFormat loadFormat, string destinationFilePath)
        {
            string result = string.Empty;
            Aspose.Words.Document doc = new Aspose.Words.Document(stream, new LoadOptions() { LoadFormat = LoadFormat.Html, Encoding = Encoding.UTF8 });
            doc.Save(destinationFilePath);            
        }

        public Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }
    }
}