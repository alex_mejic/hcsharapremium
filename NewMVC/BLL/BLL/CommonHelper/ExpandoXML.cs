﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Dynamic;

namespace E4D.Hcsra.WebResources.BL.CommonHelper
{
    public static class ExpandoXml
    {
        public static dynamic AsExpando(this XDocument document)
        {
            return CreateExpando(document.Root);
        }

        private static dynamic CreateExpando(XElement element)
        {
            var result = new ExpandoObject() as IDictionary<string, object>;
            if (element.Elements().Any(e => e.HasElements))
            {
                var list = new List<ExpandoObject>();
                result.Add(element.Name.ToString(), list);
                foreach (var childElement in element.Elements())
                {
                    list.Add(CreateExpando(childElement));
                }
            }
            else
            {
                foreach (var leafElement in element.Elements())
                {
                    foreach (var attribute in leafElement.Attributes())
                    {
                        result.Add(attribute.Name.ToString(), attribute.Value);
                    }

                    result.Add(leafElement.Name.ToString(), leafElement.Value);
                }
            }

            return result;
        }
    }

    public static class ExpandoXml2
    {
        public static dynamic AsExpando2(this XDocument document)
        {
            return CreateExpando(document.Root);
        }

        private static dynamic CreateExpando(XElement element)
        {
            var result = new ExpandoObject() as IDictionary<string, object>;
            if (element.Elements().Any(e => e.HasElements))
            {
                var list = new List<ExpandoObject>();
                result.Add(element.Name.ToString(), list);
                foreach (var childElement in element.Elements())
                {
                    list.Add(CreateExpando(childElement));
                }
            }
            else
            {
                foreach (var leafElement in element.Elements())
                {
                    result.Add(leafElement.Name.ToString(), leafElement.Value);
                }
            }

            return result;
        }
    }
}