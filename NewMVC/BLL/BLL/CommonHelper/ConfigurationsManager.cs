﻿using System;
using System.Xml;
using System.Configuration;

namespace E4D.Apps.Dev.Utils.Configuration
{
    /// <summary>
    /// Work with configurations pages util class 
    /// </summary>
    public class ConfigurationsManager : IDisposable
    {
        /// <summary>
        /// Xml source document
        /// </summary>
        private XmlDocument configSource = null;

        /// <summary>
        /// Constructor method for load current configuration page
        /// </summary>
        /// <param name="ConfigType">Type of page</param>
        public ConfigurationsManager()
        {
            LoadConfiguration();
        }


        /// <summary>
        /// read app settings value
        /// </summary>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public  string GetApplicationSettingValue(string key, string defaultValue)
        {
            string result = ConfigurationSettings.AppSettings[key];
            return string.IsNullOrEmpty(result) ? defaultValue : result;
        }

        /// <summary>
        /// Return value from configuration page by setting property
        /// </summary>
        /// <param name="tagName">Setting key of configuration page</param>
        /// <param name="tagProperty">Property of selected setting</param>
        /// <param name="tagPropertyValue">Value of property</param>
        /// <returns>String Value</returns>
        public string GetConfigurationValue(String tagName, String tagProperty, String tagPropertyValue) 
        {
            XmlNode node = configSource.DocumentElement.SelectSingleNode("//"+tagName+"[@"+tagProperty+"='"+tagPropertyValue+"']");
            if (node == null)
                return string.Empty;
            return node.ChildNodes[0].InnerText;
        }

        /// <summary>
        /// Return value of attribute from configuration page by setting property
        /// </summary>
        /// <param name="tagName">Setting key of configuration page</param>
        /// <param name="tagProperty">Property of selected setting</param>
        /// <param name="tagPropertyValue">Value of property</param>
        /// <param name="returnedAttributeName">Attribute name to return</param>
        /// <returns>String Value</returns>
        public string GetConfigurationValue(String tagName, String tagProperty, String tagPropertyValue, String returnedAttributeName)
        {
            XmlNode node = configSource.DocumentElement.SelectSingleNode("//" + tagName + "[@" + tagProperty + "='" + tagPropertyValue + "']");
            if (node == null)
                return string.Empty;
            if (node.Attributes[returnedAttributeName] == null)
                return string.Empty;
            return node.Attributes[returnedAttributeName].Value;
        }
        /// <summary>
        /// Load configuration page 
        /// </summary>
        /// <param name="ConfigType">Configuration page type</param>
        private void LoadConfiguration() 
        {
            String configPath = String.Empty;
            configSource = new XmlDocument();
            configSource.Load(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (configSource != null)
                configSource = null;
        }

        #endregion
    }
}
