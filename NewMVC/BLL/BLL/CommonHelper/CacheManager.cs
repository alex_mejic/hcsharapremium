﻿using System.Collections;
using System.Web;

namespace E4D.Hcsra.WebResources.BL.CommonHelper
{
    public static class CacheManager
    {
        public static void Add(string key, object value)
        {
            HttpContext.Current.Cache[key] = value;
        }

        public static T GetValue<T>(string key)
        {
            return (T)HttpContext.Current.Cache.Get(key);
        }

        public static void RemoveKey(string key)
        {
            HttpContext.Current.Cache.Remove(key);
        }

        public static void RemoveAllKeys()
        {
            IDictionaryEnumerator enumarator = HttpContext.Current.Cache.GetEnumerator();

            while (enumarator.MoveNext())
            {
                HttpContext.Current.Cache.Remove(enumarator.Key.ToString());
            }
        }

        public static bool Contains(string key)
        {
            return HttpContext.Current.Cache.Get(key) != null;
        }
    }
}