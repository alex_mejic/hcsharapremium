﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace E4D.Hcsra.WebResources.BL.EnumUtil
{
    public static class EnumUtil
    {
        public static IEnumerable<T> GetValues<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }

        public static int ConvertEnumValueFieldToInt<TEnum>(string fieldData) where TEnum : struct
        {
            int result = 0;
            TEnum en;
            if (Enum.TryParse<TEnum>(fieldData, out en))
                result = (int)Enum.Parse(typeof(TEnum), en.ToString());

            return result;
        }
    }   
}
