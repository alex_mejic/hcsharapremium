﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CRM.Hcsra.DataModel;
using Repository.Repository;
using CRM.Hcsra.WebConfigDictionary;
using E4D.Hcsra.WebResources.BL.CommonHelpers;
using Xealcom.Security;
using Microsoft.Xrm.Sdk;
using DataModel.Models;
using static DataModel.Models.Enums.DetectEnums;

namespace BLL.BL
{
    public class IncidentBL
    {


        public static MVCResponse GetIncidentsByContact(string ContactID)
        {
            MVCResponse result = new MVCResponse();

            using (ImpersonateUser impersonate = new ImpersonateUser(WebConfigKeyValue.crmDomain, WebConfigKeyValue.crmOwner, WebConfigKeyValue.crmOwnerPassword))
            {
                using (IncidentRepository repo = new IncidentRepository())
                {
                    return result = repo.GetIncidentsByContact(new Guid(ContactID));
                }
            }



        }

        public static MVCResponse CreateIncident(string contactID, string polisaID, IncidentNewVM model)
        {

            MVCResponse result = new MVCResponse();
            Guid contactId = new Guid(contactID);
            Guid polisaId = new Guid(polisaID);

            try
            {

                using (ImpersonateUser impersonate = new ImpersonateUser(WebConfigKeyValue.crmDomain, WebConfigKeyValue.crmOwner, WebConfigKeyValue.crmOwnerPassword))
            {
                using (IncidentRepository repo = new IncidentRepository())
                {
                    
                        Guid incCreated = repo.CreateIncident(contactId, model);
                        //connect polisot haim
                        EntityReferenceCollection entityRefColl = new EntityReferenceCollection();
                        entityRefColl.Add(new EntityReference("cs1_polisa", polisaId));
                        repo.AssociateRequest(new EntityReference("incident", incCreated), entityRefColl, "cs1_incident_cs1_polisa");

                        # region Annotation
                        using (AnnotationRepository Annorepo = new AnnotationRepository())
                        {
                            Annotation anno = new Annotation();
                            AnnoNoteText(model, anno);
                            anno.ObjectId = new EntityReference("incident", incCreated);
                            var annoCreatedID =   Annorepo.CreateAnnotation(anno);
                            result.Data.Add("annoCreatedID", annoCreatedID);
                        }
                        #endregion

                        if (model.hcshra == true)
                        {
                            using (IncidentRepository rep = new IncidentRepository())
                            {
                                rep.updateIncTeam(incCreated);
                            }
                        }

                        result.Success = true;
                        result.Data.Add("result", "אירוע נוצר בהצלחה!");

                        result.Data.Add("incCreated", incCreated);
                    }
                   
                }
            }
            catch (Exception ex)
            {
                result.ErrorMessage = string.Format("An error has occurred. Message text :{0}, trace {1}", ex.Message, ex.StackTrace);
                result.Success = false;
            }
            return result;
        }

        public static MVCResponse GetlatestIncidentsPremium()
        {
            MVCResponse result = new MVCResponse();

            try
            {
                using (ImpersonateUser impersonate = new ImpersonateUser(WebConfigKeyValue.crmDomain, WebConfigKeyValue.crmOwner, WebConfigKeyValue.crmOwnerPassword))
                {
                    using (IncidentRepository repo = new IncidentRepository())
                    {
                        result =  repo.GetlatestIncidentsPremium();
                    }
                }
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.ErrorMessage = string.Format("An error has occurred. Message text :{0}, trace {1}", ex.Message, ex.StackTrace);
                result.Success = false;
            }
            return result;
        }

        public static MVCResponse UpdateIncidentTeam(string incId)
        {
            MVCResponse result = new MVCResponse();

            try
            {
                using (ImpersonateUser impersonate = new ImpersonateUser(WebConfigKeyValue.crmDomain, WebConfigKeyValue.crmOwner, WebConfigKeyValue.crmOwnerPassword))
                {

                    using (IncidentRepository repo = new IncidentRepository())
                    {
                        repo.updateIncTeam(new Guid(incId));
                    }

                    result.Success = true;
                    result.Data.Add("result", "אירוע עודכן בהצלחה!");
                }

            }
            catch (Exception ex)
            {
                result.ErrorMessage = string.Format("An error has occurred. Message text :{0}, trace {1}", ex.Message, ex.StackTrace);
                result.Success = false;
            }


          
            return result;
        }

        private static void AnnoNoteText(IncidentNewVM model, Annotation anno)
        {
            if (model.ambulatory == true)
            {
                anno.Subject = "פירוט טיפול אמבולאטורי";
                anno.NoteText = model.ambulatoryText;
            }
            else
            {
                anno.Subject = "נושא הפנייה";
                if (model.otherReason == "change")
                {
                    anno.NoteText = string.Concat(anno.NoteText, model.ChangeReqText2);
                }
            }
        }

        public static MVCResponse UpdateIncident(string incID, IncidentNewVM model)
        {
            MVCResponse result = new MVCResponse();


            try
            {
                using (ImpersonateUser impersonate = new ImpersonateUser(WebConfigKeyValue.crmDomain, WebConfigKeyValue.crmOwner, WebConfigKeyValue.crmOwnerPassword))
                {

                    using (IncidentRepository repo = new IncidentRepository())
                    {
                        Incident item = repo.GetIncidentById(new Guid(incID));

                        if (item != null)
                        {
                            using (AnnotationRepository Annorepo = new AnnotationRepository())
                            {

                                Annotation annoRelated = Annorepo.getAnnotationByRelID(item.Id);
                                if (annoRelated != null)
                                {
                                    AnnoNoteText(model, annoRelated);
                                    Annorepo.Update(annoRelated);
                                }
                            }

                            if (model.hcshra == true)
                            {
                                using (IncidentRepository rep = new IncidentRepository())
                                {
                                    rep.updateIncTeam(new Guid(incID));
                                }
                            }
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                result.ErrorMessage = string.Format("An error has occurred. Message text :{0}, trace {1}", ex.Message, ex.StackTrace);
                result.Success = false;
            }

    
            return result;
        }

    }
}
