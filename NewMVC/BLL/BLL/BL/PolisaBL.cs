﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CRM.Hcsra.DataModel;
using Repository.Repository;
using CRM.Hcsra.WebConfigDictionary;
using E4D.Hcsra.WebResources.BL.CommonHelpers;

namespace BLL.BL
{
    public class PolisaBL
    { 
        public MVCResponse GetPolisotByMevutah(String entityId, String entityLogicalName, String ID)
        {
            MVCResponse result = new MVCResponse();
            List<PolisaModel> policiesList = null;
          
                try
                {
                    if (!String.IsNullOrEmpty(entityId))
                    {
                        Guid entityGuid = new Guid(entityId);
                        using (PolisaRepository polisaDM = new PolisaRepository())
                        {

                            string onlinePlicies = null;
                            policiesList = polisaDM.GetPolisotToView(entityGuid, entityLogicalName, ID);

                            string[] fieldsArray = WebConfigKeyValue.policyFields.Split(',');
                            StringBuilder sb = new StringBuilder();
                            for (int i = 0; i < fieldsArray.Length; i++)
                            {
                                sb.Append("\"" + fieldsArray[i] + "\",");
                            }
                            List<string> listFieldsArray = fieldsArray.ToList();

                            foreach (PolisaModel polisa in policiesList)
                            {
                                string data = "{\"CrmFields\":[" + sb.ToString().TrimEnd(',') + "],\"PolicyKey\":\"" + polisa.Code + "\", \"UserName\": \"" + Manager.GetUserNameFromWindowsIdentity() + "\"}";
                                dynamic obj = System.Web.Helpers.Json.Decode(Manager.WebFacadeCall(data, WebConfigKeyValue.policyInfoWCFPath));
                                if (obj.Data != null)
                                {
                                    polisa.StatusName = fieldsArray.Contains("cs1_status") ? Manager.ConvertIfDbNullTo<string>(obj.Data[listFieldsArray.IndexOf("cs1_status")].Value) : "";
                                    polisa.Month_premia = fieldsArray.Contains("cs1_month_premia") ? Manager.ConvertIfDbNullTo<decimal>(obj.Data[listFieldsArray.IndexOf("cs1_month_premia")].Value) : "";
                                    polisa.ShemTohnit = fieldsArray.Contains("cs1_shem_tohnit") ? Manager.ConvertIfDbNullTo<string>(obj.Data[listFieldsArray.IndexOf("cs1_shem_tohnit")].Value) : "";
                                    polisa.CreditCard = fieldsArray.Contains("cs1_mis_kartis_ashrai") ? Manager.ConvertIfDbNullTo<string>(obj.Data[listFieldsArray.IndexOf("cs1_mis_kartis_ashrai")].Value) : "";
                                }
                                else if (!string.IsNullOrEmpty(obj.ErrorMessage))
                                {
                                    string[] errorMessages = ((string)obj.ErrorMessage).Split(new string[] { "ERR_ID" }, StringSplitOptions.None);
                                    if (errorMessages[0] == WebConfigKeyValue.noPremissionErrorOracle)
                                    {
                                        //ViewBag.ErrorMessage = WebConfigKeyValue.policiesErrorNoPremission;//אין הרשאות
                                    }
                                    else
                                    {
                                        //string error = errorMessages.Length > 1 ? errorMessages[1] : "0";
                                        //ViewBag.ErrorMessage = WebConfigKeyValue.generalErrorMessage + " " + error;
                                        //return PartialView("~/Views/Policies/_Policies.cshtml");
                                    }
                                }
                            }

                            // ViewBag.PoliciesFromOnLine = onlinePlicies;
                            // return PartialView("~/Views/Policies/_Policies.cshtml", policiesList);
                            result.Data["policiesList"] = policiesList;
                            return result;
                        }

                    }
                    //contact
                    //return PartialView("~/Views/Policies/_Policies.cshtml");
                }
                catch (Exception ex)
                {
                    result.Data["Exception"] = ex.Message;
                    //CRM.Hcsra.BL.Monitor.CodeMonitor.WriteExceptionEntry("An error occurred while trying to retrieve polisot", ex, "");
                    // ViewBag.ErrorMessage = "אירעה שגיאה בהצגת פוליסות, אנא פנה למנהל מערכת";
                    //return PartialView("~/Views/Policies/_Policies.cshtml");
                }
                return result;
        }
    }
}
