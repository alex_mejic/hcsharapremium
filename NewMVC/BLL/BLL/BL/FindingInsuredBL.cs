﻿using DataModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRM.Hcsra.DataModel;
using CRM.Hcsra.Repository;
using Repository.Repository;
using CRM.Hcsra.WebConfigDictionary;
using Xealcom.Security;

namespace BLL.BL
{
    public class FindingInsuredBL
    {
        public static MVCResponse GetInshured(FindingInsuredSearchModel searchModel)
        {
            MVCResponse result = new MVCResponse();
            try
            {
                using (ImpersonateUser impersonate = new ImpersonateUser(WebConfigKeyValue.crmDomain, WebConfigKeyValue.crmOwner, WebConfigKeyValue.crmOwnerPassword))
                {
                    using (FindingInsuredRepository findingInsuredRep = new FindingInsuredRepository())
                    {
                        List<InshuredContactVM> contactsList = findingInsuredRep.GetInshured(searchModel);
                        result.Data.Add("contactsList", contactsList);
                    }
                }
            }

            catch (Exception ex)
            {
                result.ErrorMessage = ex.Message;
                throw ex;
            }

            return result;
        }
    }
}
