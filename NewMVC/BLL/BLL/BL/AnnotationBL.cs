﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CRM.Hcsra.DataModel;
using Repository.Repository;
using CRM.Hcsra.WebConfigDictionary;
using E4D.Hcsra.WebResources.BL.CommonHelpers;
using Xealcom.Security;
using Microsoft.Xrm.Sdk;
using DataModel.Models;
using static DataModel.Models.Enums.DetectEnums;
using System.Web;
using System.IO;

namespace BLL.BL
{
    public class AnnotationBL
    {
        public void CreateFileAnnotation(string incCreated, HttpPostedFile hpf,string _uploadedFileName)
        {
            var annotation = new Annotation();
            annotation.Subject = _uploadedFileName;
            annotation.ObjectId = new EntityReference(Incident.EntityLogicalName, new Guid(incCreated));
            annotation.ObjectTypeCode = Incident.EntityLogicalName;

            byte[] fileData = null;
            using (var binaryReader = new BinaryReader(hpf.InputStream))
            {
                fileData = binaryReader.ReadBytes(hpf.ContentLength);
            }

            var textContentBase64 = Convert.ToBase64String(fileData);

            annotation.FileName = hpf.FileName;
            annotation.MimeType = hpf.ContentType;
            annotation.DocumentBody = textContentBase64;

            using (ImpersonateUser impersonate = new ImpersonateUser(WebConfigKeyValue.crmDomain, WebConfigKeyValue.crmOwner, WebConfigKeyValue.crmOwnerPassword))
            { 
                    using (AnnotationRepository repo = new AnnotationRepository())
                    {
                        repo.Create(annotation);
                    }

            }
        }
    }
}
