﻿using BLL.BL;
using CRM.Hcsra.DataModel;
using CRM.Hcsra.WebConfigDictionary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Xealcom.Security;

namespace MVC.Controllers
{
    public class PoliciesController : Controller
    {
        private PolisaBL polisaBl = new PolisaBL();

        [HttpGet]
        public MVCResponse GetPolisotByMevutah(string entityId, string entityLogicalName, string ID)
        {
            MVCResponse polisotByMevutah;
            using (new ImpersonateUser(WebConfigKeyValue.crmDomain, WebConfigKeyValue.crmOwner, WebConfigKeyValue.crmOwnerPassword))
            {
                polisotByMevutah = this.polisaBl.GetPolisotByMevutah(entityId, entityLogicalName, ID);
            }
            return polisotByMevutah;
        }
    }
}