﻿using BLL.BL;
using CRM.Hcsra.DataModel;
using DataModel.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace MVC.Controllers
{
    public class IncidentController : ApiController
    {
        public class IncTransterModel
        {
            public string contactID { get; set; }
            public string polisaID { get; set; }
            public IncidentNewVM model { get; set; }

        }

        public class IncUpdateModel
        {
            public IncidentNewVM model { get; set; }
            public string incID { get; set; }

        }

        public MVCResponse CreateIncident(IncTransterModel model)
        {
            return IncidentBL.CreateIncident(model.contactID, model.polisaID, model.model);
        }

        public MVCResponse GetIncidentsByContact(string ContactID)
        {
            return IncidentBL.GetIncidentsByContact(ContactID);
        }

        public MVCResponse UpdateIncident(IncUpdateModel model)
        {
            return IncidentBL.UpdateIncident(model.incID, model.model);
        }


        [HttpGet]
        public MVCResponse GetlatestIncidentsPremium()
        {
            return IncidentBL.GetlatestIncidentsPremium();
        }


        

        [HttpPost()]
        public string UploadFiles()
        {
            string response = string.Empty;
            var currRequest = System.Web.HttpContext.Current.Request;

            System.Web.HttpFileCollection hfc = currRequest.Files;

            var incCreated = currRequest.Params["incCreated"];
            var subjectTitle = currRequest.Params["subjectTitle"];

            HttpPostedFile file = hfc[0];
                if (incCreated != null)
                {
                    try
                    {
                        new AnnotationBL().CreateFileAnnotation(incCreated, file, subjectTitle);
                        response = "Files Uploaded Successfully";
                    }
                    catch (Exception ex)
                    {
                        response = "Upload Failed -> Message " + ex.Message;
                    }
                }

            return response;
        }

        //[HttpPost]
        //[Route("PostFileWithData")]
        //public async Task<HttpResponseMessage> Post()
        //{
        //    if (!Request.Content.IsMimeMultipartContent())
        //    {
        //        throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
        //    }

        //    var root = HttpContext.Current.Server.MapPath("~/App_Data/Uploadfiles");
        //    Directory.CreateDirectory(root);
        //    var provider = new MultipartFormDataStreamProvider(root);
        //    var result = await Request.Content.ReadAsMultipartAsync(provider);


        //    var model = result.FormData["jsonData"];
        //    if (model == null)
        //    {
        //        throw new HttpResponseException(HttpStatusCode.BadRequest);
        //    }
        //    //TODO: Do something with the JSON data.  

        //    //get the posted files  
        //    foreach (var file in result.FileData)
        //    {
        //        //TODO: Do something with uploaded file.  
        //    }

        //    return Request.CreateResponse(HttpStatusCode.OK, "success!");
        //}

    }
}
