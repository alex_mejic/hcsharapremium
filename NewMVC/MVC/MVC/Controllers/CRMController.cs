﻿using BLL.BL;
using CRM.Hcsra.DataModel;
using DataModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace MVC.Controllers
{
    public class CRMController : ApiController
    {
        [System.Web.Http.HttpPost]
        public MVCResponse GetInshured(FindingInsuredSearchModel searchModel)
        {
            return FindingInsuredBL.GetInshured(searchModel);
        }
    }
}