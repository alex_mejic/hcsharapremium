﻿angular.module('jqbutton', [])
    .directive('jqbutton', function () {
        return {
            restrict: 'AE', // HTML Attribute or Elements
            replace: true,
            template: '<button></button>',
            link: function (scope, element, attrs) {
                // Turn the button into a jQuery button
                element.text(attrs.text).button();

                scope.$watch(attrs.jqButtonDisabled, function (value) {
                    element.button("option", "disabled", value);
                });
            }
        };
    });

    angular.module('accordion', [])
    .directive('accordion', function () {
        return {
            restrict: 'AE',
            replace: true,
            link: function (scope, element, attrs) {
                scope.$watch('contactGuid', function (newValue, oldValue) {
                    if (oldValue != newValue) {
                        element.accordion({
                            collapsible: true,
                            active: false,
                            heightStyle: "content",
                            icons: false
                        });
                    }
                });

                scope.$watch('accordionDataReady', function (newValue, oldValue) {
                    if (oldValue != newValue) {
                        element.accordion({
                            collapsible: true,
                            active: false,
                            heightStyle: "content",
                            icons: false
                        });
                    }
                });
            }
        };
    });

angular.module('datepicker', [])
    .directive('datepicker', function () {
        return {
            restrict: 'AE',
            replace: true,
            link: function (scope, element, attrs) {
                setTimeout(function () {
                    if (attrs.policy !== undefined) {
                        // Choose specific dates
                        //element.datepicker({ beforeShowDay: available });

                        var now = new Date();
                        var minDate = now.setDate(1);
                        // Check if it is December
                        var month = now.getMonth() != 11 ? now.getMonth() + 1 : 0;
                        var maxDate = new Date(now.setMonth(month));
                        // Check if it is December
                        if (month == 0) {
                            maxDate.setFullYear(now.getFullYear() + 1);
                        }

                        maxDate.setDate(daysInMonth(maxDate.getMonth() + 1, maxDate.getFullYear()));

                        element.datepicker({
                            maxDate: new Date(maxDate),
                            minDate: new Date(minDate),
                            isRTL: true,
                            dateFormat: 'dd/mm/yy'
                        });
                    }
                    else {
                        element.datepicker({ dateFormat: 'dd/mm/yy' });
                    }
                }, 300);

                // Save the current month's first day and next month's first day and insert them into an array of dates that will later be the only dates available for the user to pick.
                var date = new Date();
                date.setDate(1);
                var nextMonth = new Date(date);
                var month = nextMonth.getMonth();
                var year = nextMonth.getFullYear();
                if (month == 11) {
                    nextMonth.setMonth(0);
                    nextMonth.setFullYear(year + 1);
                }
                else {
                    nextMonth.setMonth(month + 1);
                }

                // Set dates format
                var arr = [];
                arr.push(date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear());
                arr.push(nextMonth.getDate() + "-" + (nextMonth.getMonth() + 1) + "-" + nextMonth.getFullYear());

                function available(date) {
                    dmy = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
                    if ($.inArray(dmy, arr) != -1) {
                        return [true, "", "Available"];
                    } else {
                        return [false, "", "unAvailable"];
                    }
                }

                function daysInMonth(month, year) {
                    return new Date(year, month, 0).getDate();
                }
            }
        };
    });

angular.module('isnumber', [])
    .directive('isnumber', function () {
        return {
            restrict: 'EA',
            replace: true,
            template: '<input name="{{inputName}}" ng-model="inputValue">',
            scope: {
                inputValue: '=',
                inputName: '='
            },
            link: function (scope, element, attr) {
                scope.$watch('inputValue', function (newValue, oldValue) {
                    if (attr.required && newValue == undefined) {
                        scope.inputValue = "";
                    }
                    else {

                        //Checking if Decimal
                        var arr = String(newValue).split("");
                        if (arr.length === 0) return;
                        if (arr.length === 1 && (arr[0] == '-')) return;
                        //                        if (arr.length === 2 && newValue === '.') return;
                        if (isNaN(newValue)) {
                            scope.inputValue = oldValue;
                        }

                        //Limiting the Decimal to 2 digits
                        var dotsArr = String(newValue).split(".");
                        if (dotsArr.length == 2) {
                            if (dotsArr[1].length > 2) {
                                scope.inputValue = oldValue;
                            }
                        }

                        //Check min/max values
                        if (attr.max != undefined && parseInt(attr.max) < parseInt(newValue)) {
                            alert("הערך המקסימלי לשדה זה: " + parseInt(attr.max));
                            scope.inputValue = oldValue;
                        }
                        if (attr.min != undefined && parseInt(attr.min) > parseInt(newValue)) {
                            alert("הערך הנימינלי לשדה זה: " + parseInt(attr.min));
                            scope.inputValue = oldValue;
                        }
                    }
                });
            }
        };
    });

// confirmDialog directive
// -----------------------
//
// The following directive will create a modal confirmation box.
// Several attributes will be required for it to work:
//
// confirm-method - holds the function in the scope that handels the confirmation of the message.
// cancel-method - holds the function in the scope that handels the cancelation of the message.
// show - holds the boolean variable that will reveal the confirmation box when true.
// message - holds the message that will be written to the user
// confirm-button - holds the text that will be presented on the confirm button
// cancel-button - holds the text that will be presented on the cancel button

// Example:
// --------
//
// <div confirm-dialog show='rejectYipuiKoah' confirm-method='RejectActivation' cancel-method='CloseThisDialog' confirm-button="אישור" cancel-button="ביטול" message='שים לב! האם אתה בטוח שברצונך לדחות את ייפוי הכוח?'></div>
//
// <div confirm-dialog show='errorInYipuiKoah' confirm-method='ConfirmAsError' cancel-method='CloseThisDialog' confirm-button="אישור" cancel-button="ביטול" message='שים לב! תשלח הודעה למסלקה שהמסמכים לא תקינים. האם אתה בטוח שאתה רוצה להמשיך?'></div>
//
// <div confirm-dialog show='confirmYipuiKoah' confirm-method='ConfirmYiupiKoah' cancel-method='CloseThisDialog' confirm-button="אישור" cancel-button="ביטול" message='שים לב! תשלח הודעה למסלקה שהמסמכים תקינים. האם אתה בטוח שאתה רוצה להמשיך?'></div>

angular.module('confirmDialog', [])
    .directive("confirmDialog", function ($parse) {
        var directiveDefinitionObject = {
            restrict: 'A',
            replace: true,
            template: '<div style="width: 100%; height: 100%; position: absolute; background: rgba(0, 0, 0, 0.7); top: 0px; left: 0; z-index: 9999;" ng-show="show" ng-click="CancelFunction()">' +
                          '<div style="z-index: 10000; width: 440px; height: 110px; position: fixed; background: rgb(255, 255, 255); top: 0px; bottom: 0px; right: 0px; left: 0px; margin: auto; border: 1px solid black;" ng-click="$event.stopPropagation();">' +
                              '<p style="direction:rtl">' + unescape("%u200F%u200F") + '{{ message }}' + unescape("%u200F") + ' </p>' +
                              '<div>' +
                                  '<input id="data_cancel" jqbutton type="button" text="{{ cancelButton }}" ng-click="CancelFunction()"></input>' +
                                  '<input id="data_confirm" jqbutton type="button" text="{{ confirmButton }}" ng-click="ConfirmFunction()"></input>' +
                              '</div>' +
                          '</div>' +
                      '</div>',
            scope: {
                confirmMethod: '&',
                cancelMethod: '&',
                show: '=',
                message: '@message',
                confirmButton: '@confirmButton',
                cancelButton: '@cancelButton'
            },
            link: function (scope, element, attrs) {
                scope.ConfirmFunction = function () {
                    scope.$eval(scope.confirmMethod());
                }

                scope.CancelFunction = function () {
                    scope.$eval(scope.cancelMethod());
                }
            }
        };

        return directiveDefinitionObject;
    });

angular.module('dialog', [])
    .directive('dialog', function ($parse) {
        return {
            restrict: 'A',
            replace: true,
            template: '<div style="width: 100%; height: 100%; position: absolute; background: rgba(0, 0, 0, 0.7); top: 0px; left: 0; z-index: 9999;" ng-show="activatedialog" ng-click="CancelFunction()">' +
                        '<div style="z-index: 10000; width: 400px; height: 80px; padding-top:10px; position: fixed; background: rgb(255, 255, 255); top: 0px; bottom: 0px; right: 0px; left: 0px; margin: auto; border: 1px solid black;" ng-click="$event.stopPropagation();">' +
                            "<label style='padding-left:10px'>{{ dialoglabel }}</label><input type='text' ng-model='rejectReason' /><br/><button style='margin-top:10px' jqbutton text='אישור' ng-click='DialogReasonOnClick()'></button> <button style='margin-top:10px' jqbutton text='ביטול' ng-click='CancelFunction()'></button>" +
                        '</div>' +
                      '</div>',
            scope: {
                activatedialog: '=',
                dialoglabel: '@dialoglabel',
                dialogFunction: '&',
                cancelFunction: '&'
            },
            link: function (scope, element, attrs) {
                scope.DialogReasonOnClick = function () {
                    scope.$eval(scope.dialogFunction(scope.rejectReason));
                }

                scope.CancelFunction = function () {
                    scope.$eval(scope.cancelFunction());
                }
            }
        };
    });

angular.module('errSrc', [])
    .directive('errSrc', function () {
        return {
            link: function (scope, element, attrs) {
                element.bind('error', function () {
                    if (attrs.src != attrs.errSrc) {
                        attrs.$set('src', attrs.errSrc);
                    }
                });
            }
        }
    });

angular.module('bsDropdown', []).directive('bsDropdown', function ($compile) {
    return {
        restrict: 'E',
        scope: {
            items: '=dropdownData',
            doSelect: '&selectVal',
            selectedItem: '=preselectedItem'
        },
        link: function (scope, element, attrs) {
            var html = '';
            switch (attrs.menuType) {
                case "button":
                    html += '<div class="btn-group"><button class="btn button-label btn-info">Action</button><button class="btn btn-info dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>';
                    break;
                default:
                    html += '<div class="dropdown"><a class="dropdown-toggle" role="button" data-toggle="dropdown"  href="javascript:;">Dropdown<b class="caret"></b></a>';
                    break;
            }
            html += '<ul class="dropdown-menu"><li ng-repeat="item in items"><a tabindex="-1" data-ng-click="selectVal(item)">{{item.name}}</a></li></ul></div>';
            element.append($compile(html)(scope));
            for (var i = 0; i < scope.items.length; i++) {
                if (scope.items[i].id === scope.selectedItem) {
                    scope.bSelectedItem = scope.items[i];
                    break;
                }
            }
            scope.selectVal = function (item) {
                switch (attrs.menuType) {
                    case "button":
                        $('button.button-label', element).html(item.name);
                        break;
                    default:
                        $('a.dropdown-toggle', element).html('<b class="caret"></b> ' + item.name);
                        break;
                }
                scope.doSelect({
                    selectedVal: item.id
                });
            };
            scope.selectVal(scope.bSelectedItem);
        }
    };
});

angular.module('colornote1', []).directive('colornote1', function ($parse) {
    return {
        restrict: 'AE',
        replace: true,
        template: '<div><div style="position: absolute; right: 17%; bottom:1%" ng-click="salesNoteOnClick(this);">' +
                            '<div>' +
                                '<div style="background-color: blue; width: 200px; height: 150px; position: absolute; left: 15%; max-width: 180px; font-family: Guttman Yad-Brush; -webkit-transform: rotate(25deg); -moz-transform: rotate(25deg); -o-transform: rotate(25deg); writing-mode: lr-tb; font-size: 33px; max-height: 135px; overflow: hidden;">' +
                                    '<span class="rotateNote1" ng-style="{\'font-size\': rightBottomNoteFontSize, \'bottom\': rightBottomNoteTop + \'%\'}">{{ rightBottomNote }}</span>' +
                                '</div>' +
                            '</div>' +
                      '</div>' +
                      ' <div style="width: 100%; height: 100%; position: absolute; background: rgba(0, 0, 0, 0.7); top: 0px; left: 0; z-index: 9999;" ng-show="showmodal" ng-click="hideModalFunction()">' +
                '<div style="z-index: 10000; width: 400px; height: 102px; padding-top:10px; position: fixed; background: rgb(255, 255, 255); top: 0px; bottom: 0px; right: 0px; left: 0px; margin: auto; border: 1px solid black;" ng-click="$event.stopPropagation();">' +
                    '<div>' +
                        '<div>' +
                            '<label class="input-field">נא הקלד טקסט</label>' +
                            '<input class="input-field" ng-model="rightBottomNote" type="text" />' +
                        '</div>' +
                        '<div>' +
                            '<label class="input-field">גודל גופן</label>' +
                            '<input class="input-field" ng-model="rightBottomNoteFontSize" type="text" />' +
                        '</div>' +
                        '<div>' +
                            '<label class="input-field">גובה הטקסט</label>' +
                            '<input class="input-field" ng-model="rightBottomNoteTop" type="text" />' +
                        '</div>' +
                    '</div></div>',
        scope: {
            showmodal: '='
        },
        link: function (scope, element, attrs) {
            scope.salesNoteOnClick = function () {
                scope.showmodal = true;
            };

            scope.hideModalFunction = function () {
                scope.showmodal = false;
            };
        }
    }
});

angular.module('colornote', [])
    .directive("colornote", function ($parse) {
        var directiveDefinitionObject = {
            restrict: 'A',
            replace: true,
            template: '<div style="z-index:99999; width: 180px; height: 135px;">' +
                        '<div style="position: absolute; cursor: pointer" ng-style="{\'right\': noteRightPosition, \'bottom\': noteBottomPosition}" ng-click="salesNoteOnClick(this);">' +
                            '<div>' +
                                '<div ng-style="{\'transform\': transform, \'-webkit-transform\': transform, \'-ms-transform\': transform, \'background-color\': backgroundColor}" style="width: 200px; height: 200px; position: absolute; left: 15%; max-width: 180px; font-family: Guttman Yad-Brush;  font-size: 33px; max-height: 135px; overflow: hidden; z-index:9999">' +
                                    '<span ng-style="{\'font-size\': rightBottomNoteFontSize, \'bottom\': rightBottomNoteTop + \'%\'}">{{ noteText }}</span>' +
                                '</div>' +
                            '</div>' +
                      '</div>' +
                      ' <div style="width: 1888px; height: 1000px; position: absolute; background: rgba(0, 0, 0, 0.7); top: 0px;  z-index: 9999;" ng-show="show" ng-click="hideModalFunction()">' +
                '<div style="z-index: 10000; width: 400px; height: 156px; padding-top:10px; position: fixed; background: rgb(255, 255, 255); top: 0px; bottom: 0px; right: 0px; left: 0px; margin: auto; border: 1px solid black;" ng-click="$event.stopPropagation();">' +
                        '<div>' +
                            '<label class="input-field">נא הקלד טקסט</label>' +
                            '<input class="input-field" ng-model="noteText" type="text" />' +
                        '</div>' +
                        '<div>' +
                            '<label class="input-field">צבע הפתק</label>' +
                            '<input class="input-field" ng-model="backgroundColor" type="color" />' +
                        '</div>' +
            //                        '<div>' +
            //                            '<label class="input-field">מיקום - גובה</label>' +
            //                            '<input class="input-field" ng-model="noteBottomPosition" type="text" ng-change="OnChangeBottomPosition()" />' +
            //                        '</div>' +
            //                        '<div>' +
            //                            '<label class="input-field">מיקום רוחב</label>' +
            //                            '<input class="input-field" ng-model="noteRightPosition" type="text" ng-change="OnChangeRightPosition()" />' +
            //                        '</div>' +
                        '<div>' +
                            '<label class="input-field">גודל גופן</label>' +
                            '<input class="input-field" ng-model="rightBottomNoteFontSize" type="text" />' +
                        '</div>' +
                        '<div>' +
                            '<label class="input-field">גובה הטקסט</label>' +
                            '<input class="input-field" ng-model="rightBottomNoteTop" type="text" />' +
                        '</div>' +
                        '<div>' +
                            '<label class="input-field">זוית סיבוב</label>' +
                            '<input class="input-field" ng-model="rightBottomNoteRotate" type="text" />' +
                        '</div>' +
                    '</div>' +
                      '</div>',
            scope: true,
            link: function (scope, element, attrs) {
                var LEFT_BUTTON = 0;
                var MIDDLE_BUTTON = 1;
                var RIGHT_BUTTON = 2;

                scope["backgroundColor" + $("div[colornote]").length.toString()] = "white";
                scope.backgroundColor = "white";
                scope.noteText = "הטקסט שלך";
                
                var newElement = [];
                newElement.push(element.children()[0]);
                $(newElement).draggable();
                $(newElement).css("top", "100px");
                $(newElement).css("left", "170px");
                var offset = $(newElement).offset();
                scope.salesNoteOnClick = function () {
                    if (flag === 1) {
                        scope.show = !scope.show;
                    }

                    flag = 0;
                };

                scope.hideModalFunction = function () {
                    scope.show = !scope.show;
                };



                scope.transform = "rotate(0deg)";

                scope.$watch("rightBottomNoteRotate", function (val, oldVal) {
                    scope.transform = "rotate(" + val + "deg)";
                    offset = $(newElement).offset();
                });

                // Capture the click/drag event in order to show the modal window only when clicking, ignoring dragging
                var flag = 0;
                element[0].addEventListener("mousedown", function (e) {
                    if (e.button == LEFT_BUTTON) {
                        flag = 1;
                    }

                    if (e.button == MIDDLE_BUTTON) {
                        element.remove();
                    }

                }, false);
                element[0].addEventListener("mouseup", function (e) {
                    if (e.button == RIGHT_BUTTON) {
                        flag = 0;
                    }

                }, false);
                element[0].addEventListener("mousemove", function (e) {
                    if (e.button == RIGHT_BUTTON) {
                        var img = element;
                        var center_x = (offset.left) + (img.width() / 2);
                        var center_y = (offset.top) + (img.height() / 2);
                        var mouse_x = e.pageX; var mouse_y = e.pageY;
                        var radians = Math.atan2(mouse_x - center_x, mouse_y - center_y);
                        scope.rightBottomNoteRotate = (radians * (180 / Math.PI) * -1) + 90;

                        scope.$apply();
                    }

                    if (flag === 1) {
                        flag = 2;
                    }
                }, false);
                element[0].addEventListener("contextmenu", function (e) {
                    e.preventDefault();
                }, false);
            }
        };

        return directiveDefinitionObject;
    });

angular.module('note', [])
    .directive('note', function ($compile) {
        return {
            restrict: 'AE',
            template: '<div style="cursor: pointer; width: 100px; height: 100px; background: url(\'/crm-mvc/images/salesnote2.png\'); background-size: 100px;  position: absolute; left: 100px; top: 100px; z-index: 10000" ng-click="AddNote()">&nbsp</div>',
            controller: function ($scope, $element) {
                $scope.AddNote = function () {
                    var el = $compile('<div colornote show="showModalForm"></div>')($scope);
                    $element.parent().append(el);
                }
                $element.draggable();
            }
        };
    });



angular.module('Utils', []).
directive('myEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.myEnter);
                });

                event.preventDefault();
            }
        });
    };
}).
directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
});




