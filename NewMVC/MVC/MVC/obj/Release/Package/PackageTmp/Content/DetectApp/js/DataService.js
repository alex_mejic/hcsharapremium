﻿/// <reference path="CreateKoderuaFromMailCtrl.js" />

app.factory('DateService', function ($http) {

    var GetInshured = function (data) {
        return $http({
            method: 'POST',
            url: '/PremiumMVC/api/CRM/GetInshured',
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify(data)
        });
    };

    var GetPolisotByMevutah = function (id,teudatzeut) {
        return $http({
            method: 'GET',
            url: '/PremiumMVC/api/Policies/GetPolisotByMevutah?entityId=' + id + '&entityLogicalName=contact&ID=' + teudatzeut,
            contentType: "application/json",
            dataType: "json",
        });
    };

    var GetIncidentsByContact = function (contactID) {
        return $http({
            method: 'GET',
            url: '/PremiumMVC/api/Incident/GetIncidentsByContact?ContactID=' + contactID,
            contentType: "application/json",
            dataType: "json",
        });
    };

    var CreateIncident = function (data) {
        return $http({
            method: 'POST',
            url: '/PremiumMVC/api/Incident/CreateIncident',
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify(data)
        });
    };

    var UpdateIncident = function (data) {
        return $http({
            method: 'POST',
            url: '/PremiumMVC/api/Incident/UpdateIncident',
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify(data)
        });
    };

    var GetlatestIncidentsPremium = function () {
        return $http({
            method: 'GET',
            url: '/PremiumMVC/api/Incident/GetlatestIncidentsPremium',
            contentType: "application/json",
            dataType: "json"
        });
    };




    return {
        GetInshured: GetInshured,
        GetPolisotByMevutah: GetPolisotByMevutah,
        GetIncidentsByContact: GetIncidentsByContact,
        CreateIncident: CreateIncident,
        UpdateIncident: UpdateIncident,
        GetlatestIncidentsPremium: GetlatestIncidentsPremium
    };
});

