﻿app.controller('DetailViewCtrl',
    function efController($scope, $timeout, $location, DateService, $window, $log, blockUI, Common, $filter, $routeParams, $http, $localStorage, $sessionStorage, $timeout) {
        $scope.ID = $routeParams.ID;
        $scope.createmodel = {};


        if ($sessionStorage.dataModel == undefined) {
            return;
        }
        var item = $sessionStorage.dataModel;

        $scope.createmodel = item.editmodel;


        $scope.incidentId = item.incidentId;

        $scope.isEditabe = true;
        var inshuredInfo = item;
        if (item.editmodel != undefined) {

            $scope.isFromEdit = true;

            if (!item.editmodel.OwnerName.includes("פרימיום")) {
                angular.element.find('.dis').forEach(function(formControl) {
                    formControl.disabled = true;
                });
                $scope.isEditabe = false;
            }

            if (item.editmodel.ambulatory == 'true' || item.editmodel.ambulatory == true) {
                $scope.createmodel.ambulatory = 'true';
                $scope.createmodel.ambulatoryText = item.editmodel.note != undefined ? item.editmodel.note.Text : "";

            } else {
                $scope.createmodel.ambulatory = 'false';
            }
        }


        $scope.emptyForm = function() {
            if ($scope.addFile == false) {
                $scope.formdata = new FormData();
                angular.forEach(
                    angular.element("input[type='file']"),
                    function(inputElem) {
                        angular.element(inputElem).val(null);
                    });
            }
        };




        $scope.blockBtn = false;
        $scope.update = function() {

            if ($scope.sendFiles) {
                if ($scope.uploadedFileName == undefined || $scope.uploadedFileName == "") {
                    alert("הזן שם קובץ");
                    return;
                }
            }


            var IncUpdateModel = {
                model: $scope.createmodel,
                incID: $scope.incidentId
            };


            blockUI.start("מעדכן אירוע..אנא המתינו...");
            DateService.UpdateIncident(IncUpdateModel)
                .then(function(res) {
                    if (res.data.Success == true) {
                        blockUI.start("אירוע עודכן בהצלחה!");
                        if (IncUpdateModel.model.note.ID != undefined) {
                            //if ($scope.sendFiles) {
                            //    $scope.formdata.append('uploadedFileName', $scope.uploadedFileName);
                            //    $scope.formdata.append('incCreated', $scope.incidentId);
                            //    $scope.uploadFiles();
                            //}


                        }

                        if (IncUpdateModel.model.hcshra == true) {
                            $scope.blockBtn = true;
                        }
                    } else {
                        blockUI.start("שגיאה , פנה למנהל מערכת ");
                        $log.error(res.data.ErrorMessage);
                    }
                    $timeout(function() {
                        blockUI.stop();
                        blockUI.stop();
                        blockUI.stop();
                        $location.path('/');
                    }, 1500);
                });


        }
        $scope.save = function() {

            blockUI.start("יוצא אירוע..אנא המתינו...");
            if ($scope.createmodel != undefined) {


                if ($scope.sendFiles) {
                    if ($scope.uploadedFileName == undefined || $scope.uploadedFileName == "") {
                        alert("הזן שם קובץ");
                        return;
                    }
                }

                var CreateIncTransterModel = {
                    contactID: item.ContactId,
                    polisaID: item.PolisaSelected,
                    model: $scope.createmodel
                };

                DateService.CreateIncident(CreateIncTransterModel)
                    .then(function(res) {
                        if (res.data.Success == true) {

                            if (res.data.Data.incCreated != undefined && res.data.Data.annoCreatedID != undefined) {
                                if ($scope.sendFiles) {
                                    $scope.formdata.append('uploadedFileName', $scope.uploadedFileName);
                                    $scope.formdata.append('incCreated', res.data.Data.incCreated);
                                    $scope.uploadFiles();
                                }
                            }
                            blockUI.start("אירוע נוצר בהצלחה!");
                        } else {
                            blockUI.start("שגיאה ביצירת אירוע, פנה למנהל מערכת או ונסה מאוחר יותר");
                        }

                        $timeout(function() {
                            blockUI.stop();
                            blockUI.stop();
                            blockUI.stop();
                            $location.path('/');
                        }, 2000);


                    });
            }
        };
        $scope.dateOptions = {
            formatYear: 'yy',
            maxDate: new Date(),
            startingDay: 1,
            showWeeks: false
        };
        $scope.open = function() {
            $scope.popup.opened = true;
        };
        $scope.popup = {
            opened: false
        };









        $scope.addedFiles = [];
        $scope.files = [];



        $scope.$on("seletedFile", function (event, args) {
            $scope.$apply(function () {
                $scope.files.push(args.file);
            });
        });
       
   


        $scope.removeFile = function (file) {
            $scope.files.splice(file, 1);

            $scope.addedFiles.forEach(function (item) {
                if (item.fileName == file.name) {
                    $scope.addedFiles.splice(item, 1);
                }
            });

        };
        $scope.associateWithFile = function (file, input) {
            var model = {
                fileName: file.name,
                title: input
            };

            if ($scope.addedFiles.length == 0) {
                $scope.addedFiles.push(model);
                return;
            }

            var found = false;
            var index;
            for (var i = 0; i < $scope.addedFiles.length; i++) {
                if ($scope.addedFiles[i].fileName == file.name) {
                    found = true;
                    index = i;
                    break;
                } else {
                    found = false;
                }
            }
            if (found) {
                $scope.addedFiles[index].title = model.title;
            } else {
                $scope.addedFiles.push(model);
            }
        };

        $scope.uploadTest = function (file, input,index) {
            $log.info(file);
            $log.info(input);
            $log.info(index);

            var myElInput = angular.element(document.querySelector('#input-' + index));

            if (myElInput.val() == "") {

            }

            var myEl = angular.element(document.querySelector('#object-' + index));
            var loader = '<i class="fa fa-cog fa-spin fa-3x fa-fw"></i>';
            var vi = '<i class="fa fa-check fa-2x" aria-hidden="true"></i>';
            var cancel = '<i class="fa fa-ban fa-2x" aria-hidden="true"></i>';
            
            $scope.formdata = new FormData();
            $scope.formdata.append("incCreated", $scope.incidentId);
            $scope.formdata.append("subjectTitle", input);
            $scope.formdata.append("file", file);

            myEl.html(loader);
                $http({
                    method: 'POST',
                    url: '/PremiumMVC/api/incident/UploadFiles',
                    headers: { 'Content-Type': undefined },
                    data: $scope.formdata
                }).
                success(function (data, status, headers, config) {
                    //alert("success!");
                    myEl.html(vi);
                }).
                error(function (data, status, headers, config) {
                    $log.info(data);
                    myEl.html(cancel);
                });






        };

    });


app.directive('uploadFiles', function () {
    return {
        scope: true,        //create a new scope  
        link: function (scope, el, attrs) {
            el.bind('change', function (event) {
                var files = event.target.files;
                //iterate files since 'multiple' may be specified on the element  
                for (var i = 0; i < files.length; i++) {
                    //emit event upward  
                    scope.$emit("seletedFile", { file: files[i] });
                }
            });
        }
    };
});


var tempData = {
"incidentId": "20b0ed92-4b43-e611-80d2-5065f3501aca",
"cs1_status": "",
"createdby": "crmtest LAST",
"createdon": "2016-07-06T07:30:55Z",
"cs1_tat_status": "",
"ownerid": "פמי פרימיום",
"title": "שבח פרידלר ת.ז. 068604420-פמי פרימיום-06/07/2016",
"PolisotNumbers": [
    "913fe995-daa3-e211-8951-005056a32ec2"
],
"editmodel": {
    "ambulatory": true,
    "ambulatoryReason": "reason1",
    "noteText": "פירוט טיפול אמבולאטורי:\r\n3333333333333333333",
    "OwnerName": "פמי פרימיום"
}
};