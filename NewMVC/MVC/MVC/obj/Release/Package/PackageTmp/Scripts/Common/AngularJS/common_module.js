﻿angular.module('Common', [])
    .service('Common', function ($window, $log) {
        var items = {};
        items.removeExtraBrackets = function (value) {
            if (value[0] === '{' && value[value.length - 1] === '}') {
                value = value.substring(1, value.length - 1);
            }

            return value;
        }

        // Was taken from http://stackoverflow.com/questions/13952686/how-to-make-html-input-tag-only-accept-numerical-values
        items.isNumberKey = function (evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        items.isDecimalKey = function (evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode != 46 && (charCode < 48 || charCode > 57)))
                return false;
            return true;
        }

        // Was taken from - http://stackoverflow.com/questions/149055/how-can-i-format-numbers-as-money-in-javascript/149099#149099
        items.NumberToMoney = function (number, c, d, t) {
            // prefix of 2.
            c = c == undefined ? 2 : c;
            d = d == undefined ? "." : d;
            t = t == undefined ? "," : t;
            s = number < 0 ? "-" : "",
            i = parseInt(number = Math.abs(+number || 0).toFixed(2)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;

            return (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(number - i).toFixed(c).slice(2) : "") + s;
        }

        items.GetFirstOfNextMonthDate = function () {
            var today = new Date();
            // Next month
            var mm = today.getMonth();

            // Check if we are in December and change the month to January
            if (mm == 11) {
                mm = 1;
            }
            else {
                mm += 2;
            }

            // Current year
            var yyyy = today.getFullYear();
            // Check if we are in December and increase the year by 1
            if (mm == 1) {
                yyyy++;
            }

            // Add 0 to single digit monthes
            if (mm < 10) {
                mm = '0' + mm;
            }

            // Build string
            return '01/' + mm + '/' + yyyy;
        }

        items.ChangeToDateFormat = function (date) {
            if (date) {
                var dd = date.getDate();
                var mm = date.getMonth() + 1;
                var yyyy = date.getFullYear();
                // Add 0 to single character monthes
                if (mm < 10) {
                    mm = '0' + mm;
                }

                if (dd < 10) {
                    dd = '0' + dd;
                }

                // Build string
                return dd + '/' + mm + '/' + yyyy;
            }
        }

        items.ConvertDateBeforeSendingToServer = function (date) {
            if (date) {
                var elems = date.split("/");
                if (elems.length == 3) {
                    var dd = elems[0];
                    var mm = elems[1];
                    var yyyy = elems[2];

                    return mm + '/' + dd + '/' + yyyy;
                }
            }
        }

        items.HebrewAlert = function (message) {
             alert(unescape("%u200F%u200F") + message + unescape("%u200F"));
        }

        items.ValidateID = function (idNumber) {
            var isZeros = true; //אינדיקציה אם הוכנסו רק אפסים

            if (idNumber.length < 9) {
                idNumber = items.ZeroPad(idNumber, 9);
            }

            // CHECK THE ID NUMBER
            var mone = 0, incNum;
            for (var i = 0; i < 9; i++) {
                incNum = Number(idNumber.charAt(i));

                if (incNum != 0) {
                    isZeros = false;
                }

                incNum *= (i % 2) + 1;
                if (incNum > 9) {
                    incNum -= 9;
                }
                mone += incNum;
            } // for

            if (isZeros) {
                items.HebrewAlert("מספר הזיהוי אינו חוקי אנא הקלד מספר זיהוי חוקי .");
                return false;
            }
            else if (mone % 10 != 0) {
                items.HebrewAlert("מספר הזיהוי אינו חוקי אנא הקלד מספר זיהוי חוקי .");
                return false;
            }

            return true;
        }

        items.ZeroPad = function (number, count) {
            while (number.length < count) {
                number = "0" + number;
            }

            return number;
        }

        items.IsValidID = function (num) {
            var tot = 0;
            var tz = new String(num);
            for (i = 0; i < 8; i++) {
                x = (((i % 2) + 1) * tz.charAt(i));
                if (x > 9) {
                    x = x.toString();
                    x = parseInt(x.charAt(0)) + parseInt(x.charAt(1))
                }
                tot += x;
            }

            if ((tot + parseInt(tz.charAt(8))) % 10 == 0) {

                return true;
            } else {
                return false;
            }
        }





        items.openNewWindow = function (entityID, entityType,originUrl)
        {

                        var isIE = $window.navigator.appVersion.indexOf("MSIE") != -1;
                        var originOrganization;
                        var hostName;
                        var url2open;
                        if (isIE) {
                            originOrganization = window.parent.location.href.split('/')[3];
                        } else {
                            originOrganization = document.referrer.split('/')[3];
                        }

                        if (entityID == null && entityType == "cs1_interception") {
                            url2open = "http://" + $window.location["hostname"] + '/' + originOrganization + '/main.aspx?etn=' + entityType + '&pagetype=entityrecord&extraqs=';
                        } else if (originUrl != undefined) {
                            url2open = "http://" + $window.location["hostname"] + '/' + originUrl + '/main.aspx?etn=' + entityType + '&pagetype=entityrecord&extraqs=' + "&id={" + entityID + "}";

                        } else {
                            url2open = "http://" + $window.location["hostname"] + '/' + originOrganization + '/main.aspx?etn=' + entityType + '&pagetype=entityrecord&extraqs=' + "&id={" + entityID + "}";
                        }

                        //
                        //dev
                        //var url2open = "http:" + $window.location.origin.split(':')[1] + '/' + origin + '/main.aspx?etn=' + entityType + '&pagetype=entityrecord&extraqs=' + "&id={" + entityID + "}";

                        //local

                        $log.info(encodeURI(url2open));
                        $window.open(encodeURI(url2open), '_blank');
           
        }
        return items;
    });