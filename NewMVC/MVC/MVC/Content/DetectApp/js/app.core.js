﻿var app = angular.module('app', ['ngAnimate', 'Common', 'trNgGrid', 'app.routes', 'ui.bootstrap', 'LocalStorageModule', 'blockUI', 'ngCookies', 'ngStorage']);


angular
    .module('app.routes', ['ngRoute'])
    .config(routes);


//angular
//    .module('app.repo',[])
//    .service('LocaRepo', function () {
//            var items = [];

//            var addItem = function (newObj) {
//                items.push(newObj);
//            };

//            var getItems = function () {
//                return items;
//            };
//            return {
//                addItem: addItem,
//                getItems: getItems,
//            };

//    });




app.service('LocaRepo', function () {
    var items = [];

    var addItem = function (newObj) {
        items = [];
        items.push(newObj);
    };

    var getItems = function () {
        return items;
    };
    return {
        addItem: addItem,
        getItems: getItems,
    };
});

function routes($routeProvider, $locationProvider) {
    $routeProvider.
        when('/', {
            templateUrl: '/PremiumMVC/Content/DetectApp/app/sections/incidents/_incidents.tpl.html',
            controller: 'LastIncidentsController',
        })
        .when('/detect', {
            templateUrl: '/PremiumMVC/Content/DetectApp/app/sections/detect/_detect.tpl.html',
            controller: 'DetectController',
        })
        .when('/detail/:ID', {
            templateUrl: '/PremiumMVC/Content/DetectApp/app/sections/detail/_detail.tpl.html',
            controller: 'DetailViewCtrl',
        })
        .when('/404', {
            templateUrl: 'sections/404/404.tpl.html'
        })
        .otherwise({
            redirectTo: '/'
        });

    //$locationProvider.html5Mode(false);
}