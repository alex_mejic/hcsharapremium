﻿app.controller('DetectController', function efController($scope, $timeout, $location, DateService,
    $window, $log, Common, $filter, localStorageService, $interval, blockUI, sortIncidentFilter, $sessionStorage) {
    
    localStorageService.clearAll();
    $scope.validFrom = false;
   

    //store for back

    $scope.contactsList = {};
    $scope.Incidents = {};
    $scope.Polisot = {};

    //$sessionStorage.usersFound = {};
    //$sessionStorage.polisotFound = {};
    //$sessionStorage.insidentsFound = {};

    if ($sessionStorage.usersFound != undefined) {
        $scope.contactsList = $sessionStorage.usersFound;
    }

    if ($sessionStorage.polisotFound != undefined) {
        $scope.Polisot = $sessionStorage.polisotFound;
    }

    if ($sessionStorage.insidentsFound != undefined) {
        $scope.Incidents = $sessionStorage.insidentsFound;
    }
    if ($sessionStorage.searchModel != undefined) {
        $scope.searchModel = $sessionStorage.searchModel;
    }


    $scope.clearScreen = function () {
        $scope.contactsList = undefined;
        $scope.Incidents = undefined;
        $scope.Polisot = undefined;
        $scope.searchModel = undefined;

        delete $sessionStorage.usersFound;
        delete $sessionStorage.polisotFound;
        delete $sessionStorage.insidentsFound;
        delete $sessionStorage.searchModel;
    };





    $scope.InshuredSelectedId;
    $scope.GetInshured = function ()
    {
        $scope.InshuredSelectedId = undefined;

        blockUI.start("טוען נתוני מבוטחים...");

        if ($scope.searchModel.inshuredId != undefined && $scope.searchModel.inshuredId != "") {
            if ($scope.searchModel.inshuredId.length < 9) {
                $scope.searchModel.inshuredId = Common.ZeroPad($scope.searchModel.inshuredId, 9);
            }
        }
        DateService.GetInshured($scope.searchModel)
            .then(function (res) {
                if (res.data.Data.contactsList != undefined && res.data.Data.contactsList.length > 0) {
                    $scope.contactsList = res.data.Data.contactsList;

                    $sessionStorage.usersFound = res.data.Data.contactsList;

                    $sessionStorage.searchModel = $scope.searchModel;

                    blockUI.stop();
                } else {
                    blockUI.start("לא נמצאו מבוטחים");
                    $scope.InshuredSelectedId = undefined;
                    $timeout(function () {
                        blockUI.stop();
                    }, 2000);
                }
                blockUI.stop();
            }, function (res) {
                blockUI.start("שגיאה בהבאת נתונים מהשרת, נסה שנית מאוחר יותר או פנה למנהל מערכת");
                $log.error(res.data.ExceptionMessage);

                $timeout(function () {
                    blockUI.stop();
                }, 3000);
             
            });
        blockUI.stop();
    }

    $scope.InshuredSelected = [];
    $scope.$watchCollection("InshuredSelected", function () {
        if ($scope.InshuredSelected.length > 0) {
            var inshuredId = $scope.InshuredSelected[0].TeudatZehut;
            var entityId = $scope.InshuredSelected[0].entityId;

            $scope.InshuredSelectedId = entityId;

            blockUI.start(" טוען נתוני פוליסות ואירועים של המבוטח...");

            //get polisot -> current polisot from demo array
            $scope.Polisot = demoPolisotData.Data.policiesList;
            $sessionStorage.polisotFound = demoPolisotData.Data.policiesList;
            //---------------------

            DateService.GetIncidentsByContact($scope.InshuredSelectedId)
           .then(function (res) {
               if (res.data.Data.Incidents != undefined && res.data.Data.Incidents.length > 0) {
                   $scope.Incidents = res.data.Data.Incidents;

                   $sessionStorage.insidentsFound = res.data.Data.Incidents;

                   blockUI.stop();
                   $scope.Incidentsbkp = $scope.Incidents;
               } else {
                   $scope.Incidentsbkp = [];
               }

           });
                blockUI.stop();
        }
    });

    $scope.PolisotSelected = [];
    $scope.$watchCollection("PolisotSelected", function () {
        if ($scope.PolisotSelected.length > 0) {
            

            $scope.validFrom = true;

            $scope.dataToModal = {
                ContactId: $scope.InshuredSelectedId,
                PolisaSelected: $scope.PolisotSelected[0].Key
            };


            $scope.sortedItems = [];
            angular.forEach($scope.PolisotSelected, function (obj) {
                var items = sortIncidentFilter($scope.Incidentsbkp, obj.Key);
                angular.forEach(items, function (item) {
                    if (-1 == $scope.sortedItems.indexOf(item)) {
                        $scope.sortedItems.push(item);
                    }
                });
                if ($scope.sortedItems.length > 0) {
                    $scope.Incidents = $scope.sortedItems;
                } 
            });
        } else if ($scope.Incidentsbkp != undefined &&  $scope.Incidentsbkp.length != 0) {
            $scope.Incidents = $scope.Incidentsbkp;
           
        }

        if ($scope.PolisotSelected.length == 0) {
            $scope.validFrom = false;
            $scope.dataToModal = {};
        }
    });
 
    $scope.CreateNew = function () {
       
        $sessionStorage.dataModel = $scope.dataToModal;
        $location.path('/detail/new');
    };

    $scope.ViewInDetail = function(item)
    {
       $sessionStorage.dataModel = item;
       $location.path('/detail/' + item.incidentId);
        
    }

    $scope.updateIncList = function (id) {
        if (id != undefined) {
            DateService.GetIncidentsByContact(id)
      .then(function (res) {
          if (res.data.Data.Incidents != undefined && res.data.Data.Incidents.length > 0) {
              $scope.Incidents = res.data.Data.Incidents;
              blockUI.stop();
              $scope.Incidentsbkp = $scope.Incidents;
          } else {
              $scope.Incidentsbkp = [];
          }
      });
        }
    };

});

app.filter('sortIncident', function () {
    return function (arr, giud) {

        var filtered = [];

        angular.forEach(arr, function (incident) {
            angular.forEach(incident.PolisotNumbers, function (polica) {
                if (polica == giud) {
                    filtered.push(incident);
                }
            });
        });

        return filtered;
    };
})
app.run(function () {
    var heTranslation = {
        "Search": "חפש",
        "First Page": "עמוד ראשון",
        "Next Page": "עמוד הבא",
        "Previous Page": "עמוד קודם",
        "Last Page": "עמוד אחרון",
        "Sort": "מיון",
        "No items to display": "אין נתונים",
        "displayed": "מוצגים",
        "in total": " מתוך "
    };
    TrNgGrid.translations["he"] = heTranslation;
});


var demoPolisotData = { "ErrorMessage": null, "Data": { "policiesList": [{ "Key": "913fe995-daa3-e211-8951-005056a32ec2", "Name": null, "Month_premia": 0.0, "TkufatBituh": 15, "Code": "0006591234", "StartDate": "2006-11-30T22:00:00Z", "CreditCard": null, "StatusName": "מבוטל", "PaymentType": null, "IncidentKey": null, "TarSium": null, "TarBitul": "2011-07-31T21:00:00Z", "TarihTumTkufatBituh": null, "MisCheshbonBank": "0", "ShemTohnit": "הכ\"י 1 למשכנתא", "MevutahRashi": { "Id": "572c9fdf-3fa3-e211-8951-005056a32ec2", "LogicalName": "contact", "Name": "שבח פרידלר ת.ז. 068604420" }, "MevutahMishni": null, "MisZehutBalPolisa": "068604420", "SugMevutah": "ראשי", "IsMevutahRashi": "כן", "Product": null, "DerechGvia": "אין" }, { "Key": "a495ee5a-eaa3-e211-8951-005056a32ec2", "Name": null, "Month_premia": 1141.0, "TkufatBituh": 15, "Code": "0003666856", "StartDate": "2007-11-30T22:00:00Z", "CreditCard": null, "StatusName": "מוקפא", "PaymentType": null, "IncidentKey": null, "TarSium": null, "TarBitul": "2012-02-29T22:00:00Z", "TarihTumTkufatBituh": null, "MisCheshbonBank": "0", "ShemTohnit": "שי לעצמאים", "MevutahRashi": { "Id": "572c9fdf-3fa3-e211-8951-005056a32ec2", "LogicalName": "contact", "Name": "שבח פרידלר ת.ז. 068604420" }, "MevutahMishni": null, "MisZehutBalPolisa": "068604420", "SugMevutah": "ראשי", "IsMevutahRashi": "כן", "Product": null, "DerechGvia": "אין" }, { "Key": "7bcc1693-f3a3-e211-8951-005056a32ec2", "Name": null, "Month_premia": 0.0, "TkufatBituh": 39, "Code": "0003688918", "StartDate": "2008-01-31T22:00:00Z", "CreditCard": null, "StatusName": "מוקפא", "PaymentType": null, "IncidentKey": null, "TarSium": null, "TarBitul": "2008-01-31T22:00:00Z", "TarihTumTkufatBituh": null, "MisCheshbonBank": "0", "ShemTohnit": "בסט אינווסט תגמולים  לעצמאים", "MevutahRashi": { "Id": "572c9fdf-3fa3-e211-8951-005056a32ec2", "LogicalName": "contact", "Name": "שבח פרידלר ת.ז. 068604420" }, "MevutahMishni": null, "MisZehutBalPolisa": "068604420", "SugMevutah": "ראשי", "IsMevutahRashi": "כן", "Product": null, "DerechGvia": "אין" }, { "Key": "752e2825-f9a3-e211-8951-005056a32ec2", "Name": null, "Month_premia": 0.0, "TkufatBituh": 39, "Code": "0003748159", "StartDate": "2008-10-31T22:00:00Z", "CreditCard": null, "StatusName": "מוקפא", "PaymentType": null, "IncidentKey": null, "TarSium": null, "TarBitul": "2008-10-31T22:00:00Z", "TarihTumTkufatBituh": null, "MisCheshbonBank": "0", "ShemTohnit": "בסט אינווסט תגמולים  לעצמאים", "MevutahRashi": { "Id": "572c9fdf-3fa3-e211-8951-005056a32ec2", "LogicalName": "contact", "Name": "שבח פרידלר ת.ז. 068604420" }, "MevutahMishni": null, "MisZehutBalPolisa": "068604420", "SugMevutah": "ראשי", "IsMevutahRashi": "כן", "Product": null, "DerechGvia": "אין" }, { "Key": "762e2825-f9a3-e211-8951-005056a32ec2", "Name": null, "Month_premia": 0.0, "TkufatBituh": 39, "Code": "0003748100", "StartDate": "2008-10-31T22:00:00Z", "CreditCard": null, "StatusName": "מוקפא", "PaymentType": null, "IncidentKey": null, "TarSium": null, "TarBitul": "2008-10-31T22:00:00Z", "TarihTumTkufatBituh": null, "MisCheshbonBank": "0", "ShemTohnit": "בסט אינווסט תגמולים  לעצמאים", "MevutahRashi": { "Id": "572c9fdf-3fa3-e211-8951-005056a32ec2", "LogicalName": "contact", "Name": "שבח פרידלר ת.ז. 068604420" }, "MevutahMishni": null, "MisZehutBalPolisa": "068604420", "SugMevutah": "ראשי", "IsMevutahRashi": "כן", "Product": null, "DerechGvia": "אין" }, { "Key": "772e2825-f9a3-e211-8951-005056a32ec2", "Name": null, "Month_premia": 0.0, "TkufatBituh": 39, "Code": "0003691110", "StartDate": "2008-01-31T22:00:00Z", "CreditCard": null, "StatusName": "מוקפא", "PaymentType": null, "IncidentKey": null, "TarSium": null, "TarBitul": "2008-01-31T22:00:00Z", "TarihTumTkufatBituh": null, "MisCheshbonBank": "0", "ShemTohnit": "בסט אינווסט תגמולים  לעצמאים", "MevutahRashi": { "Id": "572c9fdf-3fa3-e211-8951-005056a32ec2", "LogicalName": "contact", "Name": "שבח פרידלר ת.ז. 068604420" }, "MevutahMishni": null, "MisZehutBalPolisa": "068604420", "SugMevutah": "ראשי", "IsMevutahRashi": "כן", "Product": null, "DerechGvia": "אין" }, { "Key": "782e2825-f9a3-e211-8951-005056a32ec2", "Name": null, "Month_premia": 80871.0, "TkufatBituh": 39, "Code": "0003688850", "StartDate": "2008-01-31T22:00:00Z", "CreditCard": null, "StatusName": "מוקפא", "PaymentType": null, "IncidentKey": null, "TarSium": null, "TarBitul": "2008-01-31T22:00:00Z", "TarihTumTkufatBituh": null, "MisCheshbonBank": "0", "ShemTohnit": "בסט אינווסט תגמולים  לעצמאים", "MevutahRashi": { "Id": "572c9fdf-3fa3-e211-8951-005056a32ec2", "LogicalName": "contact", "Name": "שבח פרידלר ת.ז. 068604420" }, "MevutahMishni": null, "MisZehutBalPolisa": "068604420", "SugMevutah": "ראשי", "IsMevutahRashi": "כן", "Product": null, "DerechGvia": "אין" }, { "Key": "99b1854f-f9a3-e211-8951-005056a32ec2", "Name": null, "Month_premia": 0.0, "TkufatBituh": 39, "Code": "0003691011", "StartDate": "2008-01-31T22:00:00Z", "CreditCard": null, "StatusName": "מוקפא", "PaymentType": null, "IncidentKey": null, "TarSium": null, "TarBitul": "2008-01-31T22:00:00Z", "TarihTumTkufatBituh": null, "MisCheshbonBank": "0", "ShemTohnit": "בסט אינווסט תגמולים  לעצמאים", "MevutahRashi": { "Id": "572c9fdf-3fa3-e211-8951-005056a32ec2", "LogicalName": "contact", "Name": "שבח פרידלר ת.ז. 068604420" }, "MevutahMishni": null, "MisZehutBalPolisa": "068604420", "SugMevutah": "ראשי", "IsMevutahRashi": "כן", "Product": null, "DerechGvia": "אין" }, { "Key": "9ab1854f-f9a3-e211-8951-005056a32ec2", "Name": null, "Month_premia": 0.0, "TkufatBituh": 39, "Code": "0003748290", "StartDate": "2008-10-31T22:00:00Z", "CreditCard": null, "StatusName": "מוקפא", "PaymentType": null, "IncidentKey": null, "TarSium": null, "TarBitul": "2008-10-31T22:00:00Z", "TarihTumTkufatBituh": null, "MisCheshbonBank": "0", "ShemTohnit": "בסט אינווסט - מנהלים", "MevutahRashi": { "Id": "572c9fdf-3fa3-e211-8951-005056a32ec2", "LogicalName": "contact", "Name": "שבח פרידלר ת.ז. 068604420" }, "MevutahMishni": null, "MisZehutBalPolisa": "000001082", "SugMevutah": "ראשי", "IsMevutahRashi": "לא", "Product": null, "DerechGvia": "אין" }, { "Key": "9bb1854f-f9a3-e211-8951-005056a32ec2", "Name": null, "Month_premia": 0.0, "TkufatBituh": 39, "Code": "0003748266", "StartDate": "2008-10-31T22:00:00Z", "CreditCard": null, "StatusName": "מוקפא", "PaymentType": null, "IncidentKey": null, "TarSium": null, "TarBitul": "2008-10-31T22:00:00Z", "TarihTumTkufatBituh": null, "MisCheshbonBank": "0", "ShemTohnit": "בסט אינווסט - מנהלים", "MevutahRashi": { "Id": "572c9fdf-3fa3-e211-8951-005056a32ec2", "LogicalName": "contact", "Name": "שבח פרידלר ת.ז. 068604420" }, "MevutahMishni": null, "MisZehutBalPolisa": "000001082", "SugMevutah": "ראשי", "IsMevutahRashi": "לא", "Product": null, "DerechGvia": "אין" }, { "Key": "9cb1854f-f9a3-e211-8951-005056a32ec2", "Name": null, "Month_premia": 0.0, "TkufatBituh": 39, "Code": "0003748118", "StartDate": "2008-10-31T22:00:00Z", "CreditCard": null, "StatusName": "מוקפא", "PaymentType": null, "IncidentKey": null, "TarSium": null, "TarBitul": "2008-10-31T22:00:00Z", "TarihTumTkufatBituh": null, "MisCheshbonBank": "0", "ShemTohnit": "בסט אינווסט תגמולים  לעצמאים", "MevutahRashi": { "Id": "572c9fdf-3fa3-e211-8951-005056a32ec2", "LogicalName": "contact", "Name": "שבח פרידלר ת.ז. 068604420" }, "MevutahMishni": null, "MisZehutBalPolisa": "068604420", "SugMevutah": "ראשי", "IsMevutahRashi": "כן", "Product": null, "DerechGvia": "אין" }, { "Key": "b7adb92a-faa3-e211-8951-005056a32ec2", "Name": null, "Month_premia": 3898.0, "TkufatBituh": 14, "Code": "0003749959", "StartDate": "2008-11-30T22:00:00Z", "CreditCard": null, "StatusName": "מוקפא", "PaymentType": null, "IncidentKey": null, "TarSium": null, "TarBitul": "2012-01-31T22:00:00Z", "TarihTumTkufatBituh": null, "MisCheshbonBank": "0", "ShemTohnit": "שי אישי", "MevutahRashi": { "Id": "572c9fdf-3fa3-e211-8951-005056a32ec2", "LogicalName": "contact", "Name": "שבח פרידלר ת.ז. 068604420" }, "MevutahMishni": null, "MisZehutBalPolisa": "068604420", "SugMevutah": "ראשי", "IsMevutahRashi": "כן", "Product": null, "DerechGvia": "אין" }, { "Key": "bb938067-faa3-e211-8951-005056a32ec2", "Name": null, "Month_premia": 0.0, "TkufatBituh": 39, "Code": "0003748274", "StartDate": "2008-10-31T22:00:00Z", "CreditCard": null, "StatusName": "מוקפא", "PaymentType": null, "IncidentKey": null, "TarSium": null, "TarBitul": "2008-10-31T22:00:00Z", "TarihTumTkufatBituh": null, "MisCheshbonBank": "0", "ShemTohnit": "בסט אינווסט - מנהלים", "MevutahRashi": { "Id": "572c9fdf-3fa3-e211-8951-005056a32ec2", "LogicalName": "contact", "Name": "שבח פרידלר ת.ז. 068604420" }, "MevutahMishni": null, "MisZehutBalPolisa": "000001082", "SugMevutah": "ראשי", "IsMevutahRashi": "לא", "Product": null, "DerechGvia": "אין" }, { "Key": "18611a9e-01a4-e211-8951-005056a32ec2", "Name": null, "Month_premia": 0.0, "TkufatBituh": 39, "Code": "0003691573", "StartDate": "2008-01-31T22:00:00Z", "CreditCard": null, "StatusName": "מוקפא", "PaymentType": null, "IncidentKey": null, "TarSium": null, "TarBitul": "2008-01-31T22:00:00Z", "TarihTumTkufatBituh": null, "MisCheshbonBank": "0", "ShemTohnit": "בסט אינווסט תגמולים  לעצמאים", "MevutahRashi": { "Id": "572c9fdf-3fa3-e211-8951-005056a32ec2", "LogicalName": "contact", "Name": "שבח פרידלר ת.ז. 068604420" }, "MevutahMishni": null, "MisZehutBalPolisa": "068604420", "SugMevutah": "ראשי", "IsMevutahRashi": "כן", "Product": null, "DerechGvia": "אין" }, { "Key": "9d23fec2-01a4-e211-8951-005056a32ec2", "Name": null, "Month_premia": 0.0, "TkufatBituh": 39, "Code": "0003748308", "StartDate": "2008-10-31T22:00:00Z", "CreditCard": null, "StatusName": "מוקפא", "PaymentType": null, "IncidentKey": null, "TarSium": null, "TarBitul": "2008-10-31T22:00:00Z", "TarihTumTkufatBituh": null, "MisCheshbonBank": "0", "ShemTohnit": "בסט אינווסט - מנהלים", "MevutahRashi": { "Id": "572c9fdf-3fa3-e211-8951-005056a32ec2", "LogicalName": "contact", "Name": "שבח פרידלר ת.ז. 068604420" }, "MevutahMishni": null, "MisZehutBalPolisa": "000001082", "SugMevutah": "ראשי", "IsMevutahRashi": "לא", "Product": null, "DerechGvia": "אין" }, { "Key": "661c0ad6-07a4-e211-8951-005056a32ec2", "Name": null, "Month_premia": 6403.0, "TkufatBituh": 13, "Code": "0003815297", "StartDate": "2009-07-31T21:00:00Z", "CreditCard": null, "StatusName": "מוקפא", "PaymentType": null, "IncidentKey": null, "TarSium": null, "TarBitul": "2012-01-31T22:00:00Z", "TarihTumTkufatBituh": null, "MisCheshbonBank": "0", "ShemTohnit": "שי לעצמאים", "MevutahRashi": { "Id": "572c9fdf-3fa3-e211-8951-005056a32ec2", "LogicalName": "contact", "Name": "שבח פרידלר ת.ז. 068604420" }, "MevutahMishni": null, "MisZehutBalPolisa": "068604420", "SugMevutah": "ראשי", "IsMevutahRashi": "כן", "Product": null, "DerechGvia": "אין" }, { "Key": "c5d6a87b-0ba4-e211-8951-005056a32ec2", "Name": null, "Month_premia": 10.0, "TkufatBituh": 38, "Code": "0003816568", "StartDate": "2009-06-30T21:00:00Z", "CreditCard": null, "StatusName": "מוקפא", "PaymentType": null, "IncidentKey": null, "TarSium": null, "TarBitul": "2009-06-30T21:00:00Z", "TarihTumTkufatBituh": null, "MisCheshbonBank": "0", "ShemTohnit": "בסט אינווסט תגמולים  לעצמאים", "MevutahRashi": { "Id": "572c9fdf-3fa3-e211-8951-005056a32ec2", "LogicalName": "contact", "Name": "שבח פרידלר ת.ז. 068604420" }, "MevutahMishni": null, "MisZehutBalPolisa": "068604420", "SugMevutah": "ראשי", "IsMevutahRashi": "כן", "Product": null, "DerechGvia": "אין" }, { "Key": "c6d6a87b-0ba4-e211-8951-005056a32ec2", "Name": null, "Month_premia": 10.0, "TkufatBituh": 38, "Code": "0003816519", "StartDate": "2009-06-30T21:00:00Z", "CreditCard": null, "StatusName": "מוקפא", "PaymentType": null, "IncidentKey": null, "TarSium": null, "TarBitul": "2010-04-30T21:00:00Z", "TarihTumTkufatBituh": null, "MisCheshbonBank": "0", "ShemTohnit": "בסט אינווסט תגמולים  לעצמאים", "MevutahRashi": { "Id": "572c9fdf-3fa3-e211-8951-005056a32ec2", "LogicalName": "contact", "Name": "שבח פרידלר ת.ז. 068604420" }, "MevutahMishni": null, "MisZehutBalPolisa": "068604420", "SugMevutah": "ראשי", "IsMevutahRashi": "כן", "Product": null, "DerechGvia": "אין" }, { "Key": "73bd3e76-0ea4-e211-8951-005056a32ec2", "Name": null, "Month_premia": 0.0, "TkufatBituh": 38, "Code": "0003816410", "StartDate": "2009-06-30T21:00:00Z", "CreditCard": null, "StatusName": "מוקפא", "PaymentType": null, "IncidentKey": null, "TarSium": null, "TarBitul": "2009-06-30T21:00:00Z", "TarihTumTkufatBituh": null, "MisCheshbonBank": "0", "ShemTohnit": "בסט אינווסט - מנהלים", "MevutahRashi": { "Id": "572c9fdf-3fa3-e211-8951-005056a32ec2", "LogicalName": "contact", "Name": "שבח פרידלר ת.ז. 068604420" }, "MevutahMishni": null, "MisZehutBalPolisa": "000001199", "SugMevutah": "ראשי", "IsMevutahRashi": "לא", "Product": null, "DerechGvia": "אין" }, { "Key": "c3977936-16a4-e211-8951-005056a32ec2", "Name": null, "Month_premia": 6808.0, "TkufatBituh": 12, "Code": "0003853678", "StartDate": "2009-12-31T22:00:00Z", "CreditCard": null, "StatusName": "מוקפא", "PaymentType": null, "IncidentKey": null, "TarSium": null, "TarBitul": "2011-07-31T21:00:00Z", "TarihTumTkufatBituh": null, "MisCheshbonBank": "189270", "ShemTohnit": "שי למנהלים", "MevutahRashi": { "Id": "572c9fdf-3fa3-e211-8951-005056a32ec2", "LogicalName": "contact", "Name": "שבח פרידלר ת.ז. 068604420" }, "MevutahMishni": null, "MisZehutBalPolisa": "000001273", "SugMevutah": "ראשי", "IsMevutahRashi": "לא", "Product": null, "DerechGvia": "אין" }, { "Key": "0f73db6c-16a4-e211-8951-005056a32ec2", "Name": null, "Month_premia": 2670.0, "TkufatBituh": 12, "Code": "0003853801", "StartDate": "2009-12-31T22:00:00Z", "CreditCard": null, "StatusName": "מוקפא", "PaymentType": null, "IncidentKey": null, "TarSium": null, "TarBitul": "2012-01-31T22:00:00Z", "TarihTumTkufatBituh": null, "MisCheshbonBank": "0", "ShemTohnit": "שי לעצמאים", "MevutahRashi": { "Id": "572c9fdf-3fa3-e211-8951-005056a32ec2", "LogicalName": "contact", "Name": "שבח פרידלר ת.ז. 068604420" }, "MevutahMishni": null, "MisZehutBalPolisa": "068604420", "SugMevutah": "ראשי", "IsMevutahRashi": "כן", "Product": null, "DerechGvia": "אין" }, { "Key": "383f5035-9394-e311-bad4-005056a32ec2", "Name": null, "Month_premia": 0.0, "TkufatBituh": 39, "Code": "0003691516", "StartDate": "2008-01-31T22:00:00Z", "CreditCard": null, "StatusName": "מוקפא", "PaymentType": null, "IncidentKey": null, "TarSium": null, "TarBitul": "2008-01-31T22:00:00Z", "TarihTumTkufatBituh": null, "MisCheshbonBank": "0", "ShemTohnit": "בסט אינווסט - מנהלים", "MevutahRashi": { "Id": "572c9fdf-3fa3-e211-8951-005056a32ec2", "LogicalName": "contact", "Name": "שבח פרידלר ת.ז. 068604420" }, "MevutahMishni": null, "MisZehutBalPolisa": "000000973", "SugMevutah": "ראשי", "IsMevutahRashi": "לא", "Product": null, "DerechGvia": "אין" }] }, "Success": true }