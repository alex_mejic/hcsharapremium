﻿
app.controller(
    'LastIncidentsController',
    function Controller($scope, $timeout, $location, DateService, $window, $log, Common, $filter, localStorageService, $interval, blockUI, $uibModal, LocaRepo, $sessionStorage) {

        $scope.Incidents = [];

        $scope.GetlatestIncidentsPremium = function () {

            blockUI.start("טוען נתונים...");

            DateService.GetlatestIncidentsPremium()
            .then(function (res) {
                if (res.data.Data.Incidents != undefined && res.data.Data.Incidents.length > 0) {
                    $scope.Incidents = res.data.Data.Incidents;
                }
                blockUI.stop();
            });
        };


        //$scope.Incidents = temp_data;

        $scope.GetlatestIncidentsPremium();

        $scope.CreateNew = function () {
            $sessionStorage.dataModel = $scope.dataToModal;
            $location.path('/detail/new');
        };

        $scope.ViewInDetail = function (item) {
            $sessionStorage.dataModel = item;
            $location.path('/detail/' + item.incidentId);
        }
    });


//var temp_data = [
//  {
//      "incidentId": "7ed0f909-5843-e611-80d2-5065f3501aca",
//      "cs1_status": "הועבר לשימור",
//      "createdby": "crmtest LAST",
//      "createdon": "2016-07-06T09:00:09Z",
//      "cs1_tat_status": "",
//      "ownerid": "crmtest LAST",
//      "title": "שבח פרידלר ת.ז. 068604420-פמי פרימיום-06/07/2016",
//      "PolisotNumbers": [
//        "7bcc1693-f3a3-e211-8951-005056a32ec2"
//      ],
//      "editmodel": {
//          "ambulatory": true,
//          "ambulatoryReason": "reason1",
//          "noteText": "פירוט טיפול אמבולאטורי:\r\n",
//          "OwnerName": "crmtest LAST"
//      }
//  },
//  {
//      "incidentId": "bf247f4d-5843-e611-80d2-5065f3501aca",
//      "cs1_status": "בטיפול",
//      "createdby": "crmtest LAST",
//      "createdon": "2016-07-06T09:02:02Z",
//      "cs1_tat_status": "",
//      "ownerid": "פמי פרימיום",
//      "title": "שבח פרידלר ת.ז. 068604420-פמי פרימיום-06/07/2016",
//      "PolisotNumbers": [
//        "7bcc1693-f3a3-e211-8951-005056a32ec2"
//      ],
//      "editmodel": {
//          "ambulatory": true,
//          "ambulatoryReason": "reason1",
//          "noteText": "פירוט טיפול אמבולאטורי:\r\n",
//          "OwnerName": "פמי פרימיום"
//      }
//  }
//];


