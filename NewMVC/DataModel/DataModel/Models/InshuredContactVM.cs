﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models
{
    public class InshuredContactVM
    {
        public string entityId { get; set; }
        public string TeudatZehut { get; set; }
        public string Fname { get; set; }
        public string Lname { get; set; }
        public string FullName { get; set; }
        public string TeudaType { get; set; }
        public int? TeudaTypeInt { get; set; }
        public DateTime? BDate { get; set; }
        public string Phone { get; set; }
        public string MobilPhone { get; set; }
        public string MainCity { get; set; }
        public string MainStreet { get; set; }
        public string HouseNumber { get; set; }
        public string FlatNumber { get; set; }
        public string ZipCode { get; set; }

    }
}
