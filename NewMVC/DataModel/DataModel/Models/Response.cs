﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace CRM.Hcsra.DataModel
{
    [DataContract]
    public class MVCResponse
    {
        private string errorMessage;

        [DataMember]
        public string ErrorMessage
        {
            get
            {
                return errorMessage;
            }
            set
            {
                Success = false;
                errorMessage = value;
            }
        }

        [DataMember]
        public Dictionary<string, object> Data { get; set; }

        [DataMember]
        public bool Success { get; set; }

        public MVCResponse()
        {
            Data = new Dictionary<string, object>();
            Success = true;
        }
    }
}