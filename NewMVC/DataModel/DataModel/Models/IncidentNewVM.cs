﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models
{
     public class IncidentNewVM
    {
        public bool hcshra { get; set; }
        //ambulatory
        public bool ambulatory { get; set; }
        public string ambulatoryText { get; set; }



        public string otherReason { get; set; }
        public DateTime date { get; set; }
        public string ChangeReqText1 { get; set; }
        public string ChangeReqText2 { get; set; }


    }
}
