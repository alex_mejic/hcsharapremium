﻿using System;
using Microsoft.Xrm.Sdk;
using System.Collections.Generic;

namespace CRM.Hcsra.DataModel
{
    public class PolisaModel
    {
        public Guid? Key { get; set; }
        public String Name { get; set; }
        public decimal Month_premia { get; set; }
        public int? TkufatBituh { get; set; }
        public String Code { get; set; }
        public DateTime? StartDate { get; set; }
        public String CreditCard { get; set; }
        public String StatusName { get; set; }
        public String PaymentType { get; set; }
        public Guid? IncidentKey { get; set; }//
        public DateTime? TarSium { get; set; }//תאריך סיום
        public DateTime? TarBitul { get; set; }//תאריך ביטול
        public DateTime? TarihTumTkufatBituh { get; set; }//תאריך ביטול
        public String MisCheshbonBank { get; set; }//מספר חשבון בנק
        public String ShemTohnit { get; set; }//שם תוכנית
        public EntityReference MevutahRashi { get; set; }//מבוטח ראשי
        public EntityReference MevutahMishni { get; set; }//מבוטי משני
        public string MisZehutBalPolisa { get; set; }//מספר זהות בעל הפוליסה
        public string SugMevutah { get; set; }//סוג מבוטח
        public string IsMevutahRashi { get; set; }//האם מבוטח ראשי
        public EntityReference Product { get; set; }//מוצר
        public string DerechGvia { get; set; }//דרך גבייה
        public string Kollektive { get; set; }
        public string AgentName { get; set; }

    }

    public class PolisaDetailsModel
    {
        public string cs1_shem_bal_polisa { get; set; }
        public string cs1_l_mis_zehut { get; set; }
        public int cs1_kod_meashen_rashi { get; set; }
        public List<KeyValuePair<string, int?>> cs1_kod_meashen_rashiSet { get; set; }
        public int cs1_kod_meashen_mishni { get; set; }
        public List<KeyValuePair<string, int?>> cs1_kod_meashen_mishniSet { get; set; }
        public string cs1_l_mis_zehut_mishni { get; set; }

        public string cs1_mis_kolektiv { get; set; }
        public string cs1_mispar_sochen { get; set; }
        public string cs1_mis_mefakeach_mekori { get; set; }
        public string cs1_product { get; set; }

        public string cs1_l_tar_bitul { get; set; }
        public string cs1_schum_premia { get; set; }
        public int cs1_derech_gvia { get; set; }
        public List<KeyValuePair<string,int?>> cs1_derech_gviaSet { get; set; }
        public int? cs1_mis_atraa { get; set; }

        public int cs1_ikul { get; set; }
        public List<KeyValuePair<string, int?>> cs1_ikulSet { get; set; }
        public string cs1_schum_loan { get; set; }
    }
}
