﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models
{
    public class FindingInsuredSearchModel
    {
        public string inshuredTel { get; set; }
        public string insuredName { get; set; }
        public string inshuredKollektive { get; set; }
        public string inshuredId { get; set; }
        public string inshuredPolicyNum { get; set; }
    }
}
