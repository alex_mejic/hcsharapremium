﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Models.Enums
{
   public class DetectEnums
    {
        public enum Ambulatory
        {
            ambulatory = 861650000,
            not_ambulatory = 861650001
        }

        public enum IncOrigin
        {
            מוקד_פמי_פרימיום = 861650005
        }
    }
}
